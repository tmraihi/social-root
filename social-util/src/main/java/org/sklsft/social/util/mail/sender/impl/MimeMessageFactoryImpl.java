package org.sklsft.social.util.mail.sender.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.sklsft.social.util.mail.model.InlineAttachment;
import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.sender.interfaces.MimeMessageFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

public class MimeMessageFactoryImpl implements MimeMessageFactory {
	
	public static final String mailEncoding = "UTF-8";
	public static final boolean useHtml = true;
	
	private JavaMailSender sender;	
	
	public MimeMessageFactoryImpl(JavaMailSender sender) {
		super();
		this.sender = sender;
	}



	public MimeMessage buildMimeMessage(UserAccountMail mail) throws MessagingException {
		
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper messageHelper = new MimeMessageHelper(message, useHtml, mailEncoding);
		
		messageHelper.setFrom(mail.getSender());
		messageHelper.setTo(mail.getRecipient());
		messageHelper.setSubject(mail.getSubject());
		messageHelper.setText(mail.getContent(), useHtml);

		if (mail.getInlineAttachments() != null) {
			for (InlineAttachment attachment : mail.getInlineAttachments()) {
				messageHelper.addInline(attachment.getCid(),
						new ClassPathResource(attachment.getPath()));

			}
		}
		
		return message;
		
	}

}
