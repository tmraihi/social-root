package org.sklsft.social.util.mail.sender.interfaces;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.sklsft.social.util.mail.model.UserAccountMail;

public interface MimeMessageFactory {

	MimeMessage buildMimeMessage(UserAccountMail mail) throws MessagingException;
}
