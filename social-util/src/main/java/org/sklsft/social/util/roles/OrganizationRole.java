package org.sklsft.social.util.roles;


public enum OrganizationRole implements Comparable<OrganizationRole>{

	OWNER(0L, "Owner"), ADMIN(1L, "Administrator"), MEMBER(2L, "Member");
	
	private OrganizationRole(Long priority, String label) {
		this.priority = priority;
		this.label = label;
	}
	
	private Long priority;
	private String label;
	
	public Long getPriority() {
		return priority;
	}
	
	public String getLabel() {
		return label;
	}
}
