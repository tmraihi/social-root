package org.sklsft.social.util.mail.exception;

public class MailSendFailureException extends RuntimeException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	public MailSendFailureException(String message) {
		super(message);

	}

	public MailSendFailureException(String message, Throwable cause) {
		super(message, cause);
	}
}