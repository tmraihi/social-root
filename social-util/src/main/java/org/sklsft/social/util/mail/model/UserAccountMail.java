package org.sklsft.social.util.mail.model;

import java.util.List;

public class UserAccountMail {

	/*
	 * properties
	 */
	private String sender;
	private String recipient;
	private String subject;
	private String content;
	private List<InlineAttachment> inlineAttachments;
	
	
	/*
	 * getters and setters
	 */
	public String getSender() {
		return sender;
	}
	
	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<InlineAttachment> getInlineAttachments() {
		return inlineAttachments;
	}

	public void setInlineAttachments(
			List<InlineAttachment> inlineAttachments) {
		this.inlineAttachments = inlineAttachments;
	}
	
}
