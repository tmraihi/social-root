package org.sklsft.social.util.pages;

public enum Page {

	PUBLIC_INDEX("/public/index.jsf"),

	PRIVATE_INDEX("/private/index.jsf"),
	
	ORGANIZATION_DETAIL("/private/organization/organization-detail.jsf");
	
	
	

	private Page(String url) {
		this.url = url;
	}

	/*
	 * properties
	 */
	private String url;

	
	/*
	 * getters
	 */
	public String getUrl() {
		return url;
	}
}
