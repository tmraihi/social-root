package org.sklsft.social.util.mail.sender.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.sender.interfaces.MimeMessageFactory;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;
import org.springframework.mail.javamail.JavaMailSender;

public class UserAccountMailSenderImpl implements UserAccountMailSender {
	
	private JavaMailSender sender;	
	private MimeMessageFactory messageFactory;
	
	
	public UserAccountMailSenderImpl(JavaMailSender sender) {
		super();
		this.sender = sender;
		this.messageFactory = new MimeMessageFactoryImpl(sender);
	}
	
	public UserAccountMailSenderImpl(JavaMailSender sender, String redirectionRecipient) {
		super();
		this.sender = sender;
		this.messageFactory = new MimeMessageFactoryRedirectionImpl(sender, redirectionRecipient);
	}


	@Override
	public void sendMail(UserAccountMail mail) throws MessagingException {
		
		MimeMessage message = messageFactory.buildMimeMessage(mail);
		sender.send(message);		
	}

}
