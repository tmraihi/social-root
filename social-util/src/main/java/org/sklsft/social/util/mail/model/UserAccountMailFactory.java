package org.sklsft.social.util.mail.model;

import java.util.List;


public class UserAccountMailFactory {
	
	/*
	 * properties injected by spring
	 */
	private String sender;
	
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}



	public UserAccountMail createUserAccountMail (String recipient, String subject, String content,
			List<InlineAttachment> inlineAttachments) {

		UserAccountMail mail = new UserAccountMail();
		mail.setRecipient(recipient);
		mail.setSubject(subject);
		mail.setContent(content);
		mail.setInlineAttachments(inlineAttachments);
		mail.setSender(this.sender);
		
		return mail;
	}

}
