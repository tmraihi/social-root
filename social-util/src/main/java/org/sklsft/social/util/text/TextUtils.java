package org.sklsft.social.util.text;

public class TextUtils {

	public static String truncate(String fullDesc, int limit) {
		if(fullDesc != null){
			if (fullDesc.length() > limit) {
				return fullDesc.substring(0, limit-1).concat("...");
			} else {
				return fullDesc;
			}
		}
		return fullDesc;
	}
}
