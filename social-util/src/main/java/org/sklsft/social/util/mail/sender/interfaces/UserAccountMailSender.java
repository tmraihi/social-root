package org.sklsft.social.util.mail.sender.interfaces;

import javax.mail.MessagingException;

import org.sklsft.social.util.mail.model.UserAccountMail;

public interface UserAccountMailSender {

	void sendMail(UserAccountMail mail) throws MessagingException;
}
