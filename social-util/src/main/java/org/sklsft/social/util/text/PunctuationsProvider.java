package org.sklsft.social.util.text;

import java.util.Set;

public interface PunctuationsProvider {

	Set<String> getPunctuations();
}
