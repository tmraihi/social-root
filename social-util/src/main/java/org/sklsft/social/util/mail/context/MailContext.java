package org.sklsft.social.util.mail.context;

import org.sklsft.social.util.mail.model.UserAccountMail;

/**
 * this class, or its children, can be used to put contextual data in a {@link UserAccountMail}.
 * @author Nicolas Thibault
 *
 */
public class MailContext {
	
	/*
	 * properties
	 */
	private String recipient;
	private String subject;	

	
	/*
	 * getters and setters
	 */
	public String getRecipient() {
		return recipient;
	}

	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}
	
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	

	/*
	 * constructor
	 */
	public MailContext(String recipient, String subject) {
		this.recipient = recipient;
		this.subject = subject;
	}
}
