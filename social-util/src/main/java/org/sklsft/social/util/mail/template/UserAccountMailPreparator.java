package org.sklsft.social.util.mail.template;

import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.sklsft.social.util.mail.context.MailContext;
import org.sklsft.social.util.mail.model.InlineAttachment;
import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.model.UserAccountMailFactory;


/**
 * 
 * @author Nicolas Thibault
 *
 */
public abstract class UserAccountMailPreparator {

	private static VelocityEngine engine;
	private static final String separator = "/";
	private static final String UTF8 = "UTF8";
	private static final String DOTVM = ".vm";
	
	static {
		engine = new VelocityEngine();
		engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		engine.setProperty("classpath.resource.loader.class",ClasspathResourceLoader.class.getName());
		engine.setProperty("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogSystem");
		engine.init();
	}
	
	public UserAccountMail prepareMail(MailContext mailContext, UserAccountMailFactory factory){
		
		StringWriter writer = new StringWriter();
		
		VelocityContext context = new VelocityContext();
		context.put("context", mailContext);		
		
		Template template = engine.getTemplate(separator + this.getClass().getName().replace(".", separator) + DOTVM, UTF8);		
		template.merge(context, writer);
		
		UserAccountMail mail = factory.createUserAccountMail(mailContext.getRecipient(), mailContext.getSubject(), writer.toString(), getAttachements());
		return mail;
	}

	protected abstract List<InlineAttachment> getAttachements();
	
	
}
