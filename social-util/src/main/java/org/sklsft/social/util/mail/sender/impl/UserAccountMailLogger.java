package org.sklsft.social.util.mail.sender.impl;

import javax.mail.MessagingException;

import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserAccountMailLogger implements UserAccountMailSender {

	private static final Logger logger = LoggerFactory
			.getLogger(UserAccountMailLogger.class);

	@Override
	public void sendMail(UserAccountMail mail) throws MessagingException {

		logger.info("sending mail with "
				+ "\n"
				+ "sender : "
				+ mail.getSender()
				+ "\n"
				+ "recipient : "
				+ (mail.getRecipient() != null ? mail.getRecipient()
						: "") + "\n" + "subject : " + mail.getSubject() + "\n"
				+ "content : " + mail.getContent());

	}
}
