package org.sklsft.social.util.text;

import org.apache.commons.lang.StringUtils;


public class UrlUtils {

	public static String getAbsoluteWebsiteUrl(String websiteUrl) {
		if (StringUtils.isEmpty(websiteUrl)) {
			return "";
		}
		if (websiteUrl.contains("http://") || websiteUrl.contains("https://")) {
			return websiteUrl;
		}
		return "http://" + websiteUrl;
	}
}
