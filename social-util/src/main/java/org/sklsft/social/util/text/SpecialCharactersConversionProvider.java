package org.sklsft.social.util.text;

import java.util.Map;

public interface SpecialCharactersConversionProvider {
	
	Map<String, String> getSpecialCharactersConversion();

}
