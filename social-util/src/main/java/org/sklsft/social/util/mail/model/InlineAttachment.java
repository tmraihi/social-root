package org.sklsft.social.util.mail.model;

/**
 * represents an inline attachement that can be referenced in a mail html content
 * @author Nicolas Thibault
 *
 */
public class InlineAttachment {

	/*
	 * properties
	 */
	private String cid;
	private String path;

	/*
	 * constructor
	 */
	public InlineAttachment(String cid, String path) {
		this.cid = cid;
		this.path = path;
	}

	/*
	 * getters and setters
	 */
	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
