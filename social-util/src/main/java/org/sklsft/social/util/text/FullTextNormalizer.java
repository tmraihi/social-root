package org.sklsft.social.util.text;

import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

public class FullTextNormalizer {
	
	/*
	 * dependencies that should be injetced by spring
	 */	
	private NeutralWordsProvider neutralWordsProvider;
	
	private SpecialCharactersConversionProvider specialCharactersConversionProvider;
	
	private PunctuationsProvider punctuationsProvider;
	

	
	public FullTextNormalizer(
			NeutralWordsProvider neutralWordsProvider,
			SpecialCharactersConversionProvider specialCharactersConversionProvider,
			PunctuationsProvider punctuationProvider) {
		super();

		this.neutralWordsProvider = neutralWordsProvider;
		this.specialCharactersConversionProvider = specialCharactersConversionProvider;
		this.punctuationsProvider = punctuationProvider;
	}
	
	
	public List<String> getCanonicalTokens(String label) {
		String lowerLabel = Normalizer.normalize(label.toLowerCase(), Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		String cleanLabel = removeSpecialCharacters(lowerLabel);
		cleanLabel = removePunctuations(cleanLabel);
		return getNonNeutralTokens(cleanLabel);
	}


	public String getCanonicalLabel(String label) {
		
		return concatenate(getCanonicalTokens(label));
	}
	
	public String getCanonicalLabelIgnoreDash(String label) {
		
		return concatenate(getCanonicalTokens(label)).replace("-", " ");
	}


	private String removeSpecialCharacters(String lowerLabel) {
		
		Map<String, String> conversionMap = specialCharactersConversionProvider.getSpecialCharactersConversion();
		
		for (Entry<String, String> entry:conversionMap.entrySet()) {
			lowerLabel = lowerLabel.replace(entry.getKey(), entry.getValue());
		}
		
		return lowerLabel;
	}
	
	
	private String removePunctuations(String cleanLabel) {
		
		Set<String> punctuations = punctuationsProvider.getPunctuations();
		
		for (String entry:punctuations) {
			cleanLabel = cleanLabel.replace(entry, " ");
		}
		
		return cleanLabel;
	}


	private List<String> getNonNeutralTokens(String cleanLabel) {
		
		String[] tokens = StringUtils.split(cleanLabel);
		
		List<String> result = new LinkedList<>();
		
		Set<String> neutralWords = neutralWordsProvider.getNeutralWords();
		
		for (String token:tokens) {
			if (token.length()>1 && !neutralWords.contains(token)) {
				result.add(token);
			}
		}
		return result;
	}
	
	
	private String concatenate(List<String> tokens) {
		StringBuilder builder = new StringBuilder("");
		boolean start = true;
		
		for (String token : tokens) {
			if (start) {
				start = false;
			} else {
				builder.append(" ");
			}
			builder.append(token);
		}
		
		return builder.toString();
	}
}
