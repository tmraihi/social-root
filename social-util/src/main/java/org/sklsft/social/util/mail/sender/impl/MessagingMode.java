package org.sklsft.social.util.mail.sender.impl;

/**
 * an enumeration of the different modes used to send mime messages
 * <li> DEV : the message is simply logged
 * <li> TEST : the message is sent to a redirection recipient
 * <li> PROD : the message is really sent to the recipient
 * @author Nicolas Thibault
 *
 */
public enum MessagingMode {
	
	DEV,
	TEST,
	PROD

}
