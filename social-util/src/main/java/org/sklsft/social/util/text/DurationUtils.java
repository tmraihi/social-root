package org.sklsft.social.util.text;

import java.util.Date;

public class DurationUtils {

	private static final long NB_MS_IN_DAY = 86400000L;
	private static final long NB_MS_IN_HOUR = 3600000L;
	private static final long NB_MS_IN_MIN = 60000L;
	private static final long NB_MS_IN_SEC = 1000L;
	
	public static String getDuration(Date date) {
		long duration = (new Date()).getTime() - date.getTime();
		
		long days = duration/NB_MS_IN_DAY;
		
		if (days > 0) {
			return String.valueOf(days) + "j";
		}
		
		long hours = duration/NB_MS_IN_HOUR;
		
		if (hours > 0) {
			return String.valueOf(hours) + "h";
		}
		
		long minutes = duration/NB_MS_IN_MIN;
		
		if (minutes > 0) {
			return String.valueOf(minutes) + "min.";
		}
		
		long seconds = duration/NB_MS_IN_SEC;
		
		return String.valueOf(seconds) + "s";

	}
	
}
