package org.sklsft.social.util.mail.sender.impl;

import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;
import org.springframework.mail.javamail.JavaMailSender;

public class UserAccountMailSenderFactory {
	
	/*
	 * properties to be injected by jndi
	 */
	private String messagingMode;
	private String redirectionRecipient;
	private JavaMailSender javaMailSender;
	
	
	/*
	 * getters and setters
	 */
	public void setMessagingMode(String messagingMode) {
		this.messagingMode = messagingMode;
	}

	public void setRedirectionRecipient(String redirectionRecipient) {
		this.redirectionRecipient = redirectionRecipient;
	}

	public void setJavaMailSender(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}	


	public UserAccountMailSender getSender() {
		UserAccountMailSender sender;
		
		MessagingMode mode = MessagingMode.valueOf(messagingMode);
		
		switch (mode) {
			case DEV :
				sender = new UserAccountMailLogger();
				break;
			case TEST :
				sender = new UserAccountMailSenderImpl(javaMailSender, redirectionRecipient);
				break;
			case PROD :
				sender = new UserAccountMailSenderImpl(javaMailSender);
				break;
			default:
				throw new RuntimeException("No MimeMessageSender has been configured for the messaging mode : " + mode);
		}
		
		return sender;
	}

}
