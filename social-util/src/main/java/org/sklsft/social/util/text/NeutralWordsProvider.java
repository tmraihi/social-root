package org.sklsft.social.util.text;

import java.util.Set;

public interface NeutralWordsProvider {

	Set<String> getNeutralWords();
}
