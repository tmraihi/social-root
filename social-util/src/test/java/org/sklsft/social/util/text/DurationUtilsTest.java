package org.sklsft.social.util.text;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

public class DurationUtilsTest {

	@Test
	public void testDurationDays() {
		Date currentDate = DateUtils.addDays(new Date(), -1);
		String duration = DurationUtils.getDuration(currentDate);
		System.out.println(duration);
		Assert.assertTrue(duration.equals("1j"));
	}
	
	@Test
	public void testDurationHours() {
		Date currentDate = DateUtils.addHours(new Date(), -3);
		String duration = DurationUtils.getDuration(currentDate);
		System.out.println(duration);
		Assert.assertTrue(duration.equals("3h"));
	}
	
	@Test
	public void testDurationMinutes() {
		Date currentDate = DateUtils.addMinutes(new Date(), -3);
		String duration = DurationUtils.getDuration(currentDate);
		System.out.println(duration);
		Assert.assertTrue(duration.equals("3min."));
	}
	
	@Test
	public void testDurationSeconds() {
		Date currentDate = DateUtils.addSeconds(new Date(), -3);
		String duration = DurationUtils.getDuration(currentDate);
		System.out.println(duration);
		Assert.assertTrue(duration.equals("3s"));
	}
}
