package org.sklsft.social.util.mail;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sklsft.social.util.mail.model.UserAccountMail;
import org.sklsft.social.util.mail.model.UserAccountMailFactory;
import org.sklsft.social.util.mail.sender.impl.UserAccountMailLogger;
import org.sklsft.social.util.mail.sender.impl.UserAccountMailSenderFactory;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class UserAccountMailSenderTest {
	
	private static JavaMailSender javaMailSender;
	private static UserAccountMailSenderFactory factory;
	private static UserAccountMailSender userAccountMailSender;
	private static UserAccountMailFactory userAccountMailFactory;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		javaMailSender = new JavaMailSenderImpl();
		factory = new UserAccountMailSenderFactory();
		factory.setJavaMailSender(javaMailSender);
		
		factory.setMessagingMode("DEV");
		userAccountMailSender = factory.getSender();
		
		userAccountMailFactory = new UserAccountMailFactory();
		userAccountMailFactory.setSender("contact@supralog.com");
	}
	
	@Test
	public void testMailSender() throws MessagingException {
		
		Assert.assertTrue(userAccountMailSender instanceof UserAccountMailLogger);
		
		UserAccountMail mail = userAccountMailFactory.createUserAccountMail("test@supralog.com", "dummy subject", "<html><body><h1>dummy content</h1></body></html>", null);
		userAccountMailSender.sendMail(mail);
	}
}
