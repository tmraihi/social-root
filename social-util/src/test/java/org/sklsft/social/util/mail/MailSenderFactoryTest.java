package org.sklsft.social.util.mail;

import java.lang.reflect.Field;

import javax.mail.MessagingException;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sklsft.social.util.mail.sender.impl.MimeMessageFactoryImpl;
import org.sklsft.social.util.mail.sender.impl.MimeMessageFactoryRedirectionImpl;
import org.sklsft.social.util.mail.sender.impl.UserAccountMailLogger;
import org.sklsft.social.util.mail.sender.impl.UserAccountMailSenderFactory;
import org.sklsft.social.util.mail.sender.impl.UserAccountMailSenderImpl;
import org.sklsft.social.util.mail.sender.interfaces.UserAccountMailSender;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

public class MailSenderFactoryTest {
	
	private static JavaMailSender javaMailSender;
	private static UserAccountMailSenderFactory factory;
	
	
	@BeforeClass
	public static void setUpBeforeClass() {
		javaMailSender = new JavaMailSenderImpl();
		factory = new UserAccountMailSenderFactory();
		factory.setJavaMailSender(javaMailSender);		
	}
	
	@Test
	public void testMailSenderFactoryDev() throws MessagingException {
		
		factory.setMessagingMode("DEV");
		UserAccountMailSender mailSender = factory.getSender();
		Assert.assertTrue(mailSender instanceof UserAccountMailLogger);
			
	}
	
	@Test
	public void testMailSenderFactoryTest() throws MessagingException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
		String redirectionRecipient = "redirect@supralog.com";
		factory.setMessagingMode("TEST");
		factory.setRedirectionRecipient(redirectionRecipient);
		
		UserAccountMailSender mailSender = factory.getSender();
		Assert.assertTrue(mailSender instanceof UserAccountMailSenderImpl);
		
		Object mimeMessageFactory = extractMimeMessageFactory((UserAccountMailSenderImpl)mailSender);
		Assert.assertTrue(mimeMessageFactory instanceof MimeMessageFactoryRedirectionImpl);
		
		String effectiveRedirectionRecipient = extractRedirectionRecipient((MimeMessageFactoryRedirectionImpl)mimeMessageFactory);
		Assert.assertEquals(effectiveRedirectionRecipient, redirectionRecipient);
	}
	

	@Test
	public void testMailSenderFactoryProd() throws MessagingException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		
		factory.setMessagingMode("PROD");
		
		UserAccountMailSender mailSender = factory.getSender();
		Assert.assertTrue(mailSender instanceof UserAccountMailSenderImpl);
		
		Object mimeMessageFactory = extractMimeMessageFactory((UserAccountMailSenderImpl)mailSender);
		Assert.assertTrue(mimeMessageFactory instanceof MimeMessageFactoryImpl);
	}
	
	
	private Object extractMimeMessageFactory(UserAccountMailSenderImpl mailSender) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = mailSender.getClass().getDeclaredField("messageFactory");
		field.setAccessible(true);
		return field.get(mailSender);
	}
	
	private String extractRedirectionRecipient(MimeMessageFactoryRedirectionImpl mimeMessageFactory) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field field = mimeMessageFactory.getClass().getDeclaredField("redirectionRecipient");
		field.setAccessible(true);
		return field.get(mimeMessageFactory).toString();
	}
}
