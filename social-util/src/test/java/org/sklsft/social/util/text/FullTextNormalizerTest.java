package org.sklsft.social.util.text;

import org.junit.BeforeClass;
import org.junit.Test;

public class FullTextNormalizerTest {

	private static FullTextNormalizer normalizer;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		normalizer = new FullTextNormalizer(new NeutralWordsProviderMock(), new SpecialCharactersConversionProviderMock(), new PunctuationsProviderMock());
	}
	
	@Test
	public void testNormalizer() {
		String arg;
		String normalized;
		
		arg = "le_très beau château. Voilà un !porte-clef";
		System.out.println(arg);
		
		normalized = normalizer.getCanonicalLabel(arg);
		System.out.println(normalized);
	}
}
