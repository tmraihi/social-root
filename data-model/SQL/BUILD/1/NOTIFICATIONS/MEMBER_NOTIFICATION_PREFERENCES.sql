-- create table --
CREATE TABLE MEMBER_NOTIFICATION_PREFERENCES
(
id BIGINT,
MEMBER_ID BIGINT NOT NULL,
ACTION_TYPE_ID BIGINT NOT NULL,
MAIL_TYPE VARCHAR(255) NOT NULL
);
/

ALTER TABLE MEMBER_NOTIFICATION_PREFERENCES ADD CONSTRAINT PK_MEMBER_NOTIFICATION_PREFERENCES PRIMARY KEY (ID);
/

-- create sequence --
CREATE SEQUENCE MEMBER_NOTIFICATION_PREFERENCES_id_seq
INCREMENT 1
MINVALUE 0
MAXVALUE 9223372036854775807
START 0
CACHE 1;
/

ALTER TABLE MEMBER_NOTIFICATION_PREFERENCES ADD CONSTRAINT UC_MEMBER_NOTIFICATION_PREFERENCES UNIQUE (MEMBER_ID,ACTION_TYPE_ID);
/

-- Specific Code Start --
-- Specific Code End --
