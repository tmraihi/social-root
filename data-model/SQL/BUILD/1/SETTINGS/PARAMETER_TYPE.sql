-- create table --
CREATE TABLE PARAMETER_TYPE
(
id BIGINT,
CODE VARCHAR(255) NOT NULL,
DESCRIPTION VARCHAR(255) NOT NULL
);
/

ALTER TABLE PARAMETER_TYPE ADD CONSTRAINT PK_PARAMETER_TYPE PRIMARY KEY (ID);
/

-- create sequence --
CREATE SEQUENCE PARAMETER_TYPE_id_seq
INCREMENT 1
MINVALUE 0
MAXVALUE 9223372036854775807
START 0
CACHE 1;
/

ALTER TABLE PARAMETER_TYPE ADD CONSTRAINT UC_PARAMETER_TYPE UNIQUE (CODE);
/

-- Specific Code Start --
INSERT INTO public.parameter_type(id, code, description)VALUES (1, 'String', 'String');
INSERT INTO public.parameter_type(id, code, description)VALUES (2, 'Boolean', 'Boolean');
INSERT INTO public.parameter_type(id, code, description)VALUES (3, 'Integer', 'Integer');
INSERT INTO public.parameter_type(id, code, description)VALUES (4, 'Long', 'Long');
INSERT INTO public.parameter_type(id, code, description)VALUES (5, 'Double', 'Double');
-- Specific Code End --
