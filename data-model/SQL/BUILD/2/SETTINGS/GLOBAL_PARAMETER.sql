-- table foreign keys and indexes --
ALTER TABLE GLOBAL_PARAMETER ADD CONSTRAINT FK_GLOBAL_PARAMETER_2 FOREIGN KEY (PARAMETER_TYPE_ID) REFERENCES PARAMETER_TYPE;
/

CREATE INDEX FK_GLOBAL_PARAMETER_2 ON GLOBAL_PARAMETER(PARAMETER_TYPE_ID);
/

-- Specific Code Start --
-- Specific Code End --
