package org.sklsft.social.repository.mongodb;


import com.mongodb.DB;
import com.mongodb.MongoClient;


public class MongoDbFactory  {

	/*
	 * properties
	 */
	private MongoClient mongoClient;
	private String dbName;
	
	
	/*
	 * getters and setters
	 */
	public MongoClient getMongoClient() {
		return mongoClient;
	}
	public void setMongoClient(MongoClient mongoClient) {
		this.mongoClient = mongoClient;
	}
	public String getDbName() {
		return dbName;
	}
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}


	/**
	 * Factory method to build the mongo DB
	 * @return
	 * @throws Exception
	 */
	public DB getDb() throws Exception {
		return mongoClient.getDB(dbName);
	}
	
	

}
