package org.sklsft.social.repository.dao.interfaces.notifications.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.model.notifications.ActionType;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface ActionTypeBaseDao extends BaseDao<ActionType, Long> {

/**
 * count filtered object list
 */
Long count(ActionTypeFilter filter);

/**
 * scroll filtered object list
 */
List<ActionType> scroll(ActionTypeFilter filter, ActionTypeSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String code);

/**
 * find object or null
 */
ActionType findOrNull(String code);

/**
 * find object
 */
ActionType find(String code);

}
