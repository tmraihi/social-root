package org.sklsft.social.repository.dao.interfaces.nomenclature.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.model.nomenclature.Keyword;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface KeywordBaseDao extends BaseDao<Keyword, Long> {

/**
 * count filtered object list
 */
Long count(KeywordFilter filter);

/**
 * scroll filtered object list
 */
List<Keyword> scroll(KeywordFilter filter, KeywordSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String canonicalLabel);

/**
 * find object or null
 */
Keyword findOrNull(String canonicalLabel);

/**
 * find object
 */
Keyword find(String canonicalLabel);

/**
 * search
 */
List<Keyword> search(String arg);

}
