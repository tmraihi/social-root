package org.sklsft.social.repository.dao.interfaces.account;

import org.sklsft.social.repository.dao.interfaces.account.base.UserAccountBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface UserAccountDao extends UserAccountBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
