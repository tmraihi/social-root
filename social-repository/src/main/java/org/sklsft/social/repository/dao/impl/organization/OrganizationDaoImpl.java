package org.sklsft.social.repository.dao.impl.organization;

import org.sklsft.social.repository.dao.impl.organization.base.OrganizationBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class OrganizationDaoImpl extends OrganizationBaseDaoImpl implements OrganizationDao {

/* Specific Code Start */
/* Specific Code End */
}
