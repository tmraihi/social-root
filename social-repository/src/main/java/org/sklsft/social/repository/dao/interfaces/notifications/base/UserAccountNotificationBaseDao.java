package org.sklsft.social.repository.dao.interfaces.notifications.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.model.notifications.UserAccountNotification;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface UserAccountNotificationBaseDao extends BaseDao<UserAccountNotification, Long> {

/**
 * count filtered object list
 */
Long count(UserAccountNotificationFilter filter);

/**
 * scroll filtered object list
 */
List<UserAccountNotification> scroll(UserAccountNotificationFilter filter, UserAccountNotificationSorting sorting, Long firstResult, Long maxResults);

}
