package org.sklsft.social.repository.dao.interfaces.notifications;

import org.sklsft.social.repository.dao.interfaces.notifications.base.ActionTypeBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface ActionTypeDao extends ActionTypeBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
