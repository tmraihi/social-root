package org.sklsft.social.repository.file.impl;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.sklsft.social.repository.file.DefaultLogoManager;
import org.sklsft.social.repository.file.exception.FileError;


public class DefaultLogoManagerImpl implements DefaultLogoManager {

	public byte[] getDefaultAccountLogo() {
		
		return getLogo("../user.png");
	}
	
	public byte[] getDefaultOrganizationLogo() {
		
		return getLogo("../organization.png");
	}
	
	private byte[] getLogo(String path)  {
		try {
			InputStream stream = getClass().getResourceAsStream(path);
			
			return IOUtils.toByteArray(stream);
			
		} catch (Exception e) {
			throw new FileError("account.defaultLogo.notFound");			
		}
	}
}
