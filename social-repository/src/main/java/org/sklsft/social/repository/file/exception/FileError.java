package org.sklsft.social.repository.file.exception;


public class FileError extends RuntimeException  {
	
	private static final long serialVersionUID = 1L;
	
	public FileError(String message) {
		super(message);

	}

	public FileError(String message, Throwable cause) {
		super(message, cause);
	}

}
