package org.sklsft.social.repository.dao.interfaces.organization.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.model.organization.OrganizationMember;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationMemberBaseDao extends BaseDao<OrganizationMember, Long> {

/**
 * load object list from organization
 */
List<OrganizationMember> loadListFromOrganization(Long organizationId);

/**
 * load object list eagerly from organization
 */
List<OrganizationMember> loadListEagerlyFromOrganization(Long organizationId);

/**
 * load object list from userAccount
 */
List<OrganizationMember> loadListFromUserAccount(Long userAccountId);

/**
 * load object list eagerly from userAccount
 */
List<OrganizationMember> loadListEagerlyFromUserAccount(Long userAccountId);

/**
 * count filtered object list
 */
Long count(OrganizationMemberFilter filter);

/**
 * count object list from organization
 */
Long countFromOrganization(Long organizationId);

/**
 * count filtered object list from organization
 */
Long countFromOrganization(Long organizationId, OrganizationMemberFilter filter);

/**
 * count object list from userAccount
 */
Long countFromUserAccount(Long userAccountId);

/**
 * count filtered object list from userAccount
 */
Long countFromUserAccount(Long userAccountId, OrganizationMemberFilter filter);

/**
 * scroll filtered object list
 */
List<OrganizationMember> scroll(OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults);

/**
 * scroll filtered object from organization
 */
List<OrganizationMember> scrollFromOrganization(Long organizationId, OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults);

/**
 * scroll filtered object from userAccount
 */
List<OrganizationMember> scrollFromUserAccount(Long userAccountId, OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String organizationCode, String userAccountEmail);

/**
 * find object or null
 */
OrganizationMember findOrNull(String organizationCode, String userAccountEmail);

/**
 * find object
 */
OrganizationMember find(String organizationCode, String userAccountEmail);

}
