package org.sklsft.social.repository.dao.interfaces.settings;

import org.sklsft.social.repository.dao.interfaces.settings.base.GlobalParameterBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface GlobalParameterDao extends GlobalParameterBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
