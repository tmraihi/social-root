package org.sklsft.social.repository.dao.interfaces.settings.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.model.settings.GlobalParameter;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface GlobalParameterBaseDao extends BaseDao<GlobalParameter, Long> {

/**
 * count filtered object list
 */
Long count(GlobalParameterFilter filter);

/**
 * scroll filtered object list
 */
List<GlobalParameter> scroll(GlobalParameterFilter filter, GlobalParameterSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String code);

/**
 * find object or null
 */
GlobalParameter findOrNull(String code);

/**
 * find object
 */
GlobalParameter find(String code);

}
