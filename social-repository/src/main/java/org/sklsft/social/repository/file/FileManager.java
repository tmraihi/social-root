package org.sklsft.social.repository.file;

import org.sklsft.social.interfaces.FileEntity;


public interface FileManager {

	public void saveFile(byte[] fileData, FileEntity object);
	
	public void deleteFile(FileEntity object);
	
	public boolean existsFile(FileEntity object);
	
	public byte[] retrieveFile(FileEntity object);
	
}
