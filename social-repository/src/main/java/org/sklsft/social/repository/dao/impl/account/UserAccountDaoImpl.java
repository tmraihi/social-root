package org.sklsft.social.repository.dao.impl.account;

import org.hibernate.criterion.Restrictions;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.impl.account.base.UserAccountBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class UserAccountDaoImpl extends UserAccountBaseDaoImpl implements UserAccountDao {

/* Specific Code Start */

	/**
	 * find object object case insensitive
	 */
	@Override
	public UserAccount find(String email) {
		if (email == null) {
			return null;
		}
		UserAccount userAccount = (UserAccount) this.sessionFactory
				.getCurrentSession().createCriteria(UserAccount.class)
				.add(Restrictions.eq("email", email).ignoreCase())
				.uniqueResult();
		if (userAccount == null) {
			throw new ObjectNotFoundException("UserAccount.notFound");
		} else {
			return userAccount;
		}
	}

	
	/**
	 * exists object case insensitive
	 */
	@Override
	public boolean exists(String email) {
		if (email == null) {
			return false;
		}
		UserAccount userAccount = (UserAccount) this.sessionFactory
				.getCurrentSession().createCriteria(UserAccount.class)
				.add(Restrictions.eq("email", email).ignoreCase())
				.uniqueResult();
		return userAccount != null;
	}

/* Specific Code End */
}
