package org.sklsft.social.repository.dao.impl.notifications.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.model.notifications.ActionType;
import org.sklsft.social.repository.dao.interfaces.notifications.base.ActionTypeBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseDaoImpl extends BaseDaoImpl<ActionType, Long> implements ActionTypeBaseDao {

/**
 * constructor
 */
public ActionTypeBaseDaoImpl() {
super(ActionType.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<ActionType> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ActionType.class);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(ActionTypeFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ActionType.class).setProjection(Projections.rowCount());
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.NOTIFICATION_LABEL", filter.getNotificationLabel());
addStringContainsRestriction(criteria, "{alias}.MAIL_LABEL", filter.getMailLabel());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<ActionType> scroll(ActionTypeFilter filter, ActionTypeSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ActionType.class);
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.NOTIFICATION_LABEL", filter.getNotificationLabel());
addStringContainsRestriction(criteria, "{alias}.MAIL_LABEL", filter.getMailLabel());
addOrder(criteria, "code", sorting.getCodeOrderType());
addOrder(criteria, "notificationLabel", sorting.getNotificationLabelOrderType());
addOrder(criteria, "mailLabel", sorting.getMailLabelOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String code) {
if (code == null) {
return false;
}
ActionType actionType = (ActionType)this.sessionFactory.getCurrentSession().createCriteria(ActionType.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return actionType != null;
}

/**
 * find object or null
 */
@Override
public ActionType findOrNull(String code) {
ActionType actionType = (ActionType)this.sessionFactory.getCurrentSession().createCriteria(ActionType.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return actionType;
}

/**
 * find object
 */
@Override
public ActionType find(String code) {
if (code == null) {
return null;
}
ActionType actionType = findOrNull(code);
if (actionType == null) {
throw new ObjectNotFoundException("ActionType.notFound");
} else {
return actionType;
}
}

}