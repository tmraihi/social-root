package org.sklsft.social.repository.dao.interfaces.settings.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.model.settings.ParameterType;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface ParameterTypeBaseDao extends BaseDao<ParameterType, Long> {

/**
 * count filtered object list
 */
Long count(ParameterTypeFilter filter);

/**
 * scroll filtered object list
 */
List<ParameterType> scroll(ParameterTypeFilter filter, ParameterTypeSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String code);

/**
 * find object or null
 */
ParameterType findOrNull(String code);

/**
 * find object
 */
ParameterType find(String code);

}
