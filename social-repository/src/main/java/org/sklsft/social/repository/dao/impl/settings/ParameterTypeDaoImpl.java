package org.sklsft.social.repository.dao.impl.settings;

import org.sklsft.social.repository.dao.impl.settings.base.ParameterTypeBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.settings.ParameterTypeDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class ParameterTypeDaoImpl extends ParameterTypeBaseDaoImpl implements ParameterTypeDao {

/* Specific Code Start */
/* Specific Code End */
}
