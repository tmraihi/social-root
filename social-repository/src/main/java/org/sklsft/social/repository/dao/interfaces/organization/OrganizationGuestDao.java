package org.sklsft.social.repository.dao.interfaces.organization;

import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.organization.base.OrganizationGuestBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface OrganizationGuestDao extends OrganizationGuestBaseDao {

/* Specific Code Start */
	
	OrganizationGuest findByOrganizationAndUser(Long organizationId, Long userId);
	
/* Specific Code End */
}
