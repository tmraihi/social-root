package org.sklsft.social.repository.dao.impl.notifications;

import org.sklsft.social.repository.dao.impl.notifications.base.ActionTypeBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class ActionTypeDaoImpl extends ActionTypeBaseDaoImpl implements ActionTypeDao {

/* Specific Code Start */
/* Specific Code End */
}
