package org.sklsft.social.repository.dao.impl.notifications.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.model.notifications.UserAccountNotification;
import org.sklsft.social.repository.dao.interfaces.notifications.base.UserAccountNotificationBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addBetweenRestriction;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addBooleanRestriction;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseDaoImpl extends BaseDaoImpl<UserAccountNotification, Long> implements UserAccountNotificationBaseDao {

/**
 * constructor
 */
public UserAccountNotificationBaseDaoImpl() {
super(UserAccountNotification.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccountNotification> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotification.class);
criteria.setFetchMode("notified",FetchMode.JOIN);
criteria.setFetchMode("actionType",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(UserAccountNotificationFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotification.class).setProjection(Projections.rowCount());
Criteria notifiedCriteria = criteria.createCriteria("notified", JoinType.LEFT_OUTER_JOIN);
Criteria actionTypeCriteria = criteria.createCriteria("actionType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(notifiedCriteria, "{alias}.EMAIL", filter.getNotifiedEmail());
addStringContainsRestriction(actionTypeCriteria, "{alias}.CODE", filter.getActionTypeCode());
addBetweenRestriction(criteria, "notificationDate", filter.getNotificationDateMinValue(), filter.getNotificationDateMaxValue());
addStringContainsRestriction(criteria, "{alias}.MESSAGE", filter.getMessage());
addBooleanRestriction(criteria, "read", filter.getRead());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccountNotification> scroll(UserAccountNotificationFilter filter, UserAccountNotificationSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotification.class);
Criteria notifiedCriteria = criteria.createCriteria("notified", JoinType.LEFT_OUTER_JOIN);
Criteria actionTypeCriteria = criteria.createCriteria("actionType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(notifiedCriteria, "{alias}.EMAIL", filter.getNotifiedEmail());
addStringContainsRestriction(actionTypeCriteria, "{alias}.CODE", filter.getActionTypeCode());
addBetweenRestriction(criteria, "notificationDate", filter.getNotificationDateMinValue(), filter.getNotificationDateMaxValue());
addStringContainsRestriction(criteria, "{alias}.MESSAGE", filter.getMessage());
addBooleanRestriction(criteria, "read", filter.getRead());
addOrder(notifiedCriteria, "email", sorting.getNotifiedEmailOrderType());
addOrder(actionTypeCriteria, "code", sorting.getActionTypeCodeOrderType());
addOrder(criteria, "notificationDate", sorting.getNotificationDateOrderType());
addOrder(criteria, "message", sorting.getMessageOrderType());
addOrder(criteria, "read", sorting.getReadOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

}