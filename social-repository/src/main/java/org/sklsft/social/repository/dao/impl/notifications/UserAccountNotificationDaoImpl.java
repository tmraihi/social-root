package org.sklsft.social.repository.dao.impl.notifications;

import org.sklsft.social.repository.dao.impl.notifications.base.UserAccountNotificationBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.notifications.UserAccountNotificationDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class UserAccountNotificationDaoImpl extends UserAccountNotificationBaseDaoImpl implements UserAccountNotificationDao {

/* Specific Code Start */
/* Specific Code End */
}
