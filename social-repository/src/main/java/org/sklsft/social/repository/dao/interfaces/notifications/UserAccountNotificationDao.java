package org.sklsft.social.repository.dao.interfaces.notifications;

import org.sklsft.social.repository.dao.interfaces.notifications.base.UserAccountNotificationBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface UserAccountNotificationDao extends UserAccountNotificationBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
