package org.sklsft.social.repository.dao.interfaces.nomenclature;

import org.sklsft.social.repository.dao.interfaces.nomenclature.base.KeywordBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface KeywordDao extends KeywordBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
