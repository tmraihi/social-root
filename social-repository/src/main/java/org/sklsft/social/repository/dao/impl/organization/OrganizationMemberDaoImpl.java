package org.sklsft.social.repository.dao.impl.organization;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.impl.organization.base.OrganizationMemberBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class OrganizationMemberDaoImpl extends OrganizationMemberBaseDaoImpl implements OrganizationMemberDao {

/* Specific Code Start */
	
	@Override
	public OrganizationMember loadOrganizationMemberByOrganizationIdAndUserId(Long organizationId, Long userId) {
		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(OrganizationMember.class);
		criteria.add(Restrictions.eq("organization.id", organizationId));
		criteria.add(Restrictions.eq("userAccount.id", userId));
		return (OrganizationMember) criteria.uniqueResult();
	}
	
/* Specific Code End */
}
