package org.sklsft.social.repository.file.impl;

import org.sklsft.social.repository.file.DefaultLogoManager;

public class DefaultLogoManagerMock implements DefaultLogoManager {

	@Override
	public byte[] getDefaultAccountLogo() {
		return null;
	}

	@Override
	public byte[] getDefaultOrganizationLogo() {
		return null;
	}

}
