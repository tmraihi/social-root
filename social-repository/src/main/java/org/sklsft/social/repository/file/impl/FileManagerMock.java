package org.sklsft.social.repository.file.impl;

import org.sklsft.social.interfaces.FileEntity;
import org.sklsft.social.repository.file.FileManager;

public class FileManagerMock implements FileManager {

	@Override
	public void saveFile(byte[] fileData, FileEntity object) {
		
	}

	@Override
	public void deleteFile(FileEntity object) {
		
	}

	@Override
	public boolean existsFile(FileEntity object) {
		
		return false;
	}

	@Override
	public byte[] retrieveFile(FileEntity object) {
		
		return null;
	}
}
