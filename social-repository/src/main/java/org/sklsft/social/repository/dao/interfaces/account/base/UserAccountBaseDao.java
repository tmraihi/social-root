package org.sklsft.social.repository.dao.interfaces.account.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationPreferencesFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationPreferencesSorting;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface UserAccountBaseDao extends BaseDao<UserAccount, Long> {

/**
 * count filtered object list
 */
Long count(UserAccountFilter filter);

/**
 * scroll filtered object list
 */
List<UserAccount> scroll(UserAccountFilter filter, UserAccountSorting sorting, Long firstResult, Long maxResults);

/**
 * load one to many component UserAccountNotificationPreferences list
 */
List<UserAccountNotificationPreferences> loadUserAccountNotificationPreferencesList(Long userAccountId);

/**
 * count one to many component UserAccountNotificationPreferences
 */
Long countUserAccountNotificationPreferences(Long userAccountId);

/**
 * count filtered one to many component UserAccountNotificationPreferences
 */
Long countUserAccountNotificationPreferences(Long userAccountId, UserAccountNotificationPreferencesFilter filter);

/**
 * scroll filtered one to many component UserAccountNotificationPreferences
 */
List<UserAccountNotificationPreferences> scrollUserAccountNotificationPreferences(Long userAccountId, UserAccountNotificationPreferencesFilter filter, UserAccountNotificationPreferencesSorting sorting, Long firstResult, Long maxResults);

/**
 * load one to many component UserAccountNotificationPreferences
 */
UserAccountNotificationPreferences loadUserAccountNotificationPreferences(Long id);

/**
 * exists object
 */
boolean exists(String email);

/**
 * find object or null
 */
UserAccount findOrNull(String email);

/**
 * find object
 */
UserAccount find(String email);

/**
 * search
 */
List<UserAccount> search(String arg);

/**
 * save one to many component UserAccountNotificationPreferences
 */
void saveUserAccountNotificationPreferences(UserAccount userAccount, UserAccountNotificationPreferences userAccountNotificationPreferences);

/**
 * delete one to many component UserAccountNotificationPreferences
 */
void deleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences);

}
