package org.sklsft.social.repository.dao.interfaces.organization;

import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.base.OrganizationMemberBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface OrganizationMemberDao extends OrganizationMemberBaseDao {

/* Specific Code Start */
	
	OrganizationMember loadOrganizationMemberByOrganizationIdAndUserId(Long organizationId, Long userId);
	
/* Specific Code End */
}
