package org.sklsft.social.repository.file.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import org.sklsft.social.interfaces.FileEntity;
import org.sklsft.social.repository.file.FileManager;
import org.sklsft.social.repository.file.exception.FileError;
import org.sklsft.social.repository.file.exception.FileNotFoundException;

import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;
import com.mongodb.gridfs.GridFSInputFile;


public class FileManagerImpl implements FileManager {

	@Inject
	private DB db;
	

	public String getPoolName(FileEntity object) {
		
		return object.getPoolName();
	}
	
	
	public void saveFile(byte[] fileData, Long id, String poolName) {

		GridFS gfs = new GridFS(db, poolName);
		GridFSDBFile findOne = gfs.findOne(String.valueOf(id));
		if (findOne != null) {
			gfs.remove(findOne);
		}
		GridFSInputFile gfsFile = gfs.createFile(fileData);
		String fileName = String.valueOf(id);
		gfsFile.setFilename(fileName);
		
		try {
			gfsFile.save();
		} catch (Exception e) {
			throw new FileError("failed to save file : " + poolName + " " + id);
		}
	}
	

	public void saveFile(byte[] fileData, FileEntity object) {

		saveFile(fileData, object.getId(), getPoolName(object));
	}
	
	
	public void deleteFile(Long id, String poolName) {

		GridFS gfs = new GridFS(db, poolName);
		GridFSDBFile findOne = gfs.findOne(String.valueOf(id));
		if (findOne != null) {
			try {
				gfs.remove(findOne);
			} catch (Exception e) {
				throw new FileError("failed to remove file : " + poolName + " " + id);
			}
		}
	}
	
	
	public void deleteFile(FileEntity object) {

		deleteFile(object.getId(), getPoolName(object));
	}
	
	public boolean existsFile(FileEntity object) {
		return existsFile(object.getId(), getPoolName(object));
	}
	
	
	private boolean existsFile(Long id, String poolName) {
		GridFS gfs = new GridFS(db, poolName);
		GridFSDBFile output = gfs.findOne(String.valueOf(id));
		
		return output != null;
	}


	public byte[] retrieveFile(Long id, String poolName) {
		
		GridFS gfs = new GridFS(db, poolName);
		GridFSDBFile output = gfs.findOne(String.valueOf(id));
		
		if (output == null) {
			throw new FileNotFoundException("file.notFound");
		}
		
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		try {
			output.writeTo(bout);
		} catch (IOException e) {
			throw new FileError("failed to export file : " + poolName + " " + id);
		}
		return bout.toByteArray();
	}
	

	public byte[] retrieveFile(FileEntity object) {

		return retrieveFile(object.getId(), getPoolName(object));
	}
}
