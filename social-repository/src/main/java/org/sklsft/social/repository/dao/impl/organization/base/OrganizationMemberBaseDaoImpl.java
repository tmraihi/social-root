package org.sklsft.social.repository.dao.impl.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.base.OrganizationMemberBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseDaoImpl extends BaseDaoImpl<OrganizationMember, Long> implements OrganizationMemberBaseDao {

/**
 * constructor
 */
public OrganizationMemberBaseDaoImpl() {
super(OrganizationMember.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> loadListFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list eagerly from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> loadListEagerlyFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> loadListFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list eagerly from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> loadListEagerlyFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(OrganizationMemberFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class).setProjection(Projections.rowCount());
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * count object list from organization
 */
public Long countFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class).setProjection(Projections.rowCount());
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
return (Long) criteria.uniqueResult();
}

/**
 * count filtered object list from organization
 */
public Long countFromOrganization(Long organizationId, OrganizationMemberFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class).setProjection(Projections.rowCount());
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * count object list from userAccount
 */
public Long countFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
return (Long) criteria.uniqueResult();
}

/**
 * count filtered object list from userAccount
 */
public Long countFromUserAccount(Long userAccountId, OrganizationMemberFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> scroll(OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * scroll filtered object list from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> scrollFromOrganization(Long organizationId, OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * scroll filtered object list from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationMember> scrollFromUserAccount(Long userAccountId, OrganizationMemberFilter filter, OrganizationMemberSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String organizationCode, String userAccountEmail) {
if (organizationCode == null && userAccountEmail == null) {
return false;
}
OrganizationMember organizationMember = (OrganizationMember)this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class)
.createAlias("organization","organization")
.createAlias("userAccount","userAccount")
.add(Restrictions.eq("organization.code",organizationCode))
.add(Restrictions.eq("userAccount.email",userAccountEmail))
.uniqueResult();
return organizationMember != null;
}

/**
 * find object or null
 */
@Override
public OrganizationMember findOrNull(String organizationCode, String userAccountEmail) {
OrganizationMember organizationMember = (OrganizationMember)this.sessionFactory.getCurrentSession().createCriteria(OrganizationMember.class)
.createAlias("organization","organization")
.createAlias("userAccount","userAccount")
.add(Restrictions.eq("organization.code",organizationCode))
.add(Restrictions.eq("userAccount.email",userAccountEmail))
.uniqueResult();
return organizationMember;
}

/**
 * find object
 */
@Override
public OrganizationMember find(String organizationCode, String userAccountEmail) {
if (organizationCode == null && userAccountEmail == null) {
return null;
}
OrganizationMember organizationMember = findOrNull(organizationCode, userAccountEmail);
if (organizationMember == null) {
throw new ObjectNotFoundException("OrganizationMember.notFound");
} else {
return organizationMember;
}
}

}