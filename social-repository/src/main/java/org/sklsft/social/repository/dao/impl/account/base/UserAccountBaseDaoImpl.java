package org.sklsft.social.repository.dao.impl.account.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationPreferencesFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationPreferencesSorting;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.account.base.UserAccountBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addBetweenRestriction;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addBooleanRestriction;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseDaoImpl extends BaseDaoImpl<UserAccount, Long> implements UserAccountBaseDao {

/**
 * constructor
 */
public UserAccountBaseDaoImpl() {
super(UserAccount.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccount> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(UserAccountFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class).setProjection(Projections.rowCount());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(criteria, "{alias}.FIRST_NAME", filter.getFirstName());
addStringContainsRestriction(criteria, "{alias}.LAST_NAME", filter.getLastName());
addBetweenRestriction(criteria, "creationDate", filter.getCreationDateMinValue(), filter.getCreationDateMaxValue());
addBooleanRestriction(criteria, "activated", filter.getActivated());
addBooleanRestriction(criteria, "emailValidated", filter.getEmailValidated());
addStringContainsRestriction(criteria, "{alias}.USER_ROLE", filter.getUserRole());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccount> scroll(UserAccountFilter filter, UserAccountSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(criteria, "{alias}.FIRST_NAME", filter.getFirstName());
addStringContainsRestriction(criteria, "{alias}.LAST_NAME", filter.getLastName());
addBetweenRestriction(criteria, "creationDate", filter.getCreationDateMinValue(), filter.getCreationDateMaxValue());
addBooleanRestriction(criteria, "activated", filter.getActivated());
addBooleanRestriction(criteria, "emailValidated", filter.getEmailValidated());
addStringContainsRestriction(criteria, "{alias}.USER_ROLE", filter.getUserRole());
addOrder(criteria, "email", sorting.getEmailOrderType());
addOrder(criteria, "firstName", sorting.getFirstNameOrderType());
addOrder(criteria, "lastName", sorting.getLastNameOrderType());
addOrder(criteria, "creationDate", sorting.getCreationDateOrderType());
addOrder(criteria, "activated", sorting.getActivatedOrderType());
addOrder(criteria, "emailValidated", sorting.getEmailValidatedOrderType());
addOrder(criteria, "userRole", sorting.getUserRoleOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load one to many component UserAccountNotificationPreferences list
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccountNotificationPreferences> loadUserAccountNotificationPreferencesList(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotificationPreferences.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count one to many component UserAccountNotificationPreferences
 */
@Override
public Long countUserAccountNotificationPreferences(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotificationPreferences.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
return (Long) criteria.uniqueResult();
}

/**
@Override
 * count filtered one to many component UserAccountNotificationPreferences
 */
public Long countUserAccountNotificationPreferences(Long userAccountId, UserAccountNotificationPreferencesFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotificationPreferences.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria actionTypeCriteria = criteria.createCriteria("actionType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(actionTypeCriteria, "{alias}.CODE", filter.getActionTypeCode());
addStringContainsRestriction(criteria, "{alias}.MAIL_TYPE", filter.getMailType());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered one to many component UserAccountNotificationPreferences
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccountNotificationPreferences> scrollUserAccountNotificationPreferences(Long userAccountId, UserAccountNotificationPreferencesFilter filter, UserAccountNotificationPreferencesSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccountNotificationPreferences.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria actionTypeCriteria = criteria.createCriteria("actionType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(actionTypeCriteria, "{alias}.CODE", filter.getActionTypeCode());
addStringContainsRestriction(criteria, "{alias}.MAIL_TYPE", filter.getMailType());
addOrder(actionTypeCriteria, "code", sorting.getActionTypeCodeOrderType());
addOrder(criteria, "mailType", sorting.getMailTypeOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load one to many component UserAccountNotificationPreferences
 */
@Override
public UserAccountNotificationPreferences loadUserAccountNotificationPreferences(Long id) {
UserAccountNotificationPreferences userAccountNotificationPreferences = (UserAccountNotificationPreferences)this.sessionFactory.getCurrentSession().get(UserAccountNotificationPreferences.class,id);
if (userAccountNotificationPreferences == null) {
throw new ObjectNotFoundException("UserAccountNotificationPreferences.notFound");
} else {
return userAccountNotificationPreferences;
}
}

/**
 * exists object
 */
@Override
public boolean exists(String email) {
if (email == null) {
return false;
}
UserAccount userAccount = (UserAccount)this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class)
.add(Restrictions.eq("email",email))
.uniqueResult();
return userAccount != null;
}

/**
 * find object or null
 */
@Override
public UserAccount findOrNull(String email) {
UserAccount userAccount = (UserAccount)this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class)
.add(Restrictions.eq("email",email))
.uniqueResult();
return userAccount;
}

/**
 * find object
 */
@Override
public UserAccount find(String email) {
if (email == null) {
return null;
}
UserAccount userAccount = findOrNull(email);
if (userAccount == null) {
throw new ObjectNotFoundException("UserAccount.notFound");
} else {
return userAccount;
}
}

/**
 * search
 */
@Override
@SuppressWarnings("unchecked")
public List<UserAccount> search(String arg) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(UserAccount.class);
addStringContainsRestriction(criteria, "{alias}.EMAIL", arg);
criteria.setMaxResults(20);
return criteria.list();
}

/**
 * save one to many component UserAccountNotificationPreferences
 */
@Override
public void saveUserAccountNotificationPreferences(UserAccount userAccount, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferences.setUserAccount(userAccount);
this.sessionFactory.getCurrentSession().save(userAccountNotificationPreferences);
}

/**
 * delete one to many component UserAccountNotificationPreferences
 */
@Override
public void deleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferences.getUserAccount().getUserAccountNotificationPreferencesCollection().remove(userAccountNotificationPreferences);
this.sessionFactory.getCurrentSession().delete(userAccountNotificationPreferences);
}

}