package org.sklsft.social.repository.dao.impl.nomenclature;

import org.sklsft.social.repository.dao.impl.nomenclature.base.KeywordBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.nomenclature.KeywordDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class KeywordDaoImpl extends KeywordBaseDaoImpl implements KeywordDao {

/* Specific Code Start */
/* Specific Code End */
}
