package org.sklsft.social.repository.dao.interfaces.organization.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.organization.filters.OrganizationFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationSorting;
import org.sklsft.social.model.organization.Organization;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationBaseDao extends BaseDao<Organization, Long> {

/**
 * count filtered object list
 */
Long count(OrganizationFilter filter);

/**
 * scroll filtered object list
 */
List<Organization> scroll(OrganizationFilter filter, OrganizationSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String code);

/**
 * find object or null
 */
Organization findOrNull(String code);

/**
 * find object
 */
Organization find(String code);

/**
 * search
 */
List<Organization> search(String arg);

}
