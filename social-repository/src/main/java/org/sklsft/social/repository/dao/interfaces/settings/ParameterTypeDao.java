package org.sklsft.social.repository.dao.interfaces.settings;

import org.sklsft.social.repository.dao.interfaces.settings.base.ParameterTypeBaseDao;

/**
 * auto generated dao interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface ParameterTypeDao extends ParameterTypeBaseDao {

/* Specific Code Start */
/* Specific Code End */
}
