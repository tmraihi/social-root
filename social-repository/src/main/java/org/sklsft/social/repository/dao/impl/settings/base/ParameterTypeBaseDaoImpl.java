package org.sklsft.social.repository.dao.impl.settings.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.model.settings.ParameterType;
import org.sklsft.social.repository.dao.interfaces.settings.base.ParameterTypeBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseDaoImpl extends BaseDaoImpl<ParameterType, Long> implements ParameterTypeBaseDao {

/**
 * constructor
 */
public ParameterTypeBaseDaoImpl() {
super(ParameterType.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<ParameterType> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ParameterType.class);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(ParameterTypeFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ParameterType.class).setProjection(Projections.rowCount());
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<ParameterType> scroll(ParameterTypeFilter filter, ParameterTypeSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(ParameterType.class);
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
addOrder(criteria, "code", sorting.getCodeOrderType());
addOrder(criteria, "description", sorting.getDescriptionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String code) {
if (code == null) {
return false;
}
ParameterType parameterType = (ParameterType)this.sessionFactory.getCurrentSession().createCriteria(ParameterType.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return parameterType != null;
}

/**
 * find object or null
 */
@Override
public ParameterType findOrNull(String code) {
ParameterType parameterType = (ParameterType)this.sessionFactory.getCurrentSession().createCriteria(ParameterType.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return parameterType;
}

/**
 * find object
 */
@Override
public ParameterType find(String code) {
if (code == null) {
return null;
}
ParameterType parameterType = findOrNull(code);
if (parameterType == null) {
throw new ObjectNotFoundException("ParameterType.notFound");
} else {
return parameterType;
}
}

}