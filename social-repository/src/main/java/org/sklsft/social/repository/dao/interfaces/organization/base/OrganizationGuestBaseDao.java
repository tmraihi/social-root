package org.sklsft.social.repository.dao.interfaces.organization.base;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.model.patterns.BaseDao;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.model.organization.OrganizationGuest;
/**
 * auto generated base dao interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationGuestBaseDao extends BaseDao<OrganizationGuest, Long> {

/**
 * load object list from organization
 */
List<OrganizationGuest> loadListFromOrganization(Long organizationId);

/**
 * load object list eagerly from organization
 */
List<OrganizationGuest> loadListEagerlyFromOrganization(Long organizationId);

/**
 * load object list from userAccount
 */
List<OrganizationGuest> loadListFromUserAccount(Long userAccountId);

/**
 * load object list eagerly from userAccount
 */
List<OrganizationGuest> loadListEagerlyFromUserAccount(Long userAccountId);

/**
 * count filtered object list
 */
Long count(OrganizationGuestFilter filter);

/**
 * count object list from organization
 */
Long countFromOrganization(Long organizationId);

/**
 * count filtered object list from organization
 */
Long countFromOrganization(Long organizationId, OrganizationGuestFilter filter);

/**
 * count object list from userAccount
 */
Long countFromUserAccount(Long userAccountId);

/**
 * count filtered object list from userAccount
 */
Long countFromUserAccount(Long userAccountId, OrganizationGuestFilter filter);

/**
 * scroll filtered object list
 */
List<OrganizationGuest> scroll(OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults);

/**
 * scroll filtered object from organization
 */
List<OrganizationGuest> scrollFromOrganization(Long organizationId, OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults);

/**
 * scroll filtered object from userAccount
 */
List<OrganizationGuest> scrollFromUserAccount(Long userAccountId, OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults);

/**
 * exists object
 */
boolean exists(String organizationCode, String email);

/**
 * find object or null
 */
OrganizationGuest findOrNull(String organizationCode, String email);

/**
 * find object
 */
OrganizationGuest find(String organizationCode, String email);

}
