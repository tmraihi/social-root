package org.sklsft.social.repository.dao.impl.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.organization.filters.OrganizationFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationSorting;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.repository.dao.interfaces.organization.base.OrganizationBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBaseDaoImpl extends BaseDaoImpl<Organization, Long> implements OrganizationBaseDao {

/**
 * constructor
 */
public OrganizationBaseDaoImpl() {
super(Organization.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<Organization> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organization.class);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(OrganizationFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organization.class).setProjection(Projections.rowCount());
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.NAME", filter.getName());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<Organization> scroll(OrganizationFilter filter, OrganizationSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organization.class);
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.NAME", filter.getName());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
addOrder(criteria, "code", sorting.getCodeOrderType());
addOrder(criteria, "name", sorting.getNameOrderType());
addOrder(criteria, "description", sorting.getDescriptionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String code) {
if (code == null) {
return false;
}
Organization organization = (Organization)this.sessionFactory.getCurrentSession().createCriteria(Organization.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return organization != null;
}

/**
 * find object or null
 */
@Override
public Organization findOrNull(String code) {
Organization organization = (Organization)this.sessionFactory.getCurrentSession().createCriteria(Organization.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return organization;
}

/**
 * find object
 */
@Override
public Organization find(String code) {
if (code == null) {
return null;
}
Organization organization = findOrNull(code);
if (organization == null) {
throw new ObjectNotFoundException("Organization.notFound");
} else {
return organization;
}
}

/**
 * search
 */
@Override
@SuppressWarnings("unchecked")
public List<Organization> search(String arg) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Organization.class);
addStringContainsRestriction(criteria, "{alias}.CODE", arg);
criteria.setMaxResults(20);
return criteria.list();
}

}