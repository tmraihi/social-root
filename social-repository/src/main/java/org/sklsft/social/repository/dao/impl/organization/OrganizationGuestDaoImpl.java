package org.sklsft.social.repository.dao.impl.organization;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.impl.organization.base.OrganizationGuestBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class OrganizationGuestDaoImpl extends OrganizationGuestBaseDaoImpl implements OrganizationGuestDao {

/* Specific Code Start */
	
	@Override
	public OrganizationGuest findByOrganizationAndUser(Long organizationId, Long userId) {
		Criteria criteria = this.sessionFactory.getCurrentSession()
				.createCriteria(OrganizationGuest.class);
		criteria.add(Restrictions.eq("organization.id", organizationId));
		criteria.add(Restrictions.eq("userAccount.id", userId));
		return (OrganizationGuest) criteria.uniqueResult();
	}
	
/* Specific Code End */
}
