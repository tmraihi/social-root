package org.sklsft.social.repository.dao.impl.nomenclature.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.model.nomenclature.Keyword;
import org.sklsft.social.repository.dao.interfaces.nomenclature.base.KeywordBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseDaoImpl extends BaseDaoImpl<Keyword, Long> implements KeywordBaseDao {

/**
 * constructor
 */
public KeywordBaseDaoImpl() {
super(Keyword.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<Keyword> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Keyword.class);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(KeywordFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Keyword.class).setProjection(Projections.rowCount());
addStringContainsRestriction(criteria, "{alias}.CANONICAL_LABEL", filter.getCanonicalLabel());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<Keyword> scroll(KeywordFilter filter, KeywordSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Keyword.class);
addStringContainsRestriction(criteria, "{alias}.CANONICAL_LABEL", filter.getCanonicalLabel());
addOrder(criteria, "canonicalLabel", sorting.getCanonicalLabelOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String canonicalLabel) {
if (canonicalLabel == null) {
return false;
}
Keyword keyword = (Keyword)this.sessionFactory.getCurrentSession().createCriteria(Keyword.class)
.add(Restrictions.eq("canonicalLabel",canonicalLabel))
.uniqueResult();
return keyword != null;
}

/**
 * find object or null
 */
@Override
public Keyword findOrNull(String canonicalLabel) {
Keyword keyword = (Keyword)this.sessionFactory.getCurrentSession().createCriteria(Keyword.class)
.add(Restrictions.eq("canonicalLabel",canonicalLabel))
.uniqueResult();
return keyword;
}

/**
 * find object
 */
@Override
public Keyword find(String canonicalLabel) {
if (canonicalLabel == null) {
return null;
}
Keyword keyword = findOrNull(canonicalLabel);
if (keyword == null) {
throw new ObjectNotFoundException("Keyword.notFound");
} else {
return keyword;
}
}

/**
 * search
 */
@Override
@SuppressWarnings("unchecked")
public List<Keyword> search(String arg) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(Keyword.class);
addStringContainsRestriction(criteria, "{alias}.CANONICAL_LABEL", arg);
criteria.setMaxResults(20);
return criteria.list();
}

}