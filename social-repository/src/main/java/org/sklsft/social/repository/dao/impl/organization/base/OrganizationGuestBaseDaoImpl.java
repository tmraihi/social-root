package org.sklsft.social.repository.dao.impl.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.organization.base.OrganizationGuestBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseDaoImpl extends BaseDaoImpl<OrganizationGuest, Long> implements OrganizationGuestBaseDao {

/**
 * constructor
 */
public OrganizationGuestBaseDaoImpl() {
super(OrganizationGuest.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> loadListFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list eagerly from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> loadListEagerlyFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> loadListFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * load object list eagerly from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> loadListEagerlyFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
criteria.setFetchMode("organization",FetchMode.JOIN);
criteria.setFetchMode("userAccount",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(OrganizationGuestFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class).setProjection(Projections.rowCount());
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * count object list from organization
 */
public Long countFromOrganization(Long organizationId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class).setProjection(Projections.rowCount());
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
return (Long) criteria.uniqueResult();
}

/**
 * count filtered object list from organization
 */
public Long countFromOrganization(Long organizationId, OrganizationGuestFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class).setProjection(Projections.rowCount());
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * count object list from userAccount
 */
public Long countFromUserAccount(Long userAccountId) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
return (Long) criteria.uniqueResult();
}

/**
 * count filtered object list from userAccount
 */
public Long countFromUserAccount(Long userAccountId, OrganizationGuestFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class).setProjection(Projections.rowCount());
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> scroll(OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(criteria, "email", sorting.getEmailOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * scroll filtered object list from organization
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> scrollFromOrganization(Long organizationId, OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (organizationId == null){
criteria.add(Restrictions.isNull("organization.id"));
} else {
criteria.add(Restrictions.eq("organization.id", organizationId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(criteria, "email", sorting.getEmailOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * scroll filtered object list from userAccount
 */
@Override
@SuppressWarnings("unchecked")
public List<OrganizationGuest> scrollFromUserAccount(Long userAccountId, OrganizationGuestFilter filter, OrganizationGuestSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class);
if (userAccountId == null){
criteria.add(Restrictions.isNull("userAccount.id"));
} else {
criteria.add(Restrictions.eq("userAccount.id", userAccountId));
}
Criteria organizationCriteria = criteria.createCriteria("organization", JoinType.LEFT_OUTER_JOIN);
Criteria userAccountCriteria = criteria.createCriteria("userAccount", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(organizationCriteria, "{alias}.CODE", filter.getOrganizationCode());
addStringContainsRestriction(criteria, "{alias}.EMAIL", filter.getEmail());
addStringContainsRestriction(userAccountCriteria, "{alias}.EMAIL", filter.getUserAccountEmail());
addStringContainsRestriction(criteria, "{alias}.ROLE", filter.getRole());
addStringContainsRestriction(criteria, "{alias}.POSITION", filter.getPosition());
addOrder(organizationCriteria, "code", sorting.getOrganizationCodeOrderType());
addOrder(criteria, "email", sorting.getEmailOrderType());
addOrder(userAccountCriteria, "email", sorting.getUserAccountEmailOrderType());
addOrder(criteria, "role", sorting.getRoleOrderType());
addOrder(criteria, "position", sorting.getPositionOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String organizationCode, String email) {
if (organizationCode == null && email == null) {
return false;
}
OrganizationGuest organizationGuest = (OrganizationGuest)this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class)
.createAlias("organization","organization")
.add(Restrictions.eq("organization.code",organizationCode))
.add(Restrictions.eq("email",email))
.uniqueResult();
return organizationGuest != null;
}

/**
 * find object or null
 */
@Override
public OrganizationGuest findOrNull(String organizationCode, String email) {
OrganizationGuest organizationGuest = (OrganizationGuest)this.sessionFactory.getCurrentSession().createCriteria(OrganizationGuest.class)
.createAlias("organization","organization")
.add(Restrictions.eq("organization.code",organizationCode))
.add(Restrictions.eq("email",email))
.uniqueResult();
return organizationGuest;
}

/**
 * find object
 */
@Override
public OrganizationGuest find(String organizationCode, String email) {
if (organizationCode == null && email == null) {
return null;
}
OrganizationGuest organizationGuest = findOrNull(organizationCode, email);
if (organizationGuest == null) {
throw new ObjectNotFoundException("OrganizationGuest.notFound");
} else {
return organizationGuest;
}
}

}