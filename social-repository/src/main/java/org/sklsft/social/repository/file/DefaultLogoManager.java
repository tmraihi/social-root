package org.sklsft.social.repository.file;



public interface DefaultLogoManager {

	public byte[] getDefaultAccountLogo();
	
	public byte[] getDefaultOrganizationLogo();
}
