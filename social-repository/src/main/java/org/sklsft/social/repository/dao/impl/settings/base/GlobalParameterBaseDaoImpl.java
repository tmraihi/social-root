package org.sklsft.social.repository.dao.impl.settings.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.api.model.OrderType;
import org.sklsft.commons.model.patterns.BaseDaoImpl;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.base.GlobalParameterBaseDao;
import org.springframework.stereotype.Repository;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.*;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addOrder;
import static org.sklsft.commons.model.patterns.HibernateCriteriaUtils.addStringContainsRestriction;

/**
 * auto generated base dao class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseDaoImpl extends BaseDaoImpl<GlobalParameter, Long> implements GlobalParameterBaseDao {

/**
 * constructor
 */
public GlobalParameterBaseDaoImpl() {
super(GlobalParameter.class);
}
/**
 * load object list eagerly
 */
@Override
@SuppressWarnings("unchecked")
public List<GlobalParameter> loadListEagerly() {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(GlobalParameter.class);
criteria.setFetchMode("parameterType",FetchMode.JOIN);
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * count filtered object list
 */
@Override
public Long count(GlobalParameterFilter filter) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(GlobalParameter.class).setProjection(Projections.rowCount());
Criteria parameterTypeCriteria = criteria.createCriteria("parameterType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
addStringContainsRestriction(parameterTypeCriteria, "{alias}.CODE", filter.getParameterTypeCode());
addStringContainsRestriction(criteria, "{alias}.VALUE", filter.getValue());
return (Long) criteria.uniqueResult();
}

/**
 * scroll filtered object list
 */
@Override
@SuppressWarnings("unchecked")
public List<GlobalParameter> scroll(GlobalParameterFilter filter, GlobalParameterSorting sorting, Long firstResult, Long maxResults) {
Criteria criteria = this.sessionFactory.getCurrentSession().createCriteria(GlobalParameter.class);
Criteria parameterTypeCriteria = criteria.createCriteria("parameterType", JoinType.LEFT_OUTER_JOIN);
addStringContainsRestriction(criteria, "{alias}.CODE", filter.getCode());
addStringContainsRestriction(criteria, "{alias}.DESCRIPTION", filter.getDescription());
addStringContainsRestriction(parameterTypeCriteria, "{alias}.CODE", filter.getParameterTypeCode());
addStringContainsRestriction(criteria, "{alias}.VALUE", filter.getValue());
addOrder(criteria, "code", sorting.getCodeOrderType());
addOrder(criteria, "description", sorting.getDescriptionOrderType());
addOrder(parameterTypeCriteria, "code", sorting.getParameterTypeCodeOrderType());
addOrder(criteria, "value", sorting.getValueOrderType());
if (firstResult != null){
criteria.setFirstResult(firstResult.intValue());
}
if (maxResults != null){
criteria.setMaxResults(maxResults.intValue());
}
addOrder(criteria, "id", OrderType.DESC);
return criteria.list();
}

/**
 * exists object
 */
@Override
public boolean exists(String code) {
if (code == null) {
return false;
}
GlobalParameter globalParameter = (GlobalParameter)this.sessionFactory.getCurrentSession().createCriteria(GlobalParameter.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return globalParameter != null;
}

/**
 * find object or null
 */
@Override
public GlobalParameter findOrNull(String code) {
GlobalParameter globalParameter = (GlobalParameter)this.sessionFactory.getCurrentSession().createCriteria(GlobalParameter.class)
.add(Restrictions.eq("code",code))
.uniqueResult();
return globalParameter;
}

/**
 * find object
 */
@Override
public GlobalParameter find(String code) {
if (code == null) {
return null;
}
GlobalParameter globalParameter = findOrNull(code);
if (globalParameter == null) {
throw new ObjectNotFoundException("GlobalParameter.notFound");
} else {
return globalParameter;
}
}

}