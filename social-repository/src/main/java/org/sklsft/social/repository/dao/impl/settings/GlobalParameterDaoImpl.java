package org.sklsft.social.repository.dao.impl.settings;

import org.sklsft.social.repository.dao.impl.settings.base.GlobalParameterBaseDaoImpl;
import org.sklsft.social.repository.dao.interfaces.settings.GlobalParameterDao;
import org.springframework.stereotype.Repository;

/**
 * auto generated dao class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Repository
public class GlobalParameterDaoImpl extends GlobalParameterBaseDaoImpl implements GlobalParameterDao {

/* Specific Code Start */
/* Specific Code End */
}
