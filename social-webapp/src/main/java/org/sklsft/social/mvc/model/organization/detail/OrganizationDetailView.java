package org.sklsft.social.mvc.model.organization.detail;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationFullView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(ViewScope.NAME)
public class OrganizationDetailView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrganizationFullView selectedOrganization = new OrganizationFullView();

protected ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> organizationMemberScrollForm = new ScrollForm<>();
protected ScrollView<OrganizationMemberBasicView> organizationMemberScrollView = new ScrollView<>();
private OrganizationMemberFullView selectedOrganizationMember = new OrganizationMemberFullView();

protected ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> organizationGuestScrollForm = new ScrollForm<>();
protected ScrollView<OrganizationGuestBasicView> organizationGuestScrollView = new ScrollView<>();
private OrganizationGuestFullView selectedOrganizationGuest = new OrganizationGuestFullView();

/*
 * getters and setters
 */
public OrganizationFullView getSelectedOrganization() {
return selectedOrganization;
}
public void setSelectedOrganization(OrganizationFullView selectedOrganization) {
this.selectedOrganization = selectedOrganization;
}

public ScrollView<OrganizationMemberBasicView> getOrganizationMemberScrollView() {
return organizationMemberScrollView;
}
public void setOrganizationMemberScrollView(ScrollView<OrganizationMemberBasicView> organizationMemberScrollView) {
this.organizationMemberScrollView = organizationMemberScrollView;
}

public ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> getOrganizationMemberScrollForm() {
return organizationMemberScrollForm;
}
public void setOrganizationMemberScrollForm(ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> organizationMemberScrollForm) {
this.organizationMemberScrollForm = organizationMemberScrollForm;
}

public OrganizationMemberFullView getSelectedOrganizationMember() {
return selectedOrganizationMember;
}
public void setSelectedOrganizationMember(OrganizationMemberFullView selectedOrganizationMember) {
this.selectedOrganizationMember = selectedOrganizationMember;
}

public ScrollView<OrganizationGuestBasicView> getOrganizationGuestScrollView() {
return organizationGuestScrollView;
}
public void setOrganizationGuestScrollView(ScrollView<OrganizationGuestBasicView> organizationGuestScrollView) {
this.organizationGuestScrollView = organizationGuestScrollView;
}

public ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> getOrganizationGuestScrollForm() {
return organizationGuestScrollForm;
}
public void setOrganizationGuestScrollForm(ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> organizationGuestScrollForm) {
this.organizationGuestScrollForm = organizationGuestScrollForm;
}

public OrganizationGuestFullView getSelectedOrganizationGuest() {
return selectedOrganizationGuest;
}
public void setSelectedOrganizationGuest(OrganizationGuestFullView selectedOrganizationGuest) {
this.selectedOrganizationGuest = selectedOrganizationGuest;
}

/* Specific Code Start */
/* Specific Code End */
}
