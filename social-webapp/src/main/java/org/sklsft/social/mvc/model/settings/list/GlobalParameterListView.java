package org.sklsft.social.mvc.model.settings.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.api.model.settings.views.basic.GlobalParameterBasicView;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class GlobalParameterListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<GlobalParameterFilter, GlobalParameterSorting> scrollForm = new ScrollForm<>();
protected ScrollView<GlobalParameterBasicView> scrollView = new ScrollView<>();
protected GlobalParameterFullView selectedGlobalParameter = new GlobalParameterFullView();

/*
 * getters and setters
 */
public ScrollView<GlobalParameterBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<GlobalParameterBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<GlobalParameterFilter, GlobalParameterSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<GlobalParameterFilter, GlobalParameterSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public GlobalParameterFullView getSelectedGlobalParameter() {
return selectedGlobalParameter;
}
public void setSelectedGlobalParameter(GlobalParameterFullView selectedGlobalParameter) {
this.selectedGlobalParameter = selectedGlobalParameter;
}

/* Specific Code Start */
/* Specific Code End */
}
