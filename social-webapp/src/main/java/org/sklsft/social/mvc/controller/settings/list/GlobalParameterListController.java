package org.sklsft.social.mvc.controller.settings.list;

import org.sklsft.social.mvc.controller.settings.list.base.GlobalParameterBaseListController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list controller class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class GlobalParameterListController extends  GlobalParameterBaseListController {

/* Specific Code Start */
/* Specific Code End */
}
