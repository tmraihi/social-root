package org.sklsft.social.mvc.controller.organization.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.organization.detail.OrganizationMemberDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected OrganizationMemberService organizationMemberService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected OrganizationMemberDetailView organizationMemberDetailView;

/**
 * load object
 */
public void load() {
organizationMemberDetailView.setSelectedOrganizationMember(this.organizationMemberService.load(this.organizationMemberDetailView.getSelectedOrganizationMember().getId()));
}


/**
 * update object
 */
@AjaxMethod("OrganizationMember.update")
public void update() {
organizationMemberService.update(organizationMemberDetailView.getSelectedOrganizationMember().getId(), organizationMemberDetailView.getSelectedOrganizationMember().getForm());
load();
}

}
