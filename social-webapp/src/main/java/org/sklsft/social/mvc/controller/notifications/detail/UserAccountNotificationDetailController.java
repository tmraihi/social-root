package org.sklsft.social.mvc.controller.notifications.detail;

import org.sklsft.social.mvc.controller.notifications.detail.base.UserAccountNotificationBaseDetailController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail controller class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class UserAccountNotificationDetailController extends  UserAccountNotificationBaseDetailController {

/* Specific Code Start */
/* Specific Code End */
}
