package org.sklsft.social.mvc.model.settings.detail;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(ViewScope.NAME)
public class GlobalParameterDetailView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private GlobalParameterFullView selectedGlobalParameter = new GlobalParameterFullView();

/*
 * getters and setters
 */
public GlobalParameterFullView getSelectedGlobalParameter() {
return selectedGlobalParameter;
}
public void setSelectedGlobalParameter(GlobalParameterFullView selectedGlobalParameter) {
this.selectedGlobalParameter = selectedGlobalParameter;
}

/* Specific Code Start */
/* Specific Code End */
}
