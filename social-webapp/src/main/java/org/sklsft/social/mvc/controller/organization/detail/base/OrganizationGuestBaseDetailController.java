package org.sklsft.social.mvc.controller.organization.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.organization.OrganizationGuestService;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.organization.detail.OrganizationGuestDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected OrganizationGuestService organizationGuestService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected OrganizationGuestDetailView organizationGuestDetailView;

/**
 * load object
 */
public void load() {
organizationGuestDetailView.setSelectedOrganizationGuest(this.organizationGuestService.load(this.organizationGuestDetailView.getSelectedOrganizationGuest().getId()));
}


/**
 * update object
 */
@AjaxMethod("OrganizationGuest.update")
public void update() {
organizationGuestService.update(organizationGuestDetailView.getSelectedOrganizationGuest().getId(), organizationGuestDetailView.getSelectedOrganizationGuest().getForm());
load();
}

}
