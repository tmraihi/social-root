package org.sklsft.social.mvc.model.organization.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.organization.filters.OrganizationFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class OrganizationListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<OrganizationFilter, OrganizationSorting> scrollForm = new ScrollForm<>();
protected ScrollView<OrganizationBasicView> scrollView = new ScrollView<>();
protected OrganizationFullView selectedOrganization = new OrganizationFullView();

/*
 * getters and setters
 */
public ScrollView<OrganizationBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<OrganizationBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<OrganizationFilter, OrganizationSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<OrganizationFilter, OrganizationSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public OrganizationFullView getSelectedOrganization() {
return selectedOrganization;
}
public void setSelectedOrganization(OrganizationFullView selectedOrganization) {
this.selectedOrganization = selectedOrganization;
}

/* Specific Code Start */
/* Specific Code End */
}
