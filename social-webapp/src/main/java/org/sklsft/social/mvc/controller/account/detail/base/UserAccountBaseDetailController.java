package org.sklsft.social.mvc.controller.account.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.interfaces.organization.OrganizationGuestService;
import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationPreferencesFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationPreferencesSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationPreferencesBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationPreferencesFullView;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.account.detail.UserAccountDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected UserAccountService userAccountService;
@Inject
protected OrganizationMemberService organizationMemberService;
@Inject
protected OrganizationGuestService organizationGuestService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected UserAccountDetailView userAccountDetailView;

/**
 * load object
 */
public void load() {
userAccountDetailView.setSelectedUserAccount(this.userAccountService.load(this.userAccountDetailView.getSelectedUserAccount().getId()));
}


/**
 * load one to many component userAccountNotificationPreferences list
 */
public void loadUserAccountNotificationPreferencesList() {
this.resetUserAccountNotificationPreferencesFilter();
}

/**
 * refresh one to many userAccountNotificationPreferences list
 */
public void refreshUserAccountNotificationPreferencesList() {
userAccountDetailView.setUserAccountNotificationPreferencesScrollView(userAccountService.scrollUserAccountNotificationPreferences(userAccountDetailView.getSelectedUserAccount().getId(), userAccountDetailView.getUserAccountNotificationPreferencesScrollForm()));
userAccountDetailView.getUserAccountNotificationPreferencesScrollForm().setPage(userAccountDetailView.getUserAccountNotificationPreferencesScrollView().getCurrentPage());
}

/**
 * load one to many organizationMember list
 */
public void loadOrganizationMemberList() {
this.resetOrganizationMemberFilter();
}

/**
 * refresh one to many organizationMember list
 */
public void refreshOrganizationMemberList() {
userAccountDetailView.setOrganizationMemberScrollView(organizationMemberService.scrollFromUserAccount(userAccountDetailView.getSelectedUserAccount().getId(), userAccountDetailView.getOrganizationMemberScrollForm()));
userAccountDetailView.getOrganizationMemberScrollForm().setPage(userAccountDetailView.getOrganizationMemberScrollView().getCurrentPage());
}

/**
 * load one to many organizationGuest list
 */
public void loadOrganizationGuestList() {
this.resetOrganizationGuestFilter();
}

/**
 * refresh one to many organizationGuest list
 */
public void refreshOrganizationGuestList() {
userAccountDetailView.setOrganizationGuestScrollView(organizationGuestService.scrollFromUserAccount(userAccountDetailView.getSelectedUserAccount().getId(), userAccountDetailView.getOrganizationGuestScrollForm()));
userAccountDetailView.getOrganizationGuestScrollForm().setPage(userAccountDetailView.getOrganizationGuestScrollView().getCurrentPage());
}

/**
 * create one to many component userAccountNotificationPreferences
 */
public void createUserAccountNotificationPreferences() {
this.commonController.loadActionTypeOptions();
try {
userAccountDetailView.setSelectedUserAccountNotificationPreferences(this.userAccountService.createUserAccountNotificationPreferences(this.userAccountDetailView.getSelectedUserAccount().getId()));
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * create one to many organizationMember
 */
public void createOrganizationMember() {
try {
userAccountDetailView.setSelectedOrganizationMember(this.organizationMemberService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * create one to many organizationGuest
 */
public void createOrganizationGuest() {
try {
userAccountDetailView.setSelectedOrganizationGuest(this.organizationGuestService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save one to many component userAccountNotificationPreferences
 */
@AjaxMethod("UserAccountNotificationPreferences.save")
public void saveUserAccountNotificationPreferences() {
userAccountService.saveUserAccountNotificationPreferences(this.userAccountDetailView.getSelectedUserAccount().getId(), userAccountDetailView.getSelectedUserAccountNotificationPreferences().getForm());
loadUserAccountNotificationPreferencesList();
}

/**
 * save one to many organizationMember
 */
@AjaxMethod("OrganizationMember.save")
public void saveOrganizationMember() {
load();
OrganizationMemberForm form = userAccountDetailView.getSelectedOrganizationMember().getForm();
UserAccountForm userAccountForm = userAccountDetailView.getSelectedUserAccount().getForm();
form.setUserAccountEmail(userAccountForm.getEmail());
organizationMemberService.save(form);
refreshOrganizationMemberList();
}

/**
 * save one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.save")
public void saveOrganizationGuest() {
load();
OrganizationGuestForm form = userAccountDetailView.getSelectedOrganizationGuest().getForm();
UserAccountForm userAccountForm = userAccountDetailView.getSelectedUserAccount().getForm();
form.setUserAccountEmail(userAccountForm.getEmail());
organizationGuestService.save(form);
refreshOrganizationGuestList();
}

/**
 * update object
 */
@AjaxMethod("UserAccount.update")
public void update() {
userAccountService.update(userAccountDetailView.getSelectedUserAccount().getId(), userAccountDetailView.getSelectedUserAccount().getForm());
load();
}

/**
 * edit one to many organizationMember
 */
public void editOrganizationMember(Long id) {
userAccountDetailView.setSelectedOrganizationMember(organizationMemberService.load(id));
}

/**
 * edit one to many organizationGuest
 */
public void editOrganizationGuest(Long id) {
userAccountDetailView.setSelectedOrganizationGuest(organizationGuestService.load(id));
}

/**
 * update one to many organizationMember
 */
@AjaxMethod("OrganizationMember.update")
public void updateOrganizationMember() {
organizationMemberService.update(userAccountDetailView.getSelectedOrganizationMember().getId(), userAccountDetailView.getSelectedOrganizationMember().getForm());
refreshOrganizationMemberList();
}

/**
 * update one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.update")
public void updateOrganizationGuest() {
organizationGuestService.update(userAccountDetailView.getSelectedOrganizationGuest().getId(), userAccountDetailView.getSelectedOrganizationGuest().getForm());
refreshOrganizationGuestList();
}

/**
 * edit one to many component userAccountNotificationPreferences
 */
public void editUserAccountNotificationPreferences(Long id) {
this.commonController.loadActionTypeOptions();
userAccountDetailView.setSelectedUserAccountNotificationPreferences(userAccountService.loadUserAccountNotificationPreferences(id));
}

/**
 * update one to many component userAccountNotificationPreferences
 */
@AjaxMethod("UserAccountNotificationPreferences.update")
public void updateUserAccountNotificationPreferences() {
userAccountService.updateUserAccountNotificationPreferences(userAccountDetailView.getSelectedUserAccountNotificationPreferences().getId(), userAccountDetailView.getSelectedUserAccountNotificationPreferences().getForm());
loadUserAccountNotificationPreferencesList();
}

/**
 * delete one to many component userAccountNotificationPreferences
 */
@AjaxMethod("UserAccountNotificationPreferences.delete")
public void deleteUserAccountNotificationPreferences(Long id) {
userAccountService.deleteUserAccountNotificationPreferences(id);
loadUserAccountNotificationPreferencesList();
}

/**
 * delete one to many organizationMember
 */
@AjaxMethod("OrganizationMember.delete")
public void deleteOrganizationMember(Long id) {
organizationMemberService.delete(id);
refreshOrganizationMemberList();
}

/**
 * delete one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.delete")
public void deleteOrganizationGuest(Long id) {
organizationGuestService.delete(id);
refreshOrganizationGuestList();
}

/**
 * delete one to many component userAccountNotificationPreferences list
 */
@AjaxMethod("UserAccountNotificationPreferences.deleteList")
public void deleteUserAccountNotificationPreferencesList() {
List<Long> ids = new ArrayList<>();
for (UserAccountNotificationPreferencesBasicView userAccountNotificationPreferences:userAccountDetailView.getUserAccountNotificationPreferencesScrollView().getElements()) {
if (userAccountNotificationPreferences.getSelected()) {
ids.add(userAccountNotificationPreferences.getId());
}
}
userAccountService.deleteUserAccountNotificationPreferencesList(ids);
loadUserAccountNotificationPreferencesList();
}

/**
 * delete one to many organizationMember list
 */
@AjaxMethod("OrganizationMember.deleteList")
public void deleteOrganizationMemberList() {
List<Long> ids = new ArrayList<>();
for (OrganizationMemberBasicView organizationMember:userAccountDetailView.getOrganizationMemberScrollView().getElements()) {
if (organizationMember.getSelected()) {
ids.add(organizationMember.getId());
}
}
organizationMemberService.deleteList(ids);
refreshOrganizationMemberList();
}

/**
 * delete one to many organizationGuest list
 */
@AjaxMethod("OrganizationGuest.deleteList")
public void deleteOrganizationGuestList() {
List<Long> ids = new ArrayList<>();
for (OrganizationGuestBasicView organizationGuest:userAccountDetailView.getOrganizationGuestScrollView().getElements()) {
if (organizationGuest.getSelected()) {
ids.add(organizationGuest.getId());
}
}
organizationGuestService.deleteList(ids);
refreshOrganizationGuestList();
}

/**
 * reset one to many component UserAccountNotificationPreferencesFilter datatable filter
 */
public void resetUserAccountNotificationPreferencesFilter() {
this.userAccountDetailView.setUserAccountNotificationPreferencesScrollForm(new ScrollForm<>());
this.userAccountDetailView.getUserAccountNotificationPreferencesScrollForm().setFilter(new UserAccountNotificationPreferencesFilter());
this.userAccountDetailView.getUserAccountNotificationPreferencesScrollForm().setSorting(new UserAccountNotificationPreferencesSorting());
refreshUserAccountNotificationPreferencesList();
}

/**
 * reset one to many OrganizationMemberFilter datatable filter and sorting
 */
public void resetOrganizationMemberFilter() {
this.userAccountDetailView.setOrganizationMemberScrollForm(new ScrollForm<>());
this.userAccountDetailView.getOrganizationMemberScrollForm().setFilter(new OrganizationMemberFilter());
this.userAccountDetailView.getOrganizationMemberScrollForm().setSorting(new OrganizationMemberSorting());
refreshOrganizationMemberList();
}

/**
 * reset one to many OrganizationGuestFilter datatable filter and sorting
 */
public void resetOrganizationGuestFilter() {
this.userAccountDetailView.setOrganizationGuestScrollForm(new ScrollForm<>());
this.userAccountDetailView.getOrganizationGuestScrollForm().setFilter(new OrganizationGuestFilter());
this.userAccountDetailView.getOrganizationGuestScrollForm().setSorting(new OrganizationGuestSorting());
refreshOrganizationGuestList();
}

}
