package org.sklsft.social.mvc.model.settings.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.api.model.settings.views.basic.ParameterTypeBasicView;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class ParameterTypeListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<ParameterTypeFilter, ParameterTypeSorting> scrollForm = new ScrollForm<>();
protected ScrollView<ParameterTypeBasicView> scrollView = new ScrollView<>();
protected ParameterTypeFullView selectedParameterType = new ParameterTypeFullView();

/*
 * getters and setters
 */
public ScrollView<ParameterTypeBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<ParameterTypeBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<ParameterTypeFilter, ParameterTypeSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<ParameterTypeFilter, ParameterTypeSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public ParameterTypeFullView getSelectedParameterType() {
return selectedParameterType;
}
public void setSelectedParameterType(ParameterTypeFullView selectedParameterType) {
this.selectedParameterType = selectedParameterType;
}

/* Specific Code Start */
/* Specific Code End */
}
