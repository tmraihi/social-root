package org.sklsft.social.mvc.model.notifications.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.api.model.notifications.views.basic.ActionTypeBasicView;
import org.sklsft.social.api.model.notifications.views.full.ActionTypeFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class ActionTypeListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<ActionTypeFilter, ActionTypeSorting> scrollForm = new ScrollForm<>();
protected ScrollView<ActionTypeBasicView> scrollView = new ScrollView<>();
protected ActionTypeFullView selectedActionType = new ActionTypeFullView();

/*
 * getters and setters
 */
public ScrollView<ActionTypeBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<ActionTypeBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<ActionTypeFilter, ActionTypeSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<ActionTypeFilter, ActionTypeSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public ActionTypeFullView getSelectedActionType() {
return selectedActionType;
}
public void setSelectedActionType(ActionTypeFullView selectedActionType) {
this.selectedActionType = selectedActionType;
}

/* Specific Code Start */
/* Specific Code End */
}
