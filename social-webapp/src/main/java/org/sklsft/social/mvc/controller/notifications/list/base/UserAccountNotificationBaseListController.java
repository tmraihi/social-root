package org.sklsft.social.mvc.controller.notifications.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.notifications.UserAccountNotificationService;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.notifications.list.UserAccountNotificationListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected UserAccountNotificationService userAccountNotificationService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected UserAccountNotificationListView userAccountNotificationListView;

/*
 * getters and setters
 */
public UserAccountNotificationListView getUserAccountNotificationListView() {
return userAccountNotificationListView;
}
public void setUserAccountNotificationListView(UserAccountNotificationListView userAccountNotificationListView) {
this.userAccountNotificationListView = userAccountNotificationListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
userAccountNotificationListView.setScrollView(userAccountNotificationService.scroll(userAccountNotificationListView.getScrollForm()));
userAccountNotificationListView.getScrollForm().setPage(userAccountNotificationListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createUserAccountNotification() {
this.commonController.loadActionTypeOptions();
try {
this.userAccountNotificationListView.setSelectedUserAccountNotification(this.userAccountNotificationService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("UserAccountNotification.save")
public void save() {
userAccountNotificationService.save(this.userAccountNotificationListView.getSelectedUserAccountNotification().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editUserAccountNotification(Long id) {
this.commonController.loadActionTypeOptions();
userAccountNotificationListView.setSelectedUserAccountNotification(userAccountNotificationService.load(id));
}

/**
 * update object
 */
@AjaxMethod("UserAccountNotification.update")
public void update() {
userAccountNotificationService.update(this.userAccountNotificationListView.getSelectedUserAccountNotification().getId(), this.userAccountNotificationListView.getSelectedUserAccountNotification().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("UserAccountNotification.delete")
public void delete(Long id) {
userAccountNotificationService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("UserAccountNotification.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (UserAccountNotificationBasicView userAccountNotification:userAccountNotificationListView.getScrollView().getElements()) {
if (userAccountNotification.getSelected()) {
ids.add(userAccountNotification.getId());
}
}
userAccountNotificationService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.userAccountNotificationListView.setScrollForm(new ScrollForm<>());
this.userAccountNotificationListView.getScrollForm().setFilter(new UserAccountNotificationFilter());
this.userAccountNotificationListView.getScrollForm().setSorting(new UserAccountNotificationSorting());
refresh();
}

}
