package org.sklsft.social.mvc.model.settings.detail;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(ViewScope.NAME)
public class ParameterTypeDetailView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private ParameterTypeFullView selectedParameterType = new ParameterTypeFullView();

/*
 * getters and setters
 */
public ParameterTypeFullView getSelectedParameterType() {
return selectedParameterType;
}
public void setSelectedParameterType(ParameterTypeFullView selectedParameterType) {
this.selectedParameterType = selectedParameterType;
}

/* Specific Code Start */
/* Specific Code End */
}
