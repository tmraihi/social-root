package org.sklsft.social.mvc.controller.notifications.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.notifications.UserAccountNotificationService;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.notifications.detail.UserAccountNotificationDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected UserAccountNotificationService userAccountNotificationService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected UserAccountNotificationDetailView userAccountNotificationDetailView;

/**
 * load object
 */
public void load() {
this.commonController.loadActionTypeOptions();
userAccountNotificationDetailView.setSelectedUserAccountNotification(this.userAccountNotificationService.load(this.userAccountNotificationDetailView.getSelectedUserAccountNotification().getId()));
}


/**
 * update object
 */
@AjaxMethod("UserAccountNotification.update")
public void update() {
userAccountNotificationService.update(userAccountNotificationDetailView.getSelectedUserAccountNotification().getId(), userAccountNotificationDetailView.getSelectedUserAccountNotification().getForm());
load();
}

}
