package org.sklsft.social.mvc.model.notifications.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class UserAccountNotificationListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<UserAccountNotificationFilter, UserAccountNotificationSorting> scrollForm = new ScrollForm<>();
protected ScrollView<UserAccountNotificationBasicView> scrollView = new ScrollView<>();
protected UserAccountNotificationFullView selectedUserAccountNotification = new UserAccountNotificationFullView();

/*
 * getters and setters
 */
public ScrollView<UserAccountNotificationBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<UserAccountNotificationBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<UserAccountNotificationFilter, UserAccountNotificationSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<UserAccountNotificationFilter, UserAccountNotificationSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public UserAccountNotificationFullView getSelectedUserAccountNotification() {
return selectedUserAccountNotification;
}
public void setSelectedUserAccountNotification(UserAccountNotificationFullView selectedUserAccountNotification) {
this.selectedUserAccountNotification = selectedUserAccountNotification;
}

/* Specific Code Start */
/* Specific Code End */
}
