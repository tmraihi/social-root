package org.sklsft.social.mvc.controller.nomenclature.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.nomenclature.KeywordService;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.api.model.nomenclature.views.basic.KeywordBasicView;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.nomenclature.list.KeywordListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected KeywordService keywordService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected KeywordListView keywordListView;

/*
 * getters and setters
 */
public KeywordListView getKeywordListView() {
return keywordListView;
}
public void setKeywordListView(KeywordListView keywordListView) {
this.keywordListView = keywordListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
keywordListView.setScrollView(keywordService.scroll(keywordListView.getScrollForm()));
keywordListView.getScrollForm().setPage(keywordListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createKeyword() {
try {
this.keywordListView.setSelectedKeyword(this.keywordService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("Keyword.save")
public void save() {
keywordService.save(this.keywordListView.getSelectedKeyword().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editKeyword(Long id) {
keywordListView.setSelectedKeyword(keywordService.load(id));
}

/**
 * update object
 */
@AjaxMethod("Keyword.update")
public void update() {
keywordService.update(this.keywordListView.getSelectedKeyword().getId(), this.keywordListView.getSelectedKeyword().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("Keyword.delete")
public void delete(Long id) {
keywordService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("Keyword.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (KeywordBasicView keyword:keywordListView.getScrollView().getElements()) {
if (keyword.getSelected()) {
ids.add(keyword.getId());
}
}
keywordService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.keywordListView.setScrollForm(new ScrollForm<>());
this.keywordListView.getScrollForm().setFilter(new KeywordFilter());
this.keywordListView.getScrollForm().setSorting(new KeywordSorting());
refresh();
}

}
