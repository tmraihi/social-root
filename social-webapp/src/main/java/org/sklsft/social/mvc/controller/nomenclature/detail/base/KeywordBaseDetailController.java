package org.sklsft.social.mvc.controller.nomenclature.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.nomenclature.KeywordService;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.nomenclature.detail.KeywordDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected KeywordService keywordService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected KeywordDetailView keywordDetailView;

/**
 * load object
 */
public void load() {
keywordDetailView.setSelectedKeyword(this.keywordService.load(this.keywordDetailView.getSelectedKeyword().getId()));
}


/**
 * update object
 */
@AjaxMethod("Keyword.update")
public void update() {
keywordService.update(keywordDetailView.getSelectedKeyword().getId(), keywordDetailView.getSelectedKeyword().getForm());
load();
}

}
