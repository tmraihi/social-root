package org.sklsft.social.mvc.controller;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.interfaces.nomenclature.KeywordService;
import org.sklsft.social.api.interfaces.notifications.ActionTypeService;
import org.sklsft.social.api.interfaces.organization.OrganizationService;
import org.sklsft.social.api.interfaces.settings.GlobalParameterService;
import org.sklsft.social.api.interfaces.settings.ParameterTypeService;
import org.sklsft.social.mvc.model.CommonView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated common controller class file
 * <br/>used for loading select items
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class CommonController {

/*
 * the view handled by the controller
 */
@Inject
private CommonView commonView;

/*
 * the services used by the controller
 */
@Inject
private KeywordService keywordService;
@Inject
private UserAccountService userAccountService;
@Inject
private OrganizationService organizationService;
@Inject
private ActionTypeService actionTypeService;
@Inject
private ParameterTypeService parameterTypeService;
@Inject
private GlobalParameterService globalParameterService;

/**
 * search options for Keyword
 */
public List<String> searchKeywordOptions(String arg) {
List<SelectItem> options = this.keywordService.searchOptions(arg);
List<String> result = new ArrayList<>(options.size());
for (SelectItem option : options) {
result.add(option.getKey());
}
return result;
}

/**
 * search options for UserAccount
 */
public List<String> searchUserAccountOptions(String arg) {
List<SelectItem> options = this.userAccountService.searchOptions(arg);
List<String> result = new ArrayList<>(options.size());
for (SelectItem option : options) {
result.add(option.getKey());
}
return result;
}

/**
 * search options for Organization
 */
public List<String> searchOrganizationOptions(String arg) {
List<SelectItem> options = this.organizationService.searchOptions(arg);
List<String> result = new ArrayList<>(options.size());
for (SelectItem option : options) {
result.add(option.getKey());
}
return result;
}

/**
 * load options for ActionType
 */
public void loadActionTypeOptions() {
List<SelectItem> options = this.actionTypeService.getOptions();
this.commonView.setActionTypeOptions(options);
}

/**
 * load options for ParameterType
 */
public void loadParameterTypeOptions() {
List<SelectItem> options = this.parameterTypeService.getOptions();
this.commonView.setParameterTypeOptions(options);
}

/**
 * load options for GlobalParameter
 */
public void loadGlobalParameterOptions() {
List<SelectItem> options = this.globalParameterService.getOptions();
this.commonView.setGlobalParameterOptions(options);
}

/* Specific Code Start */
/* Specific Code End */
}
