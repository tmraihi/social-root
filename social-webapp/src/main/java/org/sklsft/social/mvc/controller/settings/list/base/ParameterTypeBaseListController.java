package org.sklsft.social.mvc.controller.settings.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.settings.ParameterTypeService;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.api.model.settings.views.basic.ParameterTypeBasicView;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.settings.list.ParameterTypeListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected ParameterTypeService parameterTypeService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected ParameterTypeListView parameterTypeListView;

/*
 * getters and setters
 */
public ParameterTypeListView getParameterTypeListView() {
return parameterTypeListView;
}
public void setParameterTypeListView(ParameterTypeListView parameterTypeListView) {
this.parameterTypeListView = parameterTypeListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
parameterTypeListView.setScrollView(parameterTypeService.scroll(parameterTypeListView.getScrollForm()));
parameterTypeListView.getScrollForm().setPage(parameterTypeListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createParameterType() {
try {
this.parameterTypeListView.setSelectedParameterType(this.parameterTypeService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("ParameterType.save")
public void save() {
parameterTypeService.save(this.parameterTypeListView.getSelectedParameterType().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editParameterType(Long id) {
parameterTypeListView.setSelectedParameterType(parameterTypeService.load(id));
}

/**
 * update object
 */
@AjaxMethod("ParameterType.update")
public void update() {
parameterTypeService.update(this.parameterTypeListView.getSelectedParameterType().getId(), this.parameterTypeListView.getSelectedParameterType().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("ParameterType.delete")
public void delete(Long id) {
parameterTypeService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("ParameterType.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (ParameterTypeBasicView parameterType:parameterTypeListView.getScrollView().getElements()) {
if (parameterType.getSelected()) {
ids.add(parameterType.getId());
}
}
parameterTypeService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.parameterTypeListView.setScrollForm(new ScrollForm<>());
this.parameterTypeListView.getScrollForm().setFilter(new ParameterTypeFilter());
this.parameterTypeListView.getScrollForm().setSorting(new ParameterTypeSorting());
refresh();
}

}
