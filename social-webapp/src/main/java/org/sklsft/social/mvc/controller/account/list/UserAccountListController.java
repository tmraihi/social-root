package org.sklsft.social.mvc.controller.account.list;

import org.sklsft.social.mvc.controller.account.list.base.UserAccountBaseListController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list controller class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class UserAccountListController extends  UserAccountBaseListController {

/* Specific Code Start */
/* Specific Code End */
}
