package org.sklsft.social.mvc.controller.organization.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.organization.OrganizationGuestService;
import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.api.interfaces.organization.OrganizationService;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationForm;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.organization.detail.OrganizationDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected OrganizationService organizationService;
@Inject
protected OrganizationMemberService organizationMemberService;
@Inject
protected OrganizationGuestService organizationGuestService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected OrganizationDetailView organizationDetailView;

/**
 * load object
 */
public void load() {
organizationDetailView.setSelectedOrganization(this.organizationService.load(this.organizationDetailView.getSelectedOrganization().getId()));
}


/**
 * load one to many organizationMember list
 */
public void loadOrganizationMemberList() {
this.resetOrganizationMemberFilter();
}

/**
 * refresh one to many organizationMember list
 */
public void refreshOrganizationMemberList() {
organizationDetailView.setOrganizationMemberScrollView(organizationMemberService.scrollFromOrganization(organizationDetailView.getSelectedOrganization().getId(), organizationDetailView.getOrganizationMemberScrollForm()));
organizationDetailView.getOrganizationMemberScrollForm().setPage(organizationDetailView.getOrganizationMemberScrollView().getCurrentPage());
}

/**
 * load one to many organizationGuest list
 */
public void loadOrganizationGuestList() {
this.resetOrganizationGuestFilter();
}

/**
 * refresh one to many organizationGuest list
 */
public void refreshOrganizationGuestList() {
organizationDetailView.setOrganizationGuestScrollView(organizationGuestService.scrollFromOrganization(organizationDetailView.getSelectedOrganization().getId(), organizationDetailView.getOrganizationGuestScrollForm()));
organizationDetailView.getOrganizationGuestScrollForm().setPage(organizationDetailView.getOrganizationGuestScrollView().getCurrentPage());
}

/**
 * create one to many organizationMember
 */
public void createOrganizationMember() {
try {
organizationDetailView.setSelectedOrganizationMember(this.organizationMemberService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * create one to many organizationGuest
 */
public void createOrganizationGuest() {
try {
organizationDetailView.setSelectedOrganizationGuest(this.organizationGuestService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save one to many organizationMember
 */
@AjaxMethod("OrganizationMember.save")
public void saveOrganizationMember() {
load();
OrganizationMemberForm form = organizationDetailView.getSelectedOrganizationMember().getForm();
OrganizationForm organizationForm = organizationDetailView.getSelectedOrganization().getForm();
form.setOrganizationCode(organizationForm.getCode());
organizationMemberService.save(form);
refreshOrganizationMemberList();
}

/**
 * save one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.save")
public void saveOrganizationGuest() {
load();
OrganizationGuestForm form = organizationDetailView.getSelectedOrganizationGuest().getForm();
OrganizationForm organizationForm = organizationDetailView.getSelectedOrganization().getForm();
form.setOrganizationCode(organizationForm.getCode());
organizationGuestService.save(form);
refreshOrganizationGuestList();
}

/**
 * update object
 */
@AjaxMethod("Organization.update")
public void update() {
organizationService.update(organizationDetailView.getSelectedOrganization().getId(), organizationDetailView.getSelectedOrganization().getForm());
load();
}

/**
 * edit one to many organizationMember
 */
public void editOrganizationMember(Long id) {
organizationDetailView.setSelectedOrganizationMember(organizationMemberService.load(id));
}

/**
 * edit one to many organizationGuest
 */
public void editOrganizationGuest(Long id) {
organizationDetailView.setSelectedOrganizationGuest(organizationGuestService.load(id));
}

/**
 * update one to many organizationMember
 */
@AjaxMethod("OrganizationMember.update")
public void updateOrganizationMember() {
organizationMemberService.update(organizationDetailView.getSelectedOrganizationMember().getId(), organizationDetailView.getSelectedOrganizationMember().getForm());
refreshOrganizationMemberList();
}

/**
 * update one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.update")
public void updateOrganizationGuest() {
organizationGuestService.update(organizationDetailView.getSelectedOrganizationGuest().getId(), organizationDetailView.getSelectedOrganizationGuest().getForm());
refreshOrganizationGuestList();
}

/**
 * delete one to many organizationMember
 */
@AjaxMethod("OrganizationMember.delete")
public void deleteOrganizationMember(Long id) {
organizationMemberService.delete(id);
refreshOrganizationMemberList();
}

/**
 * delete one to many organizationGuest
 */
@AjaxMethod("OrganizationGuest.delete")
public void deleteOrganizationGuest(Long id) {
organizationGuestService.delete(id);
refreshOrganizationGuestList();
}

/**
 * delete one to many organizationMember list
 */
@AjaxMethod("OrganizationMember.deleteList")
public void deleteOrganizationMemberList() {
List<Long> ids = new ArrayList<>();
for (OrganizationMemberBasicView organizationMember:organizationDetailView.getOrganizationMemberScrollView().getElements()) {
if (organizationMember.getSelected()) {
ids.add(organizationMember.getId());
}
}
organizationMemberService.deleteList(ids);
refreshOrganizationMemberList();
}

/**
 * delete one to many organizationGuest list
 */
@AjaxMethod("OrganizationGuest.deleteList")
public void deleteOrganizationGuestList() {
List<Long> ids = new ArrayList<>();
for (OrganizationGuestBasicView organizationGuest:organizationDetailView.getOrganizationGuestScrollView().getElements()) {
if (organizationGuest.getSelected()) {
ids.add(organizationGuest.getId());
}
}
organizationGuestService.deleteList(ids);
refreshOrganizationGuestList();
}

/**
 * reset one to many OrganizationMemberFilter datatable filter and sorting
 */
public void resetOrganizationMemberFilter() {
this.organizationDetailView.setOrganizationMemberScrollForm(new ScrollForm<>());
this.organizationDetailView.getOrganizationMemberScrollForm().setFilter(new OrganizationMemberFilter());
this.organizationDetailView.getOrganizationMemberScrollForm().setSorting(new OrganizationMemberSorting());
refreshOrganizationMemberList();
}

/**
 * reset one to many OrganizationGuestFilter datatable filter and sorting
 */
public void resetOrganizationGuestFilter() {
this.organizationDetailView.setOrganizationGuestScrollForm(new ScrollForm<>());
this.organizationDetailView.getOrganizationGuestScrollForm().setFilter(new OrganizationGuestFilter());
this.organizationDetailView.getOrganizationGuestScrollForm().setSorting(new OrganizationGuestSorting());
refreshOrganizationGuestList();
}

}
