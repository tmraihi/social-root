package org.sklsft.social.mvc.controller.account.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.account.views.basic.UserAccountBasicView;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.account.list.UserAccountListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected UserAccountService userAccountService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected UserAccountListView userAccountListView;

/*
 * getters and setters
 */
public UserAccountListView getUserAccountListView() {
return userAccountListView;
}
public void setUserAccountListView(UserAccountListView userAccountListView) {
this.userAccountListView = userAccountListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
userAccountListView.setScrollView(userAccountService.scroll(userAccountListView.getScrollForm()));
userAccountListView.getScrollForm().setPage(userAccountListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createUserAccount() {
try {
this.userAccountListView.setSelectedUserAccount(this.userAccountService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
public void save() {
executeAjaxMethod("UserAccount.save", new AjaxMethodTemplate() {
@Override
public Object execute() {
return userAccountService.save(userAccountListView.getSelectedUserAccount().getForm());
}
@Override
public void redirectOnComplete(Object result) {
redirect("/sections/account/user-account/details.jsf?id=" + result);
}
});
}
/**
 * edit object
 */
public void editUserAccount(Long id) {
userAccountListView.setSelectedUserAccount(userAccountService.load(id));
}

/**
 * update object
 */
@AjaxMethod("UserAccount.update")
public void update() {
userAccountService.update(this.userAccountListView.getSelectedUserAccount().getId(), this.userAccountListView.getSelectedUserAccount().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("UserAccount.delete")
public void delete(Long id) {
userAccountService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("UserAccount.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (UserAccountBasicView userAccount:userAccountListView.getScrollView().getElements()) {
if (userAccount.getSelected()) {
ids.add(userAccount.getId());
}
}
userAccountService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.userAccountListView.setScrollForm(new ScrollForm<>());
this.userAccountListView.getScrollForm().setFilter(new UserAccountFilter());
this.userAccountListView.getScrollForm().setSorting(new UserAccountSorting());
refresh();
}

}
