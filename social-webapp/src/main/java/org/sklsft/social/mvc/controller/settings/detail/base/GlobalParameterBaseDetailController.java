package org.sklsft.social.mvc.controller.settings.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.settings.GlobalParameterService;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.settings.detail.GlobalParameterDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected GlobalParameterService globalParameterService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected GlobalParameterDetailView globalParameterDetailView;

/**
 * load object
 */
public void load() {
this.commonController.loadParameterTypeOptions();
globalParameterDetailView.setSelectedGlobalParameter(this.globalParameterService.load(this.globalParameterDetailView.getSelectedGlobalParameter().getId()));
}


/**
 * update object
 */
@AjaxMethod("GlobalParameter.update")
public void update() {
globalParameterService.update(globalParameterDetailView.getSelectedGlobalParameter().getId(), globalParameterDetailView.getSelectedGlobalParameter().getForm());
load();
}

}
