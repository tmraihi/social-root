package org.sklsft.social.mvc.controller.organization.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.organization.OrganizationGuestService;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.organization.list.OrganizationGuestListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected OrganizationGuestService organizationGuestService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected OrganizationGuestListView organizationGuestListView;

/*
 * getters and setters
 */
public OrganizationGuestListView getOrganizationGuestListView() {
return organizationGuestListView;
}
public void setOrganizationGuestListView(OrganizationGuestListView organizationGuestListView) {
this.organizationGuestListView = organizationGuestListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
organizationGuestListView.setScrollView(organizationGuestService.scroll(organizationGuestListView.getScrollForm()));
organizationGuestListView.getScrollForm().setPage(organizationGuestListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createOrganizationGuest() {
try {
this.organizationGuestListView.setSelectedOrganizationGuest(this.organizationGuestService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("OrganizationGuest.save")
public void save() {
organizationGuestService.save(this.organizationGuestListView.getSelectedOrganizationGuest().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editOrganizationGuest(Long id) {
organizationGuestListView.setSelectedOrganizationGuest(organizationGuestService.load(id));
}

/**
 * update object
 */
@AjaxMethod("OrganizationGuest.update")
public void update() {
organizationGuestService.update(this.organizationGuestListView.getSelectedOrganizationGuest().getId(), this.organizationGuestListView.getSelectedOrganizationGuest().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("OrganizationGuest.delete")
public void delete(Long id) {
organizationGuestService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("OrganizationGuest.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (OrganizationGuestBasicView organizationGuest:organizationGuestListView.getScrollView().getElements()) {
if (organizationGuest.getSelected()) {
ids.add(organizationGuest.getId());
}
}
organizationGuestService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.organizationGuestListView.setScrollForm(new ScrollForm<>());
this.organizationGuestListView.getScrollForm().setFilter(new OrganizationGuestFilter());
this.organizationGuestListView.getScrollForm().setSorting(new OrganizationGuestSorting());
refresh();
}

}
