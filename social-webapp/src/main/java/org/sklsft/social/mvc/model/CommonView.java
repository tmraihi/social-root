package org.sklsft.social.mvc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.SelectItem;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated common view class file
 * <br/>used for select items
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class CommonView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private List<SelectItem>actionTypeOptions;
private List<SelectItem>parameterTypeOptions;
private List<SelectItem>globalParameterOptions;

/*
 * getters and setters
 */
public List<SelectItem> getActionTypeOptions() {
return actionTypeOptions;
}
public void setActionTypeOptions(List<SelectItem> actionTypeOptions) {
this.actionTypeOptions = actionTypeOptions;
}

public List<SelectItem> getParameterTypeOptions() {
return parameterTypeOptions;
}
public void setParameterTypeOptions(List<SelectItem> parameterTypeOptions) {
this.parameterTypeOptions = parameterTypeOptions;
}

public List<SelectItem> getGlobalParameterOptions() {
return globalParameterOptions;
}
public void setGlobalParameterOptions(List<SelectItem> globalParameterOptions) {
this.globalParameterOptions = globalParameterOptions;
}

/* Specific Code Start */
/* Specific Code End */
}
