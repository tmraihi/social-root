package org.sklsft.social.mvc.model.account.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.account.views.basic.UserAccountBasicView;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class UserAccountListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<UserAccountFilter, UserAccountSorting> scrollForm = new ScrollForm<>();
protected ScrollView<UserAccountBasicView> scrollView = new ScrollView<>();
protected UserAccountFullView selectedUserAccount = new UserAccountFullView();

/*
 * getters and setters
 */
public ScrollView<UserAccountBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<UserAccountBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<UserAccountFilter, UserAccountSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<UserAccountFilter, UserAccountSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public UserAccountFullView getSelectedUserAccount() {
return selectedUserAccount;
}
public void setSelectedUserAccount(UserAccountFullView selectedUserAccount) {
this.selectedUserAccount = selectedUserAccount;
}

/* Specific Code Start */
/* Specific Code End */
}
