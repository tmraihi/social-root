package org.sklsft.social.mvc.model.notifications.detail;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(ViewScope.NAME)
public class UserAccountNotificationDetailView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private UserAccountNotificationFullView selectedUserAccountNotification = new UserAccountNotificationFullView();

/*
 * getters and setters
 */
public UserAccountNotificationFullView getSelectedUserAccountNotification() {
return selectedUserAccountNotification;
}
public void setSelectedUserAccountNotification(UserAccountNotificationFullView selectedUserAccountNotification) {
this.selectedUserAccountNotification = selectedUserAccountNotification;
}

/* Specific Code Start */
/* Specific Code End */
}
