package org.sklsft.social.mvc.model.nomenclature.detail;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.mvc.scopes.ViewScope;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(ViewScope.NAME)
public class KeywordDetailView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private KeywordFullView selectedKeyword = new KeywordFullView();

/*
 * getters and setters
 */
public KeywordFullView getSelectedKeyword() {
return selectedKeyword;
}
public void setSelectedKeyword(KeywordFullView selectedKeyword) {
this.selectedKeyword = selectedKeyword;
}

/* Specific Code Start */
/* Specific Code End */
}
