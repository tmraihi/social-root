package org.sklsft.social.mvc.controller.settings.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.settings.ParameterTypeService;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.settings.detail.ParameterTypeDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected ParameterTypeService parameterTypeService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected ParameterTypeDetailView parameterTypeDetailView;

/**
 * load object
 */
public void load() {
parameterTypeDetailView.setSelectedParameterType(this.parameterTypeService.load(this.parameterTypeDetailView.getSelectedParameterType().getId()));
}


/**
 * update object
 */
@AjaxMethod("ParameterType.update")
public void update() {
parameterTypeService.update(parameterTypeDetailView.getSelectedParameterType().getId(), parameterTypeDetailView.getSelectedParameterType().getForm());
load();
}

}
