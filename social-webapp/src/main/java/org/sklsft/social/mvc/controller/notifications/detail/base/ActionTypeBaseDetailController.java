package org.sklsft.social.mvc.controller.notifications.detail.base;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.notifications.ActionTypeService;
import org.sklsft.social.api.model.notifications.forms.ActionTypeForm;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.notifications.detail.ActionTypeDetailView;

/**
 * auto generated base detail controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseDetailController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected ActionTypeService actionTypeService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected ActionTypeDetailView actionTypeDetailView;

/**
 * load object
 */
public void load() {
actionTypeDetailView.setSelectedActionType(this.actionTypeService.load(this.actionTypeDetailView.getSelectedActionType().getId()));
}


/**
 * update object
 */
@AjaxMethod("ActionType.update")
public void update() {
actionTypeService.update(actionTypeDetailView.getSelectedActionType().getId(), actionTypeDetailView.getSelectedActionType().getForm());
load();
}

}
