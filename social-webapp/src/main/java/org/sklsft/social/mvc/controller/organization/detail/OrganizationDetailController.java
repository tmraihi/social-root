package org.sklsft.social.mvc.controller.organization.detail;

import org.sklsft.social.mvc.controller.organization.detail.base.OrganizationBaseDetailController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated detail controller class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_REQUEST)
public class OrganizationDetailController extends  OrganizationBaseDetailController {

/* Specific Code Start */
/* Specific Code End */
}
