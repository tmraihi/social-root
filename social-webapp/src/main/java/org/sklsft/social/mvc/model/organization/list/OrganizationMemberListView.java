package org.sklsft.social.mvc.model.organization.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class OrganizationMemberListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> scrollForm = new ScrollForm<>();
protected ScrollView<OrganizationMemberBasicView> scrollView = new ScrollView<>();
protected OrganizationMemberFullView selectedOrganizationMember = new OrganizationMemberFullView();

/*
 * getters and setters
 */
public ScrollView<OrganizationMemberBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<OrganizationMemberBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public OrganizationMemberFullView getSelectedOrganizationMember() {
return selectedOrganizationMember;
}
public void setSelectedOrganizationMember(OrganizationMemberFullView selectedOrganizationMember) {
this.selectedOrganizationMember = selectedOrganizationMember;
}

/* Specific Code Start */
/* Specific Code End */
}
