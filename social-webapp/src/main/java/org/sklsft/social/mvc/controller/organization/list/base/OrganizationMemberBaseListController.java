package org.sklsft.social.mvc.controller.organization.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.organization.list.OrganizationMemberListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected OrganizationMemberService organizationMemberService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected OrganizationMemberListView organizationMemberListView;

/*
 * getters and setters
 */
public OrganizationMemberListView getOrganizationMemberListView() {
return organizationMemberListView;
}
public void setOrganizationMemberListView(OrganizationMemberListView organizationMemberListView) {
this.organizationMemberListView = organizationMemberListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
organizationMemberListView.setScrollView(organizationMemberService.scroll(organizationMemberListView.getScrollForm()));
organizationMemberListView.getScrollForm().setPage(organizationMemberListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createOrganizationMember() {
try {
this.organizationMemberListView.setSelectedOrganizationMember(this.organizationMemberService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("OrganizationMember.save")
public void save() {
organizationMemberService.save(this.organizationMemberListView.getSelectedOrganizationMember().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editOrganizationMember(Long id) {
organizationMemberListView.setSelectedOrganizationMember(organizationMemberService.load(id));
}

/**
 * update object
 */
@AjaxMethod("OrganizationMember.update")
public void update() {
organizationMemberService.update(this.organizationMemberListView.getSelectedOrganizationMember().getId(), this.organizationMemberListView.getSelectedOrganizationMember().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("OrganizationMember.delete")
public void delete(Long id) {
organizationMemberService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("OrganizationMember.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (OrganizationMemberBasicView organizationMember:organizationMemberListView.getScrollView().getElements()) {
if (organizationMember.getSelected()) {
ids.add(organizationMember.getId());
}
}
organizationMemberService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.organizationMemberListView.setScrollForm(new ScrollForm<>());
this.organizationMemberListView.getScrollForm().setFilter(new OrganizationMemberFilter());
this.organizationMemberListView.getScrollForm().setSorting(new OrganizationMemberSorting());
refresh();
}

}
