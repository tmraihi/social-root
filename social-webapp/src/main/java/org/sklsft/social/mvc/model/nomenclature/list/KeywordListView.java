package org.sklsft.social.mvc.model.nomenclature.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.api.model.nomenclature.views.basic.KeywordBasicView;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class KeywordListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<KeywordFilter, KeywordSorting> scrollForm = new ScrollForm<>();
protected ScrollView<KeywordBasicView> scrollView = new ScrollView<>();
protected KeywordFullView selectedKeyword = new KeywordFullView();

/*
 * getters and setters
 */
public ScrollView<KeywordBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<KeywordBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<KeywordFilter, KeywordSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<KeywordFilter, KeywordSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public KeywordFullView getSelectedKeyword() {
return selectedKeyword;
}
public void setSelectedKeyword(KeywordFullView selectedKeyword) {
this.selectedKeyword = selectedKeyword;
}

/* Specific Code Start */
/* Specific Code End */
}
