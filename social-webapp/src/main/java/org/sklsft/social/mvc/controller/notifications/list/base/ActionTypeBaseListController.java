package org.sklsft.social.mvc.controller.notifications.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.notifications.ActionTypeService;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.api.model.notifications.views.basic.ActionTypeBasicView;
import org.sklsft.social.api.model.notifications.views.full.ActionTypeFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.notifications.list.ActionTypeListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected ActionTypeService actionTypeService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected ActionTypeListView actionTypeListView;

/*
 * getters and setters
 */
public ActionTypeListView getActionTypeListView() {
return actionTypeListView;
}
public void setActionTypeListView(ActionTypeListView actionTypeListView) {
this.actionTypeListView = actionTypeListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
actionTypeListView.setScrollView(actionTypeService.scroll(actionTypeListView.getScrollForm()));
actionTypeListView.getScrollForm().setPage(actionTypeListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createActionType() {
try {
this.actionTypeListView.setSelectedActionType(this.actionTypeService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("ActionType.save")
public void save() {
actionTypeService.save(this.actionTypeListView.getSelectedActionType().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editActionType(Long id) {
actionTypeListView.setSelectedActionType(actionTypeService.load(id));
}

/**
 * update object
 */
@AjaxMethod("ActionType.update")
public void update() {
actionTypeService.update(this.actionTypeListView.getSelectedActionType().getId(), this.actionTypeListView.getSelectedActionType().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("ActionType.delete")
public void delete(Long id) {
actionTypeService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("ActionType.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (ActionTypeBasicView actionType:actionTypeListView.getScrollView().getElements()) {
if (actionType.getSelected()) {
ids.add(actionType.getId());
}
}
actionTypeService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.actionTypeListView.setScrollForm(new ScrollForm<>());
this.actionTypeListView.getScrollForm().setFilter(new ActionTypeFilter());
this.actionTypeListView.getScrollForm().setSorting(new ActionTypeSorting());
refresh();
}

}
