package org.sklsft.social.mvc.model.organization.list;

import java.io.Serializable;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * auto generated list view class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Component
@Scope(value=WebApplicationContext.SCOPE_SESSION)
public class OrganizationGuestListView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
protected ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> scrollForm = new ScrollForm<>();
protected ScrollView<OrganizationGuestBasicView> scrollView = new ScrollView<>();
protected OrganizationGuestFullView selectedOrganizationGuest = new OrganizationGuestFullView();

/*
 * getters and setters
 */
public ScrollView<OrganizationGuestBasicView> getScrollView() {
return scrollView;
}
public void setScrollView(ScrollView<OrganizationGuestBasicView> scrollView) {
this.scrollView = scrollView;
}

public ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> getScrollForm() {
return scrollForm;
}
public void setScrollForm(ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> scrollForm) {
this.scrollForm = scrollForm;
}

public OrganizationGuestFullView getSelectedOrganizationGuest() {
return selectedOrganizationGuest;
}
public void setSelectedOrganizationGuest(OrganizationGuestFullView selectedOrganizationGuest) {
this.selectedOrganizationGuest = selectedOrganizationGuest;
}

/* Specific Code Start */
/* Specific Code End */
}
