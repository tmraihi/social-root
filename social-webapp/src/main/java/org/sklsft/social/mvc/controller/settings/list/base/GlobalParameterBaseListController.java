package org.sklsft.social.mvc.controller.settings.list.base;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.mvc.ajax.AjaxMethodTemplate;
import org.sklsft.commons.mvc.annotations.AjaxMethod;
import org.sklsft.social.api.interfaces.settings.GlobalParameterService;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.api.model.settings.views.basic.GlobalParameterBasicView;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;
import org.sklsft.social.mvc.controller.BaseController;
import org.sklsft.social.mvc.controller.CommonController;
import org.sklsft.social.mvc.model.settings.list.GlobalParameterListView;

/**
 * auto generated base list controller class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseListController extends BaseController {

/*
 * services injected by spring
 */
@Inject
protected GlobalParameterService globalParameterService;
@Inject
protected CommonController commonController;

/*
 * view
 */
@Inject
protected GlobalParameterListView globalParameterListView;

/*
 * getters and setters
 */
public GlobalParameterListView getGlobalParameterListView() {
return globalParameterListView;
}
public void setGlobalParameterListView(GlobalParameterListView globalParameterListView) {
this.globalParameterListView = globalParameterListView;
}

/**
 * load object list
 */
public void load() {
this.reset();
}

/**
 * refresh object list
 */
public void refresh() {
globalParameterListView.setScrollView(globalParameterService.scroll(globalParameterListView.getScrollForm()));
globalParameterListView.getScrollForm().setPage(globalParameterListView.getScrollView().getCurrentPage());
}

/**
 * create object
 */
public void createGlobalParameter() {
this.commonController.loadParameterTypeOptions();
try {
this.globalParameterListView.setSelectedGlobalParameter(this.globalParameterService.create());
} catch (OperationDeniedException e) {
displayError(e.getMessage());
}
}

/**
 * save object
 */
@AjaxMethod("GlobalParameter.save")
public void save() {
globalParameterService.save(this.globalParameterListView.getSelectedGlobalParameter().getForm());
this.refresh();
}

/**
 * edit object
 */
public void editGlobalParameter(Long id) {
this.commonController.loadParameterTypeOptions();
globalParameterListView.setSelectedGlobalParameter(globalParameterService.load(id));
}

/**
 * update object
 */
@AjaxMethod("GlobalParameter.update")
public void update() {
globalParameterService.update(this.globalParameterListView.getSelectedGlobalParameter().getId(), this.globalParameterListView.getSelectedGlobalParameter().getForm());
this.refresh();
}

/**
 * delete object
 */
@AjaxMethod("GlobalParameter.delete")
public void delete(Long id) {
globalParameterService.delete(id);
this.refresh();
}

/**
 * delete object list
 */
@AjaxMethod("GlobalParameter.deleteList")
public void deleteList() {
List<Long> ids = new ArrayList<>();
for (GlobalParameterBasicView globalParameter:globalParameterListView.getScrollView().getElements()) {
if (globalParameter.getSelected()) {
ids.add(globalParameter.getId());
}
}
globalParameterService.deleteList(ids);
this.refresh();
}

/**
 * reset filters and sortings
 */
public void reset() {
this.globalParameterListView.setScrollForm(new ScrollForm<>());
this.globalParameterListView.getScrollForm().setFilter(new GlobalParameterFilter());
this.globalParameterListView.getScrollForm().setSorting(new GlobalParameterSorting());
refresh();
}

}
