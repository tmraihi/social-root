package org.sklsft.social.bl.impl.organization;

import org.sklsft.social.api.interfaces.organization.OrganizationService;
import org.sklsft.social.bl.impl.organization.base.OrganizationBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialOrganizationServiceImpl")
public class OrganizationServiceImpl extends OrganizationBaseServiceImpl implements OrganizationService{

/* Specific Code Start */
/* Specific Code End */
}
