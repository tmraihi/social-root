package org.sklsft.social.bl.impl.settings;

import org.sklsft.social.api.interfaces.settings.GlobalParameterService;
import org.sklsft.social.bl.impl.settings.base.GlobalParameterBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialGlobalParameterServiceImpl")
public class GlobalParameterServiceImpl extends GlobalParameterBaseServiceImpl implements GlobalParameterService{

/* Specific Code Start */
/* Specific Code End */
}
