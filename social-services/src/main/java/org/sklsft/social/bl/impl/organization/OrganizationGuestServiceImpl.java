package org.sklsft.social.bl.impl.organization;

import org.sklsft.social.api.interfaces.organization.OrganizationGuestService;
import org.sklsft.social.bl.impl.organization.base.OrganizationGuestBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialOrganizationGuestServiceImpl")
public class OrganizationGuestServiceImpl extends OrganizationGuestBaseServiceImpl implements OrganizationGuestService{

/* Specific Code Start */
/* Specific Code End */
}
