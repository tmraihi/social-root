package org.sklsft.social.bl.impl.notifications;

import org.sklsft.social.api.interfaces.notifications.UserAccountNotificationService;
import org.sklsft.social.bl.impl.notifications.base.UserAccountNotificationBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialUserAccountNotificationServiceImpl")
public class UserAccountNotificationServiceImpl extends UserAccountNotificationBaseServiceImpl implements UserAccountNotificationService{

/* Specific Code Start */
/* Specific Code End */
}
