package org.sklsft.social.bl.impl.settings.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.settings.base.GlobalParameterBaseService;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.api.model.settings.views.basic.GlobalParameterBasicView;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;
import org.sklsft.social.bc.mapper.settings.forms.GlobalParameterFormMapper;
import org.sklsft.social.bc.mapper.settings.views.basic.GlobalParameterBasicViewMapper;
import org.sklsft.social.bc.mapper.settings.views.full.GlobalParameterFullViewMapper;
import org.sklsft.social.bc.processor.settings.GlobalParameterProcessor;
import org.sklsft.social.bc.rightsmanager.settings.GlobalParameterRightsManager;
import org.sklsft.social.bc.statemanager.settings.GlobalParameterStateManager;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.GlobalParameterDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseServiceImpl implements GlobalParameterBaseService {

/*
 * properties injected by spring
 */
@Inject
protected GlobalParameterDao globalParameterDao;
@Inject
protected GlobalParameterFullViewMapper globalParameterFullViewMapper;
@Inject
protected GlobalParameterBasicViewMapper globalParameterBasicViewMapper;
@Inject
protected GlobalParameterFormMapper globalParameterFormMapper;
@Inject
protected GlobalParameterStateManager globalParameterStateManager;
@Inject
protected GlobalParameterRightsManager globalParameterRightsManager;
@Inject
protected GlobalParameterProcessor globalParameterProcessor;

/**
 * get options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> getOptions() {
List<GlobalParameter> globalParameterList = globalParameterDao.loadList();
List<SelectItem> result = new ArrayList<>(globalParameterList.size());
for (GlobalParameter globalParameter : globalParameterList) {
result.add(new SelectItem(globalParameter.getCode(), globalParameter.getCode()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<GlobalParameterBasicView> loadList() {
globalParameterRightsManager.checkCanAccess();
List<GlobalParameter> globalParameterList = globalParameterDao.loadListEagerly();
List<GlobalParameterBasicView> result = new ArrayList<>(globalParameterList.size());
for (GlobalParameter globalParameter : globalParameterList) {
result.add(this.globalParameterBasicViewMapper.mapFrom(new GlobalParameterBasicView(),globalParameter));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<GlobalParameterBasicView> scroll(ScrollForm<GlobalParameterFilter, GlobalParameterSorting> form) {
globalParameterRightsManager.checkCanAccess();
ScrollView<GlobalParameterBasicView> result = new ScrollView<>();
result.setSize(globalParameterDao.count());
Long count = globalParameterDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<GlobalParameter> list = globalParameterDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<GlobalParameterBasicView> elements = new ArrayList<>(list.size());
for (GlobalParameter globalParameter : list) {
elements.add(this.globalParameterBasicViewMapper.mapFrom(new GlobalParameterBasicView(),globalParameter));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public GlobalParameterFullView load(Long id) {
GlobalParameter globalParameter = globalParameterDao.load(id);
globalParameterRightsManager.checkCanAccess(globalParameter);
return this.globalParameterFullViewMapper.mapFrom(new GlobalParameterFullView(),globalParameter);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public GlobalParameterFullView find(String code) {
GlobalParameter globalParameter = globalParameterDao.find(code);
globalParameterRightsManager.checkCanAccess(globalParameter);
return this.globalParameterFullViewMapper.mapFrom(new GlobalParameterFullView(), globalParameter);
}

/**
 * create object
 */
@Override
public GlobalParameterFullView create() {
globalParameterRightsManager.checkCanCreate();
return new GlobalParameterFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(GlobalParameterForm globalParameterForm) {
GlobalParameter globalParameter = this.globalParameterFormMapper.mapTo(globalParameterForm, new GlobalParameter());
globalParameterRightsManager.checkCanSave(globalParameter);
globalParameterStateManager.checkCanSave(globalParameter);
return globalParameterProcessor.save(globalParameter);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, GlobalParameterForm globalParameterForm) {
GlobalParameter globalParameter = this.globalParameterDao.load(id);
globalParameterRightsManager.checkCanUpdate(globalParameter);
globalParameterStateManager.checkCanUpdate(globalParameter);
globalParameter = this.globalParameterFormMapper.mapTo(globalParameterForm, globalParameter);
globalParameterProcessor.update(globalParameter);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
GlobalParameter globalParameter = globalParameterDao.load(id);
globalParameterRightsManager.checkCanDelete(globalParameter);
globalParameterStateManager.checkCanDelete(globalParameter);
globalParameterProcessor.delete(globalParameter);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
GlobalParameter globalParameter;
if (idList != null){
for (Long id:idList){
globalParameter = globalParameterDao.load(id);
globalParameterRightsManager.checkCanDelete(globalParameter);
globalParameterStateManager.checkCanDelete(globalParameter);
globalParameterProcessor.delete(globalParameter);
}
}
}

}
