package org.sklsft.social.bl.impl.notifications.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.notifications.base.UserAccountNotificationBaseService;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;
import org.sklsft.social.bc.mapper.notifications.forms.UserAccountNotificationFormMapper;
import org.sklsft.social.bc.mapper.notifications.views.basic.UserAccountNotificationBasicViewMapper;
import org.sklsft.social.bc.mapper.notifications.views.full.UserAccountNotificationFullViewMapper;
import org.sklsft.social.bc.processor.notifications.UserAccountNotificationProcessor;
import org.sklsft.social.bc.rightsmanager.notifications.UserAccountNotificationRightsManager;
import org.sklsft.social.bc.statemanager.notifications.UserAccountNotificationStateManager;
import org.sklsft.social.model.notifications.UserAccountNotification;
import org.sklsft.social.repository.dao.interfaces.notifications.UserAccountNotificationDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseServiceImpl implements UserAccountNotificationBaseService {

/*
 * properties injected by spring
 */
@Inject
protected UserAccountNotificationDao userAccountNotificationDao;
@Inject
protected UserAccountNotificationFullViewMapper userAccountNotificationFullViewMapper;
@Inject
protected UserAccountNotificationBasicViewMapper userAccountNotificationBasicViewMapper;
@Inject
protected UserAccountNotificationFormMapper userAccountNotificationFormMapper;
@Inject
protected UserAccountNotificationStateManager userAccountNotificationStateManager;
@Inject
protected UserAccountNotificationRightsManager userAccountNotificationRightsManager;
@Inject
protected UserAccountNotificationProcessor userAccountNotificationProcessor;

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<UserAccountNotificationBasicView> loadList() {
userAccountNotificationRightsManager.checkCanAccess();
List<UserAccountNotification> userAccountNotificationList = userAccountNotificationDao.loadListEagerly();
List<UserAccountNotificationBasicView> result = new ArrayList<>(userAccountNotificationList.size());
for (UserAccountNotification userAccountNotification : userAccountNotificationList) {
result.add(this.userAccountNotificationBasicViewMapper.mapFrom(new UserAccountNotificationBasicView(),userAccountNotification));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<UserAccountNotificationBasicView> scroll(ScrollForm<UserAccountNotificationFilter, UserAccountNotificationSorting> form) {
userAccountNotificationRightsManager.checkCanAccess();
ScrollView<UserAccountNotificationBasicView> result = new ScrollView<>();
result.setSize(userAccountNotificationDao.count());
Long count = userAccountNotificationDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<UserAccountNotification> list = userAccountNotificationDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<UserAccountNotificationBasicView> elements = new ArrayList<>(list.size());
for (UserAccountNotification userAccountNotification : list) {
elements.add(this.userAccountNotificationBasicViewMapper.mapFrom(new UserAccountNotificationBasicView(),userAccountNotification));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public UserAccountNotificationFullView load(Long id) {
UserAccountNotification userAccountNotification = userAccountNotificationDao.load(id);
userAccountNotificationRightsManager.checkCanAccess(userAccountNotification);
return this.userAccountNotificationFullViewMapper.mapFrom(new UserAccountNotificationFullView(),userAccountNotification);
}

/**
 * create object
 */
@Override
public UserAccountNotificationFullView create() {
userAccountNotificationRightsManager.checkCanCreate();
return new UserAccountNotificationFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(UserAccountNotificationForm userAccountNotificationForm) {
UserAccountNotification userAccountNotification = this.userAccountNotificationFormMapper.mapTo(userAccountNotificationForm, new UserAccountNotification());
userAccountNotificationRightsManager.checkCanSave(userAccountNotification);
userAccountNotificationStateManager.checkCanSave(userAccountNotification);
return userAccountNotificationProcessor.save(userAccountNotification);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, UserAccountNotificationForm userAccountNotificationForm) {
UserAccountNotification userAccountNotification = this.userAccountNotificationDao.load(id);
userAccountNotificationRightsManager.checkCanUpdate(userAccountNotification);
userAccountNotificationStateManager.checkCanUpdate(userAccountNotification);
userAccountNotification = this.userAccountNotificationFormMapper.mapTo(userAccountNotificationForm, userAccountNotification);
userAccountNotificationProcessor.update(userAccountNotification);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
UserAccountNotification userAccountNotification = userAccountNotificationDao.load(id);
userAccountNotificationRightsManager.checkCanDelete(userAccountNotification);
userAccountNotificationStateManager.checkCanDelete(userAccountNotification);
userAccountNotificationProcessor.delete(userAccountNotification);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
UserAccountNotification userAccountNotification;
if (idList != null){
for (Long id:idList){
userAccountNotification = userAccountNotificationDao.load(id);
userAccountNotificationRightsManager.checkCanDelete(userAccountNotification);
userAccountNotificationStateManager.checkCanDelete(userAccountNotification);
userAccountNotificationProcessor.delete(userAccountNotification);
}
}
}

}
