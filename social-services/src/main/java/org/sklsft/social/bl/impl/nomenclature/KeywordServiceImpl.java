package org.sklsft.social.bl.impl.nomenclature;

import org.sklsft.social.api.interfaces.nomenclature.KeywordService;
import org.sklsft.social.bl.impl.nomenclature.base.KeywordBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialKeywordServiceImpl")
public class KeywordServiceImpl extends KeywordBaseServiceImpl implements KeywordService{

/* Specific Code Start */
/* Specific Code End */
}
