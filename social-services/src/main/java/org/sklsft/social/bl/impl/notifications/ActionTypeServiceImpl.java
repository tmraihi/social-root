package org.sklsft.social.bl.impl.notifications;

import org.sklsft.social.api.interfaces.notifications.ActionTypeService;
import org.sklsft.social.bl.impl.notifications.base.ActionTypeBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialActionTypeServiceImpl")
public class ActionTypeServiceImpl extends ActionTypeBaseServiceImpl implements ActionTypeService{

/* Specific Code Start */
/* Specific Code End */
}
