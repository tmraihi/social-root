package org.sklsft.social.bl.impl.account;

import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.bl.impl.account.base.UserAccountBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialUserAccountServiceImpl")
public class UserAccountServiceImpl extends UserAccountBaseServiceImpl implements UserAccountService{

/* Specific Code Start */
/* Specific Code End */
}
