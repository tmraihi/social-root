package org.sklsft.social.bl.impl.organization.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.organization.base.OrganizationBaseService;
import org.sklsft.social.api.model.organization.filters.OrganizationFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationFullView;
import org.sklsft.social.bc.mapper.organization.forms.OrganizationFormMapper;
import org.sklsft.social.bc.mapper.organization.views.basic.OrganizationBasicViewMapper;
import org.sklsft.social.bc.mapper.organization.views.full.OrganizationFullViewMapper;
import org.sklsft.social.bc.processor.organization.OrganizationProcessor;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationStateManager;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBaseServiceImpl implements OrganizationBaseService {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationDao organizationDao;
@Inject
protected OrganizationFullViewMapper organizationFullViewMapper;
@Inject
protected OrganizationBasicViewMapper organizationBasicViewMapper;
@Inject
protected OrganizationFormMapper organizationFormMapper;
@Inject
protected OrganizationStateManager organizationStateManager;
@Inject
protected OrganizationRightsManager organizationRightsManager;
@Inject
protected OrganizationProcessor organizationProcessor;

/**
 * search options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> searchOptions(String arg) {
List<Organization> organizationList = organizationDao.search(arg);
List<SelectItem> result = new ArrayList<>(organizationList.size());
for (Organization organization : organizationList) {
result.add(new SelectItem(organization.getCode(), organization.getCode()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationBasicView> loadList() {
organizationRightsManager.checkCanAccess();
List<Organization> organizationList = organizationDao.loadListEagerly();
List<OrganizationBasicView> result = new ArrayList<>(organizationList.size());
for (Organization organization : organizationList) {
result.add(this.organizationBasicViewMapper.mapFrom(new OrganizationBasicView(),organization));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationBasicView> scroll(ScrollForm<OrganizationFilter, OrganizationSorting> form) {
organizationRightsManager.checkCanAccess();
ScrollView<OrganizationBasicView> result = new ScrollView<>();
result.setSize(organizationDao.count());
Long count = organizationDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<Organization> list = organizationDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationBasicView> elements = new ArrayList<>(list.size());
for (Organization organization : list) {
elements.add(this.organizationBasicViewMapper.mapFrom(new OrganizationBasicView(),organization));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public OrganizationFullView load(Long id) {
Organization organization = organizationDao.load(id);
organizationRightsManager.checkCanAccess(organization);
return this.organizationFullViewMapper.mapFrom(new OrganizationFullView(),organization);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public OrganizationFullView find(String code) {
Organization organization = organizationDao.find(code);
organizationRightsManager.checkCanAccess(organization);
return this.organizationFullViewMapper.mapFrom(new OrganizationFullView(), organization);
}

/**
 * create object
 */
@Override
public OrganizationFullView create() {
organizationRightsManager.checkCanCreate();
return new OrganizationFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(OrganizationForm organizationForm) {
Organization organization = this.organizationFormMapper.mapTo(organizationForm, new Organization());
organizationRightsManager.checkCanSave(organization);
organizationStateManager.checkCanSave(organization);
return organizationProcessor.save(organization);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, OrganizationForm organizationForm) {
Organization organization = this.organizationDao.load(id);
organizationRightsManager.checkCanUpdate(organization);
organizationStateManager.checkCanUpdate(organization);
organization = this.organizationFormMapper.mapTo(organizationForm, organization);
organizationProcessor.update(organization);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
Organization organization = organizationDao.load(id);
organizationRightsManager.checkCanDelete(organization);
organizationStateManager.checkCanDelete(organization);
organizationProcessor.delete(organization);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
Organization organization;
if (idList != null){
for (Long id:idList){
organization = organizationDao.load(id);
organizationRightsManager.checkCanDelete(organization);
organizationStateManager.checkCanDelete(organization);
organizationProcessor.delete(organization);
}
}
}

}
