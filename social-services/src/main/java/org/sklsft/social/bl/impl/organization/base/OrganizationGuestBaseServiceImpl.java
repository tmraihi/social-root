package org.sklsft.social.bl.impl.organization.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.organization.base.OrganizationGuestBaseService;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.bc.mapper.organization.forms.OrganizationGuestFormMapper;
import org.sklsft.social.bc.mapper.organization.views.basic.OrganizationGuestBasicViewMapper;
import org.sklsft.social.bc.mapper.organization.views.full.OrganizationGuestFullViewMapper;
import org.sklsft.social.bc.processor.organization.OrganizationGuestProcessor;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationGuestRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationGuestStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseServiceImpl implements OrganizationGuestBaseService {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationGuestDao organizationGuestDao;
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;
@Inject
protected OrganizationGuestFullViewMapper organizationGuestFullViewMapper;
@Inject
protected OrganizationGuestBasicViewMapper organizationGuestBasicViewMapper;
@Inject
protected OrganizationGuestFormMapper organizationGuestFormMapper;
@Inject
protected OrganizationGuestStateManager organizationGuestStateManager;
@Inject
protected OrganizationGuestRightsManager organizationGuestRightsManager;
@Inject
protected OrganizationGuestProcessor organizationGuestProcessor;

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationGuestBasicView> loadList() {
organizationGuestRightsManager.checkCanAccess();
List<OrganizationGuest> organizationGuestList = organizationGuestDao.loadListEagerly();
List<OrganizationGuestBasicView> result = new ArrayList<>(organizationGuestList.size());
for (OrganizationGuest organizationGuest : organizationGuestList) {
result.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
return result;
}

/**
 * load object list from organization
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationGuestBasicView> loadListFromOrganization (Long organizationId) {
organizationGuestRightsManager.checkCanAccess();
List<OrganizationGuest> organizationGuestList = organizationGuestDao.loadListEagerlyFromOrganization(organizationId);
List<OrganizationGuestBasicView> result = new ArrayList<>(organizationGuestList.size());
for (OrganizationGuest organizationGuest : organizationGuestList) {
result.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
return result;
}

/**
 * load object list from userAccount
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationGuestBasicView> loadListFromUserAccount (Long userAccountId) {
organizationGuestRightsManager.checkCanAccess();
List<OrganizationGuest> organizationGuestList = organizationGuestDao.loadListEagerlyFromUserAccount(userAccountId);
List<OrganizationGuestBasicView> result = new ArrayList<>(organizationGuestList.size());
for (OrganizationGuest organizationGuest : organizationGuestList) {
result.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationGuestBasicView> scroll(ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form) {
organizationGuestRightsManager.checkCanAccess();
ScrollView<OrganizationGuestBasicView> result = new ScrollView<>();
result.setSize(organizationGuestDao.count());
Long count = organizationGuestDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationGuest> list = organizationGuestDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationGuestBasicView> elements = new ArrayList<>(list.size());
for (OrganizationGuest organizationGuest : list) {
elements.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
result.setElements(elements);
return result;
}

/**
 * scroll object list from organization
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationGuestBasicView> scrollFromOrganization (Long organizationId, ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form) {
organizationGuestRightsManager.checkCanAccess();
ScrollView<OrganizationGuestBasicView> result = new ScrollView<>();
result.setSize(organizationGuestDao.countFromOrganization(organizationId));
Long count = organizationGuestDao.countFromOrganization(organizationId, form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationGuest> list = organizationGuestDao.scrollFromOrganization(organizationId, form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationGuestBasicView> elements = new ArrayList<>(list.size());
for (OrganizationGuest organizationGuest : list) {
elements.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
result.setElements(elements);
return result;
}

/**
 * scroll object list from userAccount
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationGuestBasicView> scrollFromUserAccount (Long userAccountId, ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form) {
organizationGuestRightsManager.checkCanAccess();
ScrollView<OrganizationGuestBasicView> result = new ScrollView<>();
result.setSize(organizationGuestDao.countFromUserAccount(userAccountId));
Long count = organizationGuestDao.countFromUserAccount(userAccountId, form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationGuest> list = organizationGuestDao.scrollFromUserAccount(userAccountId, form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationGuestBasicView> elements = new ArrayList<>(list.size());
for (OrganizationGuest organizationGuest : list) {
elements.add(this.organizationGuestBasicViewMapper.mapFrom(new OrganizationGuestBasicView(),organizationGuest));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public OrganizationGuestFullView load(Long id) {
OrganizationGuest organizationGuest = organizationGuestDao.load(id);
organizationGuestRightsManager.checkCanAccess(organizationGuest);
return this.organizationGuestFullViewMapper.mapFrom(new OrganizationGuestFullView(),organizationGuest);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public OrganizationGuestFullView find(String organizationCode, String email) {
OrganizationGuest organizationGuest = organizationGuestDao.find(organizationCode, email);
organizationGuestRightsManager.checkCanAccess(organizationGuest);
return this.organizationGuestFullViewMapper.mapFrom(new OrganizationGuestFullView(), organizationGuest);
}

/**
 * create object
 */
@Override
public OrganizationGuestFullView create() {
organizationGuestRightsManager.checkCanCreate();
return new OrganizationGuestFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(OrganizationGuestForm organizationGuestForm) {
OrganizationGuest organizationGuest = this.organizationGuestFormMapper.mapTo(organizationGuestForm, new OrganizationGuest());
organizationGuestRightsManager.checkCanSave(organizationGuest);
organizationGuestStateManager.checkCanSave(organizationGuest);
return organizationGuestProcessor.save(organizationGuest);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, OrganizationGuestForm organizationGuestForm) {
OrganizationGuest organizationGuest = this.organizationGuestDao.load(id);
organizationGuestRightsManager.checkCanUpdate(organizationGuest);
organizationGuestStateManager.checkCanUpdate(organizationGuest);
organizationGuest = this.organizationGuestFormMapper.mapTo(organizationGuestForm, organizationGuest);
organizationGuestProcessor.update(organizationGuest);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
OrganizationGuest organizationGuest = organizationGuestDao.load(id);
organizationGuestRightsManager.checkCanDelete(organizationGuest);
organizationGuestStateManager.checkCanDelete(organizationGuest);
organizationGuestProcessor.delete(organizationGuest);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
OrganizationGuest organizationGuest;
if (idList != null){
for (Long id:idList){
organizationGuest = organizationGuestDao.load(id);
organizationGuestRightsManager.checkCanDelete(organizationGuest);
organizationGuestStateManager.checkCanDelete(organizationGuest);
organizationGuestProcessor.delete(organizationGuest);
}
}
}

}
