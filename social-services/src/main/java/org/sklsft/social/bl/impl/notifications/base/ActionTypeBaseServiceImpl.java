package org.sklsft.social.bl.impl.notifications.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.notifications.base.ActionTypeBaseService;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.forms.ActionTypeForm;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.api.model.notifications.views.basic.ActionTypeBasicView;
import org.sklsft.social.api.model.notifications.views.full.ActionTypeFullView;
import org.sklsft.social.bc.mapper.notifications.forms.ActionTypeFormMapper;
import org.sklsft.social.bc.mapper.notifications.views.basic.ActionTypeBasicViewMapper;
import org.sklsft.social.bc.mapper.notifications.views.full.ActionTypeFullViewMapper;
import org.sklsft.social.bc.processor.notifications.ActionTypeProcessor;
import org.sklsft.social.bc.rightsmanager.notifications.ActionTypeRightsManager;
import org.sklsft.social.bc.statemanager.notifications.ActionTypeStateManager;
import org.sklsft.social.model.notifications.ActionType;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseServiceImpl implements ActionTypeBaseService {

/*
 * properties injected by spring
 */
@Inject
protected ActionTypeDao actionTypeDao;
@Inject
protected ActionTypeFullViewMapper actionTypeFullViewMapper;
@Inject
protected ActionTypeBasicViewMapper actionTypeBasicViewMapper;
@Inject
protected ActionTypeFormMapper actionTypeFormMapper;
@Inject
protected ActionTypeStateManager actionTypeStateManager;
@Inject
protected ActionTypeRightsManager actionTypeRightsManager;
@Inject
protected ActionTypeProcessor actionTypeProcessor;

/**
 * get options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> getOptions() {
List<ActionType> actionTypeList = actionTypeDao.loadList();
List<SelectItem> result = new ArrayList<>(actionTypeList.size());
for (ActionType actionType : actionTypeList) {
result.add(new SelectItem(actionType.getCode(), actionType.getCode()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<ActionTypeBasicView> loadList() {
actionTypeRightsManager.checkCanAccess();
List<ActionType> actionTypeList = actionTypeDao.loadListEagerly();
List<ActionTypeBasicView> result = new ArrayList<>(actionTypeList.size());
for (ActionType actionType : actionTypeList) {
result.add(this.actionTypeBasicViewMapper.mapFrom(new ActionTypeBasicView(),actionType));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<ActionTypeBasicView> scroll(ScrollForm<ActionTypeFilter, ActionTypeSorting> form) {
actionTypeRightsManager.checkCanAccess();
ScrollView<ActionTypeBasicView> result = new ScrollView<>();
result.setSize(actionTypeDao.count());
Long count = actionTypeDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<ActionType> list = actionTypeDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<ActionTypeBasicView> elements = new ArrayList<>(list.size());
for (ActionType actionType : list) {
elements.add(this.actionTypeBasicViewMapper.mapFrom(new ActionTypeBasicView(),actionType));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public ActionTypeFullView load(Long id) {
ActionType actionType = actionTypeDao.load(id);
actionTypeRightsManager.checkCanAccess(actionType);
return this.actionTypeFullViewMapper.mapFrom(new ActionTypeFullView(),actionType);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public ActionTypeFullView find(String code) {
ActionType actionType = actionTypeDao.find(code);
actionTypeRightsManager.checkCanAccess(actionType);
return this.actionTypeFullViewMapper.mapFrom(new ActionTypeFullView(), actionType);
}

/**
 * create object
 */
@Override
public ActionTypeFullView create() {
actionTypeRightsManager.checkCanCreate();
return new ActionTypeFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(ActionTypeForm actionTypeForm) {
ActionType actionType = this.actionTypeFormMapper.mapTo(actionTypeForm, new ActionType());
actionTypeRightsManager.checkCanSave(actionType);
actionTypeStateManager.checkCanSave(actionType);
return actionTypeProcessor.save(actionType);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, ActionTypeForm actionTypeForm) {
ActionType actionType = this.actionTypeDao.load(id);
actionTypeRightsManager.checkCanUpdate(actionType);
actionTypeStateManager.checkCanUpdate(actionType);
actionType = this.actionTypeFormMapper.mapTo(actionTypeForm, actionType);
actionTypeProcessor.update(actionType);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
ActionType actionType = actionTypeDao.load(id);
actionTypeRightsManager.checkCanDelete(actionType);
actionTypeStateManager.checkCanDelete(actionType);
actionTypeProcessor.delete(actionType);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
ActionType actionType;
if (idList != null){
for (Long id:idList){
actionType = actionTypeDao.load(id);
actionTypeRightsManager.checkCanDelete(actionType);
actionTypeStateManager.checkCanDelete(actionType);
actionTypeProcessor.delete(actionType);
}
}
}

}
