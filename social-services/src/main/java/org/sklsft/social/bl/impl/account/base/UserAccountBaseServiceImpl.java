package org.sklsft.social.bl.impl.account.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.account.base.UserAccountBaseService;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.account.views.basic.UserAccountBasicView;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationPreferencesFilter;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationPreferencesForm;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationPreferencesSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationPreferencesBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationPreferencesFullView;
import org.sklsft.social.bc.mapper.account.forms.UserAccountFormMapper;
import org.sklsft.social.bc.mapper.account.views.basic.UserAccountBasicViewMapper;
import org.sklsft.social.bc.mapper.account.views.full.UserAccountFullViewMapper;
import org.sklsft.social.bc.mapper.notifications.forms.UserAccountNotificationPreferencesFormMapper;
import org.sklsft.social.bc.mapper.notifications.views.basic.UserAccountNotificationPreferencesBasicViewMapper;
import org.sklsft.social.bc.mapper.notifications.views.full.UserAccountNotificationPreferencesFullViewMapper;
import org.sklsft.social.bc.processor.account.UserAccountProcessor;
import org.sklsft.social.bc.rightsmanager.account.UserAccountRightsManager;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseServiceImpl implements UserAccountBaseService {

/*
 * properties injected by spring
 */
@Inject
protected UserAccountDao userAccountDao;
@Inject
protected UserAccountFullViewMapper userAccountFullViewMapper;
@Inject
protected UserAccountBasicViewMapper userAccountBasicViewMapper;
@Inject
protected UserAccountFormMapper userAccountFormMapper;
@Inject
protected UserAccountNotificationPreferencesFullViewMapper userAccountNotificationPreferencesFullViewMapper;
@Inject
protected UserAccountNotificationPreferencesBasicViewMapper userAccountNotificationPreferencesBasicViewMapper;
@Inject
protected UserAccountNotificationPreferencesFormMapper userAccountNotificationPreferencesFormMapper;
@Inject
protected UserAccountStateManager userAccountStateManager;
@Inject
protected UserAccountRightsManager userAccountRightsManager;
@Inject
protected UserAccountProcessor userAccountProcessor;

/**
 * search options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> searchOptions(String arg) {
List<UserAccount> userAccountList = userAccountDao.search(arg);
List<SelectItem> result = new ArrayList<>(userAccountList.size());
for (UserAccount userAccount : userAccountList) {
result.add(new SelectItem(userAccount.getEmail(), userAccount.getEmail()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<UserAccountBasicView> loadList() {
userAccountRightsManager.checkCanAccess();
List<UserAccount> userAccountList = userAccountDao.loadListEagerly();
List<UserAccountBasicView> result = new ArrayList<>(userAccountList.size());
for (UserAccount userAccount : userAccountList) {
result.add(this.userAccountBasicViewMapper.mapFrom(new UserAccountBasicView(),userAccount));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<UserAccountBasicView> scroll(ScrollForm<UserAccountFilter, UserAccountSorting> form) {
userAccountRightsManager.checkCanAccess();
ScrollView<UserAccountBasicView> result = new ScrollView<>();
result.setSize(userAccountDao.count());
Long count = userAccountDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<UserAccount> list = userAccountDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<UserAccountBasicView> elements = new ArrayList<>(list.size());
for (UserAccount userAccount : list) {
elements.add(this.userAccountBasicViewMapper.mapFrom(new UserAccountBasicView(),userAccount));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public UserAccountFullView load(Long id) {
UserAccount userAccount = userAccountDao.load(id);
userAccountRightsManager.checkCanAccess(userAccount);
return this.userAccountFullViewMapper.mapFrom(new UserAccountFullView(),userAccount);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public UserAccountFullView find(String email) {
UserAccount userAccount = userAccountDao.find(email);
userAccountRightsManager.checkCanAccess(userAccount);
return this.userAccountFullViewMapper.mapFrom(new UserAccountFullView(), userAccount);
}

/**
 * load one to many component userAccountNotificationPreferences list
 */
@Override
@Transactional(readOnly=true)
public List<UserAccountNotificationPreferencesBasicView> loadUserAccountNotificationPreferencesList(Long id) {
UserAccount userAccount = userAccountDao.load(id);
userAccountRightsManager.checkCanAccessUserAccountNotificationPreferences(userAccount);
List<UserAccountNotificationPreferences> userAccountNotificationPreferencesList = userAccountDao.loadUserAccountNotificationPreferencesList(id);
List<UserAccountNotificationPreferencesBasicView> result = new ArrayList<>(userAccountNotificationPreferencesList.size());
for (UserAccountNotificationPreferences userAccountNotificationPreferences:userAccountNotificationPreferencesList){
result.add(this.userAccountNotificationPreferencesBasicViewMapper.mapFrom(new UserAccountNotificationPreferencesBasicView(),userAccountNotificationPreferences));
}
return result;
}

/**
 * scroll one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(readOnly=true)
public ScrollView<UserAccountNotificationPreferencesBasicView> scrollUserAccountNotificationPreferences (Long userAccountId, ScrollForm<UserAccountNotificationPreferencesFilter, UserAccountNotificationPreferencesSorting> form) {
UserAccount userAccount = userAccountDao.load(userAccountId);
userAccountRightsManager.checkCanAccessUserAccountNotificationPreferences(userAccount);
ScrollView<UserAccountNotificationPreferencesBasicView> result = new ScrollView<>();
result.setSize(userAccountDao.countUserAccountNotificationPreferences(userAccountId));
Long count = userAccountDao.countUserAccountNotificationPreferences(userAccountId, form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<UserAccountNotificationPreferences> list = userAccountDao.scrollUserAccountNotificationPreferences(userAccountId, form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<UserAccountNotificationPreferencesBasicView> elements = new ArrayList<>(list.size());
for (UserAccountNotificationPreferences userAccountNotificationPreferences : list) {
elements.add(this.userAccountNotificationPreferencesBasicViewMapper.mapFrom(new UserAccountNotificationPreferencesBasicView(),userAccountNotificationPreferences));
}
result.setElements(elements);
return result;
}

/**
 * load one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(readOnly=true)
public UserAccountNotificationPreferencesFullView loadUserAccountNotificationPreferences(Long id) {
UserAccountNotificationPreferences userAccountNotificationPreferences = userAccountDao.loadUserAccountNotificationPreferences(id);
userAccountRightsManager.checkCanAccessUserAccountNotificationPreferences(userAccountNotificationPreferences.getUserAccount());
return this.userAccountNotificationPreferencesFullViewMapper.mapFrom(userAccountNotificationPreferences);
}

/**
 * create object
 */
@Override
public UserAccountFullView create() {
userAccountRightsManager.checkCanCreate();
return new UserAccountFullView();
}

/**
 * create one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(readOnly=true)
public UserAccountNotificationPreferencesFullView createUserAccountNotificationPreferences(Long id) {
UserAccount userAccount = userAccountDao.load(id);
userAccountRightsManager.checkCanCreateUserAccountNotificationPreferences(userAccount);
return new UserAccountNotificationPreferencesFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(UserAccountForm userAccountForm) {
UserAccount userAccount = this.userAccountFormMapper.mapTo(userAccountForm, new UserAccount());
userAccountRightsManager.checkCanSave(userAccount);
userAccountStateManager.checkCanSave(userAccount);
return userAccountProcessor.save(userAccount);
}

/**
 * save one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void saveUserAccountNotificationPreferences(Long id, UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm) {
UserAccount userAccount = this.userAccountDao.load(id);
UserAccountNotificationPreferences userAccountNotificationPreferences = this.userAccountNotificationPreferencesFormMapper.mapTo(userAccountNotificationPreferencesForm, new UserAccountNotificationPreferences());
userAccountRightsManager.checkCanSaveUserAccountNotificationPreferences(userAccountNotificationPreferences,userAccount);
userAccountStateManager.checkCanSaveUserAccountNotificationPreferences(userAccountNotificationPreferences,userAccount);
userAccountProcessor.saveUserAccountNotificationPreferences(userAccountNotificationPreferences,userAccount);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, UserAccountForm userAccountForm) {
UserAccount userAccount = this.userAccountDao.load(id);
userAccountRightsManager.checkCanUpdate(userAccount);
userAccountStateManager.checkCanUpdate(userAccount);
userAccount = this.userAccountFormMapper.mapTo(userAccountForm, userAccount);
userAccountProcessor.update(userAccount);
}

/**
 * update one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void updateUserAccountNotificationPreferences(Long id, UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm) {
UserAccountNotificationPreferences userAccountNotificationPreferences = this.userAccountDao.loadUserAccountNotificationPreferences(id);
userAccountRightsManager.checkCanUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences);
userAccountStateManager.checkCanUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences);
userAccountNotificationPreferences = this.userAccountNotificationPreferencesFormMapper.mapTo(userAccountNotificationPreferencesForm, userAccountNotificationPreferences);
userAccountProcessor.updateUserAccountNotificationPreferences(userAccountNotificationPreferences);
}


/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
UserAccount userAccount = userAccountDao.load(id);
userAccountRightsManager.checkCanDelete(userAccount);
userAccountStateManager.checkCanDelete(userAccount);
userAccountProcessor.delete(userAccount);
}

/**
 * delete one to many component userAccountNotificationPreferences
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteUserAccountNotificationPreferences(Long id) {
UserAccountNotificationPreferences userAccountNotificationPreferences = userAccountDao.loadUserAccountNotificationPreferences(id);
userAccountRightsManager.checkCanDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
userAccountStateManager.checkCanDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
this.userAccountProcessor.deleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
UserAccount userAccount;
if (idList != null){
for (Long id:idList){
userAccount = userAccountDao.load(id);
userAccountRightsManager.checkCanDelete(userAccount);
userAccountStateManager.checkCanDelete(userAccount);
userAccountProcessor.delete(userAccount);
}
}
}

/**
 * delete one to many component userAccountNotificationPreferences list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteUserAccountNotificationPreferencesList(List<Long> idList) {
UserAccountNotificationPreferences userAccountNotificationPreferences;
if (idList != null){
for (Long i:idList){
userAccountNotificationPreferences = userAccountDao.loadUserAccountNotificationPreferences(i);
userAccountRightsManager.checkCanDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
userAccountStateManager.checkCanDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
this.userAccountProcessor.deleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
}
}
}

}
