package org.sklsft.social.bl.impl.organization.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.organization.base.OrganizationMemberBaseService;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.sklsft.social.bc.mapper.organization.forms.OrganizationMemberFormMapper;
import org.sklsft.social.bc.mapper.organization.views.basic.OrganizationMemberBasicViewMapper;
import org.sklsft.social.bc.mapper.organization.views.full.OrganizationMemberFullViewMapper;
import org.sklsft.social.bc.processor.organization.OrganizationMemberProcessor;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationMemberRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationMemberStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseServiceImpl implements OrganizationMemberBaseService {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationMemberDao organizationMemberDao;
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;
@Inject
protected OrganizationMemberFullViewMapper organizationMemberFullViewMapper;
@Inject
protected OrganizationMemberBasicViewMapper organizationMemberBasicViewMapper;
@Inject
protected OrganizationMemberFormMapper organizationMemberFormMapper;
@Inject
protected OrganizationMemberStateManager organizationMemberStateManager;
@Inject
protected OrganizationMemberRightsManager organizationMemberRightsManager;
@Inject
protected OrganizationMemberProcessor organizationMemberProcessor;

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationMemberBasicView> loadList() {
organizationMemberRightsManager.checkCanAccess();
List<OrganizationMember> organizationMemberList = organizationMemberDao.loadListEagerly();
List<OrganizationMemberBasicView> result = new ArrayList<>(organizationMemberList.size());
for (OrganizationMember organizationMember : organizationMemberList) {
result.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
return result;
}

/**
 * load object list from organization
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationMemberBasicView> loadListFromOrganization (Long organizationId) {
organizationMemberRightsManager.checkCanAccess();
List<OrganizationMember> organizationMemberList = organizationMemberDao.loadListEagerlyFromOrganization(organizationId);
List<OrganizationMemberBasicView> result = new ArrayList<>(organizationMemberList.size());
for (OrganizationMember organizationMember : organizationMemberList) {
result.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
return result;
}

/**
 * load object list from userAccount
 */
@Override
@Transactional(readOnly=true)
public List<OrganizationMemberBasicView> loadListFromUserAccount (Long userAccountId) {
organizationMemberRightsManager.checkCanAccess();
List<OrganizationMember> organizationMemberList = organizationMemberDao.loadListEagerlyFromUserAccount(userAccountId);
List<OrganizationMemberBasicView> result = new ArrayList<>(organizationMemberList.size());
for (OrganizationMember organizationMember : organizationMemberList) {
result.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationMemberBasicView> scroll(ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form) {
organizationMemberRightsManager.checkCanAccess();
ScrollView<OrganizationMemberBasicView> result = new ScrollView<>();
result.setSize(organizationMemberDao.count());
Long count = organizationMemberDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationMember> list = organizationMemberDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationMemberBasicView> elements = new ArrayList<>(list.size());
for (OrganizationMember organizationMember : list) {
elements.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
result.setElements(elements);
return result;
}

/**
 * scroll object list from organization
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationMemberBasicView> scrollFromOrganization (Long organizationId, ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form) {
organizationMemberRightsManager.checkCanAccess();
ScrollView<OrganizationMemberBasicView> result = new ScrollView<>();
result.setSize(organizationMemberDao.countFromOrganization(organizationId));
Long count = organizationMemberDao.countFromOrganization(organizationId, form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationMember> list = organizationMemberDao.scrollFromOrganization(organizationId, form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationMemberBasicView> elements = new ArrayList<>(list.size());
for (OrganizationMember organizationMember : list) {
elements.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
result.setElements(elements);
return result;
}

/**
 * scroll object list from userAccount
 */
@Override
@Transactional(readOnly=true)
public ScrollView<OrganizationMemberBasicView> scrollFromUserAccount (Long userAccountId, ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form) {
organizationMemberRightsManager.checkCanAccess();
ScrollView<OrganizationMemberBasicView> result = new ScrollView<>();
result.setSize(organizationMemberDao.countFromUserAccount(userAccountId));
Long count = organizationMemberDao.countFromUserAccount(userAccountId, form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<OrganizationMember> list = organizationMemberDao.scrollFromUserAccount(userAccountId, form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<OrganizationMemberBasicView> elements = new ArrayList<>(list.size());
for (OrganizationMember organizationMember : list) {
elements.add(this.organizationMemberBasicViewMapper.mapFrom(new OrganizationMemberBasicView(),organizationMember));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public OrganizationMemberFullView load(Long id) {
OrganizationMember organizationMember = organizationMemberDao.load(id);
organizationMemberRightsManager.checkCanAccess(organizationMember);
return this.organizationMemberFullViewMapper.mapFrom(new OrganizationMemberFullView(),organizationMember);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public OrganizationMemberFullView find(String organizationCode, String userAccountEmail) {
OrganizationMember organizationMember = organizationMemberDao.find(organizationCode, userAccountEmail);
organizationMemberRightsManager.checkCanAccess(organizationMember);
return this.organizationMemberFullViewMapper.mapFrom(new OrganizationMemberFullView(), organizationMember);
}

/**
 * create object
 */
@Override
public OrganizationMemberFullView create() {
organizationMemberRightsManager.checkCanCreate();
return new OrganizationMemberFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(OrganizationMemberForm organizationMemberForm) {
OrganizationMember organizationMember = this.organizationMemberFormMapper.mapTo(organizationMemberForm, new OrganizationMember());
organizationMemberRightsManager.checkCanSave(organizationMember);
organizationMemberStateManager.checkCanSave(organizationMember);
return organizationMemberProcessor.save(organizationMember);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, OrganizationMemberForm organizationMemberForm) {
OrganizationMember organizationMember = this.organizationMemberDao.load(id);
organizationMemberRightsManager.checkCanUpdate(organizationMember);
organizationMemberStateManager.checkCanUpdate(organizationMember);
organizationMember = this.organizationMemberFormMapper.mapTo(organizationMemberForm, organizationMember);
organizationMemberProcessor.update(organizationMember);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
OrganizationMember organizationMember = organizationMemberDao.load(id);
organizationMemberRightsManager.checkCanDelete(organizationMember);
organizationMemberStateManager.checkCanDelete(organizationMember);
organizationMemberProcessor.delete(organizationMember);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
OrganizationMember organizationMember;
if (idList != null){
for (Long id:idList){
organizationMember = organizationMemberDao.load(id);
organizationMemberRightsManager.checkCanDelete(organizationMember);
organizationMemberStateManager.checkCanDelete(organizationMember);
organizationMemberProcessor.delete(organizationMember);
}
}
}

}
