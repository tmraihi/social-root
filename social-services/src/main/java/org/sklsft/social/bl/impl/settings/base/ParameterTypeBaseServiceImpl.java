package org.sklsft.social.bl.impl.settings.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.settings.base.ParameterTypeBaseService;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.api.model.settings.views.basic.ParameterTypeBasicView;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;
import org.sklsft.social.bc.mapper.settings.forms.ParameterTypeFormMapper;
import org.sklsft.social.bc.mapper.settings.views.basic.ParameterTypeBasicViewMapper;
import org.sklsft.social.bc.mapper.settings.views.full.ParameterTypeFullViewMapper;
import org.sklsft.social.bc.processor.settings.ParameterTypeProcessor;
import org.sklsft.social.bc.rightsmanager.settings.ParameterTypeRightsManager;
import org.sklsft.social.bc.statemanager.settings.ParameterTypeStateManager;
import org.sklsft.social.model.settings.ParameterType;
import org.sklsft.social.repository.dao.interfaces.settings.ParameterTypeDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseServiceImpl implements ParameterTypeBaseService {

/*
 * properties injected by spring
 */
@Inject
protected ParameterTypeDao parameterTypeDao;
@Inject
protected ParameterTypeFullViewMapper parameterTypeFullViewMapper;
@Inject
protected ParameterTypeBasicViewMapper parameterTypeBasicViewMapper;
@Inject
protected ParameterTypeFormMapper parameterTypeFormMapper;
@Inject
protected ParameterTypeStateManager parameterTypeStateManager;
@Inject
protected ParameterTypeRightsManager parameterTypeRightsManager;
@Inject
protected ParameterTypeProcessor parameterTypeProcessor;

/**
 * get options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> getOptions() {
List<ParameterType> parameterTypeList = parameterTypeDao.loadList();
List<SelectItem> result = new ArrayList<>(parameterTypeList.size());
for (ParameterType parameterType : parameterTypeList) {
result.add(new SelectItem(parameterType.getCode(), parameterType.getCode()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<ParameterTypeBasicView> loadList() {
parameterTypeRightsManager.checkCanAccess();
List<ParameterType> parameterTypeList = parameterTypeDao.loadListEagerly();
List<ParameterTypeBasicView> result = new ArrayList<>(parameterTypeList.size());
for (ParameterType parameterType : parameterTypeList) {
result.add(this.parameterTypeBasicViewMapper.mapFrom(new ParameterTypeBasicView(),parameterType));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<ParameterTypeBasicView> scroll(ScrollForm<ParameterTypeFilter, ParameterTypeSorting> form) {
parameterTypeRightsManager.checkCanAccess();
ScrollView<ParameterTypeBasicView> result = new ScrollView<>();
result.setSize(parameterTypeDao.count());
Long count = parameterTypeDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<ParameterType> list = parameterTypeDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<ParameterTypeBasicView> elements = new ArrayList<>(list.size());
for (ParameterType parameterType : list) {
elements.add(this.parameterTypeBasicViewMapper.mapFrom(new ParameterTypeBasicView(),parameterType));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public ParameterTypeFullView load(Long id) {
ParameterType parameterType = parameterTypeDao.load(id);
parameterTypeRightsManager.checkCanAccess(parameterType);
return this.parameterTypeFullViewMapper.mapFrom(new ParameterTypeFullView(),parameterType);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public ParameterTypeFullView find(String code) {
ParameterType parameterType = parameterTypeDao.find(code);
parameterTypeRightsManager.checkCanAccess(parameterType);
return this.parameterTypeFullViewMapper.mapFrom(new ParameterTypeFullView(), parameterType);
}

/**
 * create object
 */
@Override
public ParameterTypeFullView create() {
parameterTypeRightsManager.checkCanCreate();
return new ParameterTypeFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(ParameterTypeForm parameterTypeForm) {
ParameterType parameterType = this.parameterTypeFormMapper.mapTo(parameterTypeForm, new ParameterType());
parameterTypeRightsManager.checkCanSave(parameterType);
parameterTypeStateManager.checkCanSave(parameterType);
return parameterTypeProcessor.save(parameterType);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, ParameterTypeForm parameterTypeForm) {
ParameterType parameterType = this.parameterTypeDao.load(id);
parameterTypeRightsManager.checkCanUpdate(parameterType);
parameterTypeStateManager.checkCanUpdate(parameterType);
parameterType = this.parameterTypeFormMapper.mapTo(parameterTypeForm, parameterType);
parameterTypeProcessor.update(parameterType);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
ParameterType parameterType = parameterTypeDao.load(id);
parameterTypeRightsManager.checkCanDelete(parameterType);
parameterTypeStateManager.checkCanDelete(parameterType);
parameterTypeProcessor.delete(parameterType);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
ParameterType parameterType;
if (idList != null){
for (Long id:idList){
parameterType = parameterTypeDao.load(id);
parameterTypeRightsManager.checkCanDelete(parameterType);
parameterTypeStateManager.checkCanDelete(parameterType);
parameterTypeProcessor.delete(parameterType);
}
}
}

}
