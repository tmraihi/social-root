package org.sklsft.social.bl.impl.organization;

import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.bl.impl.organization.base.OrganizationMemberBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialOrganizationMemberServiceImpl")
public class OrganizationMemberServiceImpl extends OrganizationMemberBaseServiceImpl implements OrganizationMemberService{

/* Specific Code Start */
/* Specific Code End */
}
