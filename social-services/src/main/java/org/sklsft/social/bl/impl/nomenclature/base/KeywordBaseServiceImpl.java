package org.sklsft.social.bl.impl.nomenclature.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.interfaces.nomenclature.base.KeywordBaseService;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.api.model.nomenclature.views.basic.KeywordBasicView;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;
import org.sklsft.social.bc.mapper.nomenclature.forms.KeywordFormMapper;
import org.sklsft.social.bc.mapper.nomenclature.views.basic.KeywordBasicViewMapper;
import org.sklsft.social.bc.mapper.nomenclature.views.full.KeywordFullViewMapper;
import org.sklsft.social.bc.processor.nomenclature.KeywordProcessor;
import org.sklsft.social.bc.rightsmanager.nomenclature.KeywordRightsManager;
import org.sklsft.social.bc.statemanager.nomenclature.KeywordStateManager;
import org.sklsft.social.model.nomenclature.Keyword;
import org.sklsft.social.repository.dao.interfaces.nomenclature.KeywordDao;
import org.springframework.transaction.annotation.Transactional;

/**
 * auto generated base service class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseServiceImpl implements KeywordBaseService {

/*
 * properties injected by spring
 */
@Inject
protected KeywordDao keywordDao;
@Inject
protected KeywordFullViewMapper keywordFullViewMapper;
@Inject
protected KeywordBasicViewMapper keywordBasicViewMapper;
@Inject
protected KeywordFormMapper keywordFormMapper;
@Inject
protected KeywordStateManager keywordStateManager;
@Inject
protected KeywordRightsManager keywordRightsManager;
@Inject
protected KeywordProcessor keywordProcessor;

/**
 * search options
 */
@Override
@Transactional(readOnly=true)
public List<SelectItem> searchOptions(String arg) {
List<Keyword> keywordList = keywordDao.search(arg);
List<SelectItem> result = new ArrayList<>(keywordList.size());
for (Keyword keyword : keywordList) {
result.add(new SelectItem(keyword.getCanonicalLabel(), keyword.getCanonicalLabel()));
}
return result;
}

/**
 * load object list
 */
@Override
@Transactional(readOnly=true)
public List<KeywordBasicView> loadList() {
keywordRightsManager.checkCanAccess();
List<Keyword> keywordList = keywordDao.loadListEagerly();
List<KeywordBasicView> result = new ArrayList<>(keywordList.size());
for (Keyword keyword : keywordList) {
result.add(this.keywordBasicViewMapper.mapFrom(new KeywordBasicView(),keyword));
}
return result;
}

/**
 * scroll object list
 */
@Override
@Transactional(readOnly=true)
public ScrollView<KeywordBasicView> scroll(ScrollForm<KeywordFilter, KeywordSorting> form) {
keywordRightsManager.checkCanAccess();
ScrollView<KeywordBasicView> result = new ScrollView<>();
result.setSize(keywordDao.count());
Long count = keywordDao.count(form.getFilter());
result.setNumberOfPages(count/form.getElementsPerPage() + ((count%form.getElementsPerPage()) > 0L?1L:0L));
result.setCurrentPage(Math.max(1L, Math.min(form.getPage()!=null?form.getPage():1L, result.getNumberOfPages())));
List<Keyword> list = keywordDao.scroll(form.getFilter(), form.getSorting(),(result.getCurrentPage()-1)*form.getElementsPerPage(), form.getElementsPerPage());
List<KeywordBasicView> elements = new ArrayList<>(list.size());
for (Keyword keyword : list) {
elements.add(this.keywordBasicViewMapper.mapFrom(new KeywordBasicView(),keyword));
}
result.setElements(elements);
return result;
}

/**
 * load object
 */
@Override
@Transactional(readOnly=true)
public KeywordFullView load(Long id) {
Keyword keyword = keywordDao.load(id);
keywordRightsManager.checkCanAccess(keyword);
return this.keywordFullViewMapper.mapFrom(new KeywordFullView(),keyword);
}

/**
 * find object
 */
@Override
@Transactional(readOnly=true)
public KeywordFullView find(String canonicalLabel) {
Keyword keyword = keywordDao.find(canonicalLabel);
keywordRightsManager.checkCanAccess(keyword);
return this.keywordFullViewMapper.mapFrom(new KeywordFullView(), keyword);
}

/**
 * create object
 */
@Override
public KeywordFullView create() {
keywordRightsManager.checkCanCreate();
return new KeywordFullView();
}

/**
 * save object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public Long save(KeywordForm keywordForm) {
Keyword keyword = this.keywordFormMapper.mapTo(keywordForm, new Keyword());
keywordRightsManager.checkCanSave(keyword);
keywordStateManager.checkCanSave(keyword);
return keywordProcessor.save(keyword);
}

/**
 * update object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void update(Long id, KeywordForm keywordForm) {
Keyword keyword = this.keywordDao.load(id);
keywordRightsManager.checkCanUpdate(keyword);
keywordStateManager.checkCanUpdate(keyword);
keyword = this.keywordFormMapper.mapTo(keywordForm, keyword);
keywordProcessor.update(keyword);
}

/**
 * delete object
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void delete(Long id) {
Keyword keyword = keywordDao.load(id);
keywordRightsManager.checkCanDelete(keyword);
keywordStateManager.checkCanDelete(keyword);
keywordProcessor.delete(keyword);
}

/**
 * delete object list
 */
@Override
@Transactional(rollbackFor=Exception.class)
public void deleteList(List<Long> idList) {
Keyword keyword;
if (idList != null){
for (Long id:idList){
keyword = keywordDao.load(id);
keywordRightsManager.checkCanDelete(keyword);
keywordStateManager.checkCanDelete(keyword);
keywordProcessor.delete(keyword);
}
}
}

}
