package org.sklsft.social.bl.impl.settings;

import org.sklsft.social.api.interfaces.settings.ParameterTypeService;
import org.sklsft.social.bl.impl.settings.base.ParameterTypeBaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * auto generated service class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
@Service("socialParameterTypeServiceImpl")
public class ParameterTypeServiceImpl extends ParameterTypeBaseServiceImpl implements ParameterTypeService{

/* Specific Code Start */
/* Specific Code End */
}
