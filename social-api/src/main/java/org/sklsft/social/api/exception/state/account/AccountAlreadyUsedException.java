package org.sklsft.social.api.exception.state.account;

import org.sklsft.commons.api.exception.ApplicationException;


/**
 * Exception thrown when creating an account that already exists
 */
public class AccountAlreadyUsedException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public AccountAlreadyUsedException(){
		super();
	}
	
	public AccountAlreadyUsedException(String message) {
		super(message);

	}

	public AccountAlreadyUsedException(String message, Throwable cause) {
		super(message, cause);
	}
}
