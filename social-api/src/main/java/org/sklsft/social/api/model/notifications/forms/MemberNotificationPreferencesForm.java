package org.sklsft.social.api.model.notifications.forms;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class MemberNotificationPreferencesForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String actionTypeCode;
@NotNull
private String mailType;

/*
 * getters and setters
 */
public String getActionTypeCode() {
return this.actionTypeCode;
}
public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}
public String getMailType() {
return this.mailType;
}
public void setMailType(String mailType) {
this.mailType = mailType;
}

/* Specific Code Start */
/* Specific Code End */
}
