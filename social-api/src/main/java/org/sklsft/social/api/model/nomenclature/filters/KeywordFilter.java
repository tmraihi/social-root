package org.sklsft.social.api.model.nomenclature.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class KeywordFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String canonicalLabel;

/*
 * getters and setters
 */
public String getCanonicalLabel() {
return this.canonicalLabel;
}
public void setCanonicalLabel(String canonicalLabel) {
this.canonicalLabel = canonicalLabel;
}

/* Specific Code Start */
/* Specific Code End */
}
