package org.sklsft.social.api.model.account.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String email;
@NotNull
private String firstName;
@NotNull
private String lastName;
@NotNull
private Date creationDate;
@NotNull
private Boolean activated;
@NotNull
private Boolean emailValidated;
@NotNull
private String userRole;

/*
 * getters and setters
 */
public String getEmail() {
return this.email;
}
public void setEmail(String email) {
this.email = email;
}
public String getFirstName() {
return this.firstName;
}
public void setFirstName(String firstName) {
this.firstName = firstName;
}
public String getLastName() {
return this.lastName;
}
public void setLastName(String lastName) {
this.lastName = lastName;
}
public Date getCreationDate() {
return this.creationDate;
}
public void setCreationDate(Date creationDate) {
this.creationDate = creationDate;
}
public Boolean getActivated() {
return this.activated;
}
public void setActivated(Boolean activated) {
this.activated = activated;
}
public Boolean getEmailValidated() {
return this.emailValidated;
}
public void setEmailValidated(Boolean emailValidated) {
this.emailValidated = emailValidated;
}
public String getUserRole() {
return this.userRole;
}
public void setUserRole(String userRole) {
this.userRole = userRole;
}

/* Specific Code Start */
/* Specific Code End */
}
