package org.sklsft.social.api.interfaces.nomenclature.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.nomenclature.filters.KeywordFilter;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.sklsft.social.api.model.nomenclature.sortings.KeywordSorting;
import org.sklsft.social.api.model.nomenclature.views.basic.KeywordBasicView;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface KeywordBaseService {

/**
 * search options
 */
List<SelectItem> searchOptions(String arg);
public static final String SEARCH_OPTIONS_URL = "/keyword/options/search";

/**
 * load object list
 */
List<KeywordBasicView> loadList();
public static final String GET_LIST_URL = "/keyword/list";

/**
 * scroll object list
 */
ScrollView<KeywordBasicView> scroll(ScrollForm<KeywordFilter, KeywordSorting> form);
public static final String SCROLL_URL = "/keyword/scroll";

/**
 * load object
 */
KeywordFullView load(Long id);
public static final String GET_URL = "/keyword/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/keyword/find";
KeywordFullView find(String canonicalLabel);

/**
 * create object
 */
KeywordFullView create();
public static final String GET_NEW_URL = "/keyword/new";

/**
 * save object
 */
Long save(KeywordForm keywordForm);
public static final String SAVE_URL = "/keyword";

/**
 * update object
 */
void update(Long id, KeywordForm keywordForm);
public static final String UPDATE_URL = "/keyword/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/keyword/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/keyword/delete";

}
