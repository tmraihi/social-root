package org.sklsft.social.api.model.nomenclature.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class KeywordSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType canonicalLabelOrderType;

/*
 * getters and setters
 */
public OrderType getCanonicalLabelOrderType() {
return this.canonicalLabelOrderType;
}

public void setCanonicalLabelOrderType(OrderType canonicalLabelOrderType) {
this.canonicalLabelOrderType = canonicalLabelOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
