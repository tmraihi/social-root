package org.sklsft.social.api.model.notifications.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String notifiedEmail;
@NotNull
private String actionTypeCode;
@NotNull
private Date notificationDate;
@NotNull
private String fromEmail;
private String fromOrganizationCode;
private String message;
@NotNull
private Boolean read;

/*
 * getters and setters
 */
public String getNotifiedEmail() {
return this.notifiedEmail;
}
public void setNotifiedEmail(String notifiedEmail) {
this.notifiedEmail = notifiedEmail;
}
public String getActionTypeCode() {
return this.actionTypeCode;
}
public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}
public Date getNotificationDate() {
return this.notificationDate;
}
public void setNotificationDate(Date notificationDate) {
this.notificationDate = notificationDate;
}
public String getFromEmail() {
return this.fromEmail;
}
public void setFromEmail(String fromEmail) {
this.fromEmail = fromEmail;
}
public String getFromOrganizationCode() {
return this.fromOrganizationCode;
}
public void setFromOrganizationCode(String fromOrganizationCode) {
this.fromOrganizationCode = fromOrganizationCode;
}
public String getMessage() {
return this.message;
}
public void setMessage(String message) {
this.message = message;
}
public Boolean getRead() {
return this.read;
}
public void setRead(Boolean read) {
this.read = read;
}

/* Specific Code Start */
/* Specific Code End */
}
