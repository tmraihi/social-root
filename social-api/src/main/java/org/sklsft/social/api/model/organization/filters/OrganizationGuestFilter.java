package org.sklsft.social.api.model.organization.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String organizationCode;
private String email;
private String userAccountEmail;
private String role;
private String position;

/*
 * getters and setters
 */
public String getOrganizationCode() {
return this.organizationCode;
}
public void setOrganizationCode(String organizationCode) {
this.organizationCode = organizationCode;
}
public String getEmail() {
return this.email;
}
public void setEmail(String email) {
this.email = email;
}
public String getUserAccountEmail() {
return this.userAccountEmail;
}
public void setUserAccountEmail(String userAccountEmail) {
this.userAccountEmail = userAccountEmail;
}
public String getRole() {
return this.role;
}
public void setRole(String role) {
this.role = role;
}
public String getPosition() {
return this.position;
}
public void setPosition(String position) {
this.position = position;
}

/* Specific Code Start */
/* Specific Code End */
}
