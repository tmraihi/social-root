package org.sklsft.social.api.interfaces.organization;

import org.sklsft.social.api.interfaces.organization.base.OrganizationMemberBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface OrganizationMemberService extends OrganizationMemberBaseService {

/* Specific Code Start */
/* Specific Code End */
}
