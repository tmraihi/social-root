package org.sklsft.social.api.interfaces.notifications;

import org.sklsft.social.api.interfaces.notifications.base.UserAccountNotificationBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface UserAccountNotificationService extends UserAccountNotificationBaseService {

/* Specific Code Start */
/* Specific Code End */
}
