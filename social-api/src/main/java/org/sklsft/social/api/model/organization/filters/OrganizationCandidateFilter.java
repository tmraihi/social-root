package org.sklsft.social.api.model.organization.filters;

import java.io.Serializable;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationCandidateFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String organizationCode;
private String userAccountEmail;

/*
 * getters and setters
 */
public String getOrganizationCode() {
return this.organizationCode;
}
public void setOrganizationCode(String organizationCode) {
this.organizationCode = organizationCode;
}
public String getUserAccountEmail() {
return this.userAccountEmail;
}
public void setUserAccountEmail(String userAccountEmail) {
this.userAccountEmail = userAccountEmail;
}

/* Specific Code Start */
/* Specific Code End */
}
