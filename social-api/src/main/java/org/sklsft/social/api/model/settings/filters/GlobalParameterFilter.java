package org.sklsft.social.api.model.settings.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String code;
private String description;
private String parameterTypeCode;
private String value;

/*
 * getters and setters
 */
public String getCode() {
return this.code;
}
public void setCode(String code) {
this.code = code;
}
public String getDescription() {
return this.description;
}
public void setDescription(String description) {
this.description = description;
}
public String getParameterTypeCode() {
return this.parameterTypeCode;
}
public void setParameterTypeCode(String parameterTypeCode) {
this.parameterTypeCode = parameterTypeCode;
}
public String getValue() {
return this.value;
}
public void setValue(String value) {
this.value = value;
}

/* Specific Code Start */
/* Specific Code End */
}
