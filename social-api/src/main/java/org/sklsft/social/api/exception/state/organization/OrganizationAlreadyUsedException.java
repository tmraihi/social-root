package org.sklsft.social.api.exception.state.organization;

import org.sklsft.commons.api.exception.ApplicationException;


public class OrganizationAlreadyUsedException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public OrganizationAlreadyUsedException(){
		super();
	}
	
	public OrganizationAlreadyUsedException(String message) {
		super(message);

	}

	public OrganizationAlreadyUsedException(String message, Throwable cause) {
		super(message, cause);
	}
}
