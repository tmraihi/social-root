package org.sklsft.social.api.interfaces.account.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.account.filters.UserAccountFilter;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.api.model.account.sortings.UserAccountSorting;
import org.sklsft.social.api.model.account.views.basic.UserAccountBasicView;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationPreferencesFilter;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationPreferencesForm;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationPreferencesSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationPreferencesBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationPreferencesFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface UserAccountBaseService {

/**
 * search options
 */
List<SelectItem> searchOptions(String arg);
public static final String SEARCH_OPTIONS_URL = "/user-account/options/search";

/**
 * load object list
 */
List<UserAccountBasicView> loadList();
public static final String GET_LIST_URL = "/user-account/list";

/**
 * scroll object list
 */
ScrollView<UserAccountBasicView> scroll(ScrollForm<UserAccountFilter, UserAccountSorting> form);
public static final String SCROLL_URL = "/user-account/scroll";

/**
 * load object
 */
UserAccountFullView load(Long id);
public static final String GET_URL = "/user-account/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/user-account/find";
UserAccountFullView find(String email);

/**
 * load one to many component userAccountNotificationPreferences list
 */
List<UserAccountNotificationPreferencesBasicView> loadUserAccountNotificationPreferencesList(Long id);
public static final String GET_USER_ACCOUNT_NOTIFICATION_PREFERENCES_LIST_URL = "/user-account/{id}/user-account-notification-preferences/list";

/**
 * scroll one to many component userAccountNotificationPreferences
 */
ScrollView<UserAccountNotificationPreferencesBasicView> scrollUserAccountNotificationPreferences (Long userAccountId, ScrollForm<UserAccountNotificationPreferencesFilter, UserAccountNotificationPreferencesSorting> form);
public static final String SCROLL_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account/{id}/user-account-notification-preferences/scroll";

/**
 * load one to many component userAccountNotificationPreferences
 */
UserAccountNotificationPreferencesFullView loadUserAccountNotificationPreferences(Long id);
public static final String GET_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account-notification-preferences/{id}";

/**
 * create object
 */
UserAccountFullView create();
public static final String GET_NEW_URL = "/user-account/new";

/**
 * create one to many component userAccountNotificationPreferences
 */
UserAccountNotificationPreferencesFullView createUserAccountNotificationPreferences(Long id);
public static final String GET_NEW_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account/{id}/user-account-notification-preferences/new";

/**
 * save object
 */
Long save(UserAccountForm userAccountForm);
public static final String SAVE_URL = "/user-account";

/**
 * save one to many component userAccountNotificationPreferences
 */
void saveUserAccountNotificationPreferences(Long id, UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm);
public static final String SAVE_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account/{id}/user-account-notification-preferences";

/**
 * update object
 */
void update(Long id, UserAccountForm userAccountForm);
public static final String UPDATE_URL = "/user-account/{id}";

/**
 * update one to many component userAccountNotificationPreferences
 */
void updateUserAccountNotificationPreferences(Long id, UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm);
public static final String UPDATE_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account-notification-preferences/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/user-account/{id}";

/**
 * delete one to many component userAccountNotificationPreferences
 */
void deleteUserAccountNotificationPreferences(Long id);
public static final String DELETE_USER_ACCOUNT_NOTIFICATION_PREFERENCES_URL = "/user-account-notification-preferences/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/user-account/delete";

/**
 * delete one to many component userAccountNotificationPreferences list
 */
void deleteUserAccountNotificationPreferencesList(List<Long> idList);
public static final String DELETE_USER_ACCOUNT_NOTIFICATION_PREFERENCES_LIST_URL = "/user-account-notification-preferences/delete";

}
