package org.sklsft.social.api.model.notifications.views.full;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.sklsft.commons.api.model.FullView;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationFullView extends FullView<Long, UserAccountNotificationForm> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public UserAccountNotificationFullView(){
this.form = new UserAccountNotificationForm();
}

/* Specific Code Start */
/* Specific Code End */
}
