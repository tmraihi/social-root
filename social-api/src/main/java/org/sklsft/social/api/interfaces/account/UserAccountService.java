package org.sklsft.social.api.interfaces.account;

import org.sklsft.social.api.interfaces.account.base.UserAccountBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface UserAccountService extends UserAccountBaseService {

/* Specific Code Start */
/* Specific Code End */
}
