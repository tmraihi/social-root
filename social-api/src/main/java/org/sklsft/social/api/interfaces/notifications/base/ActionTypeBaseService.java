package org.sklsft.social.api.interfaces.notifications.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.notifications.filters.ActionTypeFilter;
import org.sklsft.social.api.model.notifications.forms.ActionTypeForm;
import org.sklsft.social.api.model.notifications.sortings.ActionTypeSorting;
import org.sklsft.social.api.model.notifications.views.basic.ActionTypeBasicView;
import org.sklsft.social.api.model.notifications.views.full.ActionTypeFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface ActionTypeBaseService {

/**
 * get options
 */
List<SelectItem> getOptions();
public static final String GET_OPTIONS_URL = "/action-type/options";

/**
 * load object list
 */
List<ActionTypeBasicView> loadList();
public static final String GET_LIST_URL = "/action-type/list";

/**
 * scroll object list
 */
ScrollView<ActionTypeBasicView> scroll(ScrollForm<ActionTypeFilter, ActionTypeSorting> form);
public static final String SCROLL_URL = "/action-type/scroll";

/**
 * load object
 */
ActionTypeFullView load(Long id);
public static final String GET_URL = "/action-type/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/action-type/find";
ActionTypeFullView find(String code);

/**
 * create object
 */
ActionTypeFullView create();
public static final String GET_NEW_URL = "/action-type/new";

/**
 * save object
 */
Long save(ActionTypeForm actionTypeForm);
public static final String SAVE_URL = "/action-type";

/**
 * update object
 */
void update(Long id, ActionTypeForm actionTypeForm);
public static final String UPDATE_URL = "/action-type/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/action-type/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/action-type/delete";

}
