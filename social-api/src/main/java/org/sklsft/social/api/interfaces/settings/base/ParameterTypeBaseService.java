package org.sklsft.social.api.interfaces.settings.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.settings.filters.ParameterTypeFilter;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.sklsft.social.api.model.settings.sortings.ParameterTypeSorting;
import org.sklsft.social.api.model.settings.views.basic.ParameterTypeBasicView;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface ParameterTypeBaseService {

/**
 * get options
 */
List<SelectItem> getOptions();
public static final String GET_OPTIONS_URL = "/parameter-type/options";

/**
 * load object list
 */
List<ParameterTypeBasicView> loadList();
public static final String GET_LIST_URL = "/parameter-type/list";

/**
 * scroll object list
 */
ScrollView<ParameterTypeBasicView> scroll(ScrollForm<ParameterTypeFilter, ParameterTypeSorting> form);
public static final String SCROLL_URL = "/parameter-type/scroll";

/**
 * load object
 */
ParameterTypeFullView load(Long id);
public static final String GET_URL = "/parameter-type/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/parameter-type/find";
ParameterTypeFullView find(String code);

/**
 * create object
 */
ParameterTypeFullView create();
public static final String GET_NEW_URL = "/parameter-type/new";

/**
 * save object
 */
Long save(ParameterTypeForm parameterTypeForm);
public static final String SAVE_URL = "/parameter-type";

/**
 * update object
 */
void update(Long id, ParameterTypeForm parameterTypeForm);
public static final String UPDATE_URL = "/parameter-type/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/parameter-type/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/parameter-type/delete";

}
