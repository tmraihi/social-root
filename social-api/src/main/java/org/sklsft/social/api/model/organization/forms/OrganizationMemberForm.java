package org.sklsft.social.api.model.organization.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String organizationCode;
@NotNull
private String userAccountEmail;
@NotNull
private String role;
private String position;

/*
 * getters and setters
 */
public String getOrganizationCode() {
return this.organizationCode;
}
public void setOrganizationCode(String organizationCode) {
this.organizationCode = organizationCode;
}
public String getUserAccountEmail() {
return this.userAccountEmail;
}
public void setUserAccountEmail(String userAccountEmail) {
this.userAccountEmail = userAccountEmail;
}
public String getRole() {
return this.role;
}
public void setRole(String role) {
this.role = role;
}
public String getPosition() {
return this.position;
}
public void setPosition(String position) {
this.position = position;
}

/* Specific Code Start */
/* Specific Code End */
}
