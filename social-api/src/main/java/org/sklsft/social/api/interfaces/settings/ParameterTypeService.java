package org.sklsft.social.api.interfaces.settings;

import org.sklsft.social.api.interfaces.settings.base.ParameterTypeBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface ParameterTypeService extends ParameterTypeBaseService {

/* Specific Code Start */
/* Specific Code End */
}
