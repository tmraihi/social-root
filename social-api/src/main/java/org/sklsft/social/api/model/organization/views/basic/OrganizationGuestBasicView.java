package org.sklsft.social.api.model.organization.views.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBasicView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private Long id;
private boolean selected;
private boolean canDelete;
private String organizationCode;
private String email;
private String userAccountEmail;
private String role;
private String position;

/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public boolean getSelected() {
return this.selected;
}

public void setSelected(boolean selected) {
this.selected = selected;
}

public boolean getCanDelete() {
return this.canDelete;
}

public void setCanDelete(boolean canDelete) {
this.canDelete = canDelete;
}

public String getOrganizationCode() {
return this.organizationCode;
}

public void setOrganizationCode(String organizationCode) {
this.organizationCode = organizationCode;
}

public String getEmail() {
return this.email;
}

public void setEmail(String email) {
this.email = email;
}

public String getUserAccountEmail() {
return this.userAccountEmail;
}

public void setUserAccountEmail(String userAccountEmail) {
this.userAccountEmail = userAccountEmail;
}

public String getRole() {
return this.role;
}

public void setRole(String role) {
this.role = role;
}

public String getPosition() {
return this.position;
}

public void setPosition(String position) {
this.position = position;
}


/* Specific Code Start */
/* Specific Code End */
}
