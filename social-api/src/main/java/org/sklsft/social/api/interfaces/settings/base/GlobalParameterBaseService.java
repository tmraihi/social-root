package org.sklsft.social.api.interfaces.settings.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.settings.filters.GlobalParameterFilter;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.sklsft.social.api.model.settings.sortings.GlobalParameterSorting;
import org.sklsft.social.api.model.settings.views.basic.GlobalParameterBasicView;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface GlobalParameterBaseService {

/**
 * get options
 */
List<SelectItem> getOptions();
public static final String GET_OPTIONS_URL = "/global-parameter/options";

/**
 * load object list
 */
List<GlobalParameterBasicView> loadList();
public static final String GET_LIST_URL = "/global-parameter/list";

/**
 * scroll object list
 */
ScrollView<GlobalParameterBasicView> scroll(ScrollForm<GlobalParameterFilter, GlobalParameterSorting> form);
public static final String SCROLL_URL = "/global-parameter/scroll";

/**
 * load object
 */
GlobalParameterFullView load(Long id);
public static final String GET_URL = "/global-parameter/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/global-parameter/find";
GlobalParameterFullView find(String code);

/**
 * create object
 */
GlobalParameterFullView create();
public static final String GET_NEW_URL = "/global-parameter/new";

/**
 * save object
 */
Long save(GlobalParameterForm globalParameterForm);
public static final String SAVE_URL = "/global-parameter";

/**
 * update object
 */
void update(Long id, GlobalParameterForm globalParameterForm);
public static final String UPDATE_URL = "/global-parameter/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/global-parameter/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/global-parameter/delete";

}
