package org.sklsft.social.api.model.notifications.views.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBasicView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private Long id;
private boolean selected;
private boolean canDelete;
private String code;
private String notificationLabel;
private String mailLabel;

/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public boolean getSelected() {
return this.selected;
}

public void setSelected(boolean selected) {
this.selected = selected;
}

public boolean getCanDelete() {
return this.canDelete;
}

public void setCanDelete(boolean canDelete) {
this.canDelete = canDelete;
}

public String getCode() {
return this.code;
}

public void setCode(String code) {
this.code = code;
}

public String getNotificationLabel() {
return this.notificationLabel;
}

public void setNotificationLabel(String notificationLabel) {
this.notificationLabel = notificationLabel;
}

public String getMailLabel() {
return this.mailLabel;
}

public void setMailLabel(String mailLabel) {
this.mailLabel = mailLabel;
}


/* Specific Code Start */
/* Specific Code End */
}
