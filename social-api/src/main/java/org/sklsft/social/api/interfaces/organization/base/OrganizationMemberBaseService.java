package org.sklsft.social.api.interfaces.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.organization.filters.OrganizationMemberFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationMemberSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationMemberBaseService {

/**
 * load object list
 */
List<OrganizationMemberBasicView> loadList();
public static final String GET_LIST_URL = "/organization-member/list";

/**
 * load object list from organization
 */
List<OrganizationMemberBasicView> loadListFromOrganization (Long organizationId);
public static final String GET_ORGANIZATION_MEMBER_LIST_FROM_ORGANIZATION_URL = "/organization/{organizationId}/organization-member/list";

/**
 * load object list from userAccount
 */
List<OrganizationMemberBasicView> loadListFromUserAccount (Long userAccountId);
public static final String GET_ORGANIZATION_MEMBER_LIST_FROM_USER_ACCOUNT_URL = "/user-account/{userAccountId}/organization-member/list";

/**
 * scroll object list
 */
ScrollView<OrganizationMemberBasicView> scroll(ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form);
public static final String SCROLL_URL = "/organization-member/scroll";

/**
 * scroll object list from organization
 */
ScrollView<OrganizationMemberBasicView> scrollFromOrganization (Long organizationId, ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form);
public static final String SCROLL_ORGANIZATION_MEMBER_FROM_ORGANIZATION_URL = "/organization/{organizationId}/organization-member/scroll";

/**
 * scroll object list from userAccount
 */
ScrollView<OrganizationMemberBasicView> scrollFromUserAccount (Long userAccountId, ScrollForm<OrganizationMemberFilter, OrganizationMemberSorting> form);
public static final String SCROLL_ORGANIZATION_MEMBER_FROM_USER_ACCOUNT_URL = "/user-account/{userAccountId}/organization-member/scroll";

/**
 * load object
 */
OrganizationMemberFullView load(Long id);
public static final String GET_URL = "/organization-member/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/organization-member/find";
OrganizationMemberFullView find(String organizationCode, String userAccountEmail);

/**
 * create object
 */
OrganizationMemberFullView create();
public static final String GET_NEW_URL = "/organization-member/new";

/**
 * save object
 */
Long save(OrganizationMemberForm organizationMemberForm);
public static final String SAVE_URL = "/organization-member";

/**
 * update object
 */
void update(Long id, OrganizationMemberForm organizationMemberForm);
public static final String UPDATE_URL = "/organization-member/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/organization-member/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/organization-member/delete";

}
