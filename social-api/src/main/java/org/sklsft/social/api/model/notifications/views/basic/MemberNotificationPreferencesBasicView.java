package org.sklsft.social.api.model.notifications.views.basic;

import java.io.Serializable;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class MemberNotificationPreferencesBasicView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private Long id;
private boolean selected;
private boolean canDelete;
private String actionTypeCode;
private String mailType;

/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public boolean getSelected() {
return this.selected;
}

public void setSelected(boolean selected) {
this.selected = selected;
}

public boolean getCanDelete() {
return this.canDelete;
}

public void setCanDelete(boolean canDelete) {
this.canDelete = canDelete;
}

public String getActionTypeCode() {
return this.actionTypeCode;
}

public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}

public String getMailType() {
return this.mailType;
}

public void setMailType(String mailType) {
this.mailType = mailType;
}


/* Specific Code Start */
/* Specific Code End */
}
