package org.sklsft.social.api.interfaces.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.organization.filters.OrganizationFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationBaseService {

/**
 * search options
 */
List<SelectItem> searchOptions(String arg);
public static final String SEARCH_OPTIONS_URL = "/organization/options/search";

/**
 * load object list
 */
List<OrganizationBasicView> loadList();
public static final String GET_LIST_URL = "/organization/list";

/**
 * scroll object list
 */
ScrollView<OrganizationBasicView> scroll(ScrollForm<OrganizationFilter, OrganizationSorting> form);
public static final String SCROLL_URL = "/organization/scroll";

/**
 * load object
 */
OrganizationFullView load(Long id);
public static final String GET_URL = "/organization/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/organization/find";
OrganizationFullView find(String code);

/**
 * create object
 */
OrganizationFullView create();
public static final String GET_NEW_URL = "/organization/new";

/**
 * save object
 */
Long save(OrganizationForm organizationForm);
public static final String SAVE_URL = "/organization";

/**
 * update object
 */
void update(Long id, OrganizationForm organizationForm);
public static final String UPDATE_URL = "/organization/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/organization/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/organization/delete";

}
