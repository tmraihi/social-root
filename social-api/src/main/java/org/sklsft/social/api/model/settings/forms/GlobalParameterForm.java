package org.sklsft.social.api.model.settings.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String code;
@NotNull
private String description;
@NotNull
private String parameterTypeCode;
@NotNull
private String value;

/*
 * getters and setters
 */
public String getCode() {
return this.code;
}
public void setCode(String code) {
this.code = code;
}
public String getDescription() {
return this.description;
}
public void setDescription(String description) {
this.description = description;
}
public String getParameterTypeCode() {
return this.parameterTypeCode;
}
public void setParameterTypeCode(String parameterTypeCode) {
this.parameterTypeCode = parameterTypeCode;
}
public String getValue() {
return this.value;
}
public void setValue(String value) {
this.value = value;
}

/* Specific Code Start */
/* Specific Code End */
}
