package org.sklsft.social.api.model.notifications.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class ActionTypeFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String code;
private String notificationLabel;
private String mailLabel;

/*
 * getters and setters
 */
public String getCode() {
return this.code;
}
public void setCode(String code) {
this.code = code;
}
public String getNotificationLabel() {
return this.notificationLabel;
}
public void setNotificationLabel(String notificationLabel) {
this.notificationLabel = notificationLabel;
}
public String getMailLabel() {
return this.mailLabel;
}
public void setMailLabel(String mailLabel) {
this.mailLabel = mailLabel;
}

/* Specific Code Start */
/* Specific Code End */
}
