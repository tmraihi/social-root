package org.sklsft.social.api.model.organization.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType organizationCodeOrderType;
private OrderType emailOrderType;
private OrderType userAccountEmailOrderType;
private OrderType roleOrderType;
private OrderType positionOrderType;

/*
 * getters and setters
 */
public OrderType getOrganizationCodeOrderType() {
return this.organizationCodeOrderType;
}

public void setOrganizationCodeOrderType(OrderType organizationCodeOrderType) {
this.organizationCodeOrderType = organizationCodeOrderType;
}

public OrderType getEmailOrderType() {
return this.emailOrderType;
}

public void setEmailOrderType(OrderType emailOrderType) {
this.emailOrderType = emailOrderType;
}

public OrderType getUserAccountEmailOrderType() {
return this.userAccountEmailOrderType;
}

public void setUserAccountEmailOrderType(OrderType userAccountEmailOrderType) {
this.userAccountEmailOrderType = userAccountEmailOrderType;
}

public OrderType getRoleOrderType() {
return this.roleOrderType;
}

public void setRoleOrderType(OrderType roleOrderType) {
this.roleOrderType = roleOrderType;
}

public OrderType getPositionOrderType() {
return this.positionOrderType;
}

public void setPositionOrderType(OrderType positionOrderType) {
this.positionOrderType = positionOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
