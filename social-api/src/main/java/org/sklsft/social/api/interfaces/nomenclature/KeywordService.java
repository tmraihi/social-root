package org.sklsft.social.api.interfaces.nomenclature;

import org.sklsft.social.api.interfaces.nomenclature.base.KeywordBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface KeywordService extends KeywordBaseService {

/* Specific Code Start */
/* Specific Code End */
}
