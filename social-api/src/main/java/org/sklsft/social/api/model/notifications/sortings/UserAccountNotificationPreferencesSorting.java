package org.sklsft.social.api.model.notifications.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationPreferencesSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType actionTypeCodeOrderType;
private OrderType mailTypeOrderType;

/*
 * getters and setters
 */
public OrderType getActionTypeCodeOrderType() {
return this.actionTypeCodeOrderType;
}

public void setActionTypeCodeOrderType(OrderType actionTypeCodeOrderType) {
this.actionTypeCodeOrderType = actionTypeCodeOrderType;
}

public OrderType getMailTypeOrderType() {
return this.mailTypeOrderType;
}

public void setMailTypeOrderType(OrderType mailTypeOrderType) {
this.mailTypeOrderType = mailTypeOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
