package org.sklsft.social.api.model.account.views.full;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;
import org.sklsft.commons.api.model.FullView;
import org.sklsft.social.api.model.account.forms.UserAccountForm;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountFullView extends FullView<Long, UserAccountForm> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public UserAccountFullView(){
this.form = new UserAccountForm();
}

/* Specific Code Start */
/* Specific Code End */
}
