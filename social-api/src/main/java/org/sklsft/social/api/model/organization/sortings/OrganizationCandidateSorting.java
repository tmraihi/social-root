package org.sklsft.social.api.model.organization.sortings;

import java.io.Serializable;

import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationCandidateSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType organizationCodeOrderType;
private OrderType userAccountEmailOrderType;

/*
 * getters and setters
 */
public OrderType getOrganizationCodeOrderType() {
return this.organizationCodeOrderType;
}

public void setOrganizationCodeOrderType(OrderType organizationCodeOrderType) {
this.organizationCodeOrderType = organizationCodeOrderType;
}

public OrderType getUserAccountEmailOrderType() {
return this.userAccountEmailOrderType;
}

public void setUserAccountEmailOrderType(OrderType userAccountEmailOrderType) {
this.userAccountEmailOrderType = userAccountEmailOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
