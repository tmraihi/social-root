package org.sklsft.social.api.interfaces.notifications.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.notifications.filters.UserAccountNotificationFilter;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.sklsft.social.api.model.notifications.sortings.UserAccountNotificationSorting;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationBasicView;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface UserAccountNotificationBaseService {

/**
 * load object list
 */
List<UserAccountNotificationBasicView> loadList();
public static final String GET_LIST_URL = "/user-account-notification/list";

/**
 * scroll object list
 */
ScrollView<UserAccountNotificationBasicView> scroll(ScrollForm<UserAccountNotificationFilter, UserAccountNotificationSorting> form);
public static final String SCROLL_URL = "/user-account-notification/scroll";

/**
 * load object
 */
UserAccountNotificationFullView load(Long id);
public static final String GET_URL = "/user-account-notification/{id}";

/**
 * create object
 */
UserAccountNotificationFullView create();
public static final String GET_NEW_URL = "/user-account-notification/new";

/**
 * save object
 */
Long save(UserAccountNotificationForm userAccountNotificationForm);
public static final String SAVE_URL = "/user-account-notification";

/**
 * update object
 */
void update(Long id, UserAccountNotificationForm userAccountNotificationForm);
public static final String UPDATE_URL = "/user-account-notification/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/user-account-notification/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/user-account-notification/delete";

}
