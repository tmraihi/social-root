package org.sklsft.social.api.exception.state.organization;

import org.sklsft.commons.api.exception.ApplicationException;


public class ContactNotFoundException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public ContactNotFoundException() {
		super();
	}
	
	public ContactNotFoundException(String message) {
		super(message);
	}
	
	public ContactNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
