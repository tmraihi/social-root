package org.sklsft.social.api.model.notifications.views.full;

import org.sklsft.commons.api.model.FullView;
import org.sklsft.social.api.model.notifications.forms.MemberNotificationPreferencesForm;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class MemberNotificationPreferencesFullView extends FullView<Long, MemberNotificationPreferencesForm> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public MemberNotificationPreferencesFullView(){
this.form = new MemberNotificationPreferencesForm();
}

/* Specific Code Start */
/* Specific Code End */
}
