package org.sklsft.social.api.model.account.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType emailOrderType;
private OrderType firstNameOrderType;
private OrderType lastNameOrderType;
private OrderType creationDateOrderType;
private OrderType activatedOrderType;
private OrderType emailValidatedOrderType;
private OrderType userRoleOrderType;

/*
 * getters and setters
 */
public OrderType getEmailOrderType() {
return this.emailOrderType;
}

public void setEmailOrderType(OrderType emailOrderType) {
this.emailOrderType = emailOrderType;
}

public OrderType getFirstNameOrderType() {
return this.firstNameOrderType;
}

public void setFirstNameOrderType(OrderType firstNameOrderType) {
this.firstNameOrderType = firstNameOrderType;
}

public OrderType getLastNameOrderType() {
return this.lastNameOrderType;
}

public void setLastNameOrderType(OrderType lastNameOrderType) {
this.lastNameOrderType = lastNameOrderType;
}

public OrderType getCreationDateOrderType() {
return this.creationDateOrderType;
}

public void setCreationDateOrderType(OrderType creationDateOrderType) {
this.creationDateOrderType = creationDateOrderType;
}

public OrderType getActivatedOrderType() {
return this.activatedOrderType;
}

public void setActivatedOrderType(OrderType activatedOrderType) {
this.activatedOrderType = activatedOrderType;
}

public OrderType getEmailValidatedOrderType() {
return this.emailValidatedOrderType;
}

public void setEmailValidatedOrderType(OrderType emailValidatedOrderType) {
this.emailValidatedOrderType = emailValidatedOrderType;
}

public OrderType getUserRoleOrderType() {
return this.userRoleOrderType;
}

public void setUserRoleOrderType(OrderType userRoleOrderType) {
this.userRoleOrderType = userRoleOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
