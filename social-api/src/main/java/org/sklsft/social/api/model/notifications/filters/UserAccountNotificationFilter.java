package org.sklsft.social.api.model.notifications.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String notifiedEmail;
private String actionTypeCode;
private Date notificationDateMinValue;
private Date notificationDateMaxValue;
private String message;
private Boolean read;

/*
 * getters and setters
 */
public String getNotifiedEmail() {
return this.notifiedEmail;
}
public void setNotifiedEmail(String notifiedEmail) {
this.notifiedEmail = notifiedEmail;
}
public String getActionTypeCode() {
return this.actionTypeCode;
}
public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}
public Date getNotificationDateMinValue() {
return this.notificationDateMinValue;
}
public void setNotificationDateMinValue(Date notificationDateMinValue) {
this.notificationDateMinValue = notificationDateMinValue;
}
public Date getNotificationDateMaxValue() {
return this.notificationDateMaxValue;
}
public void setNotificationDateMaxValue(Date notificationDateMaxValue) {
this.notificationDateMaxValue = notificationDateMaxValue;
}
public String getMessage() {
return this.message;
}
public void setMessage(String message) {
this.message = message;
}
public Boolean getRead() {
return this.read;
}
public void setRead(Boolean read) {
this.read = read;
}

/* Specific Code Start */
/* Specific Code End */
}
