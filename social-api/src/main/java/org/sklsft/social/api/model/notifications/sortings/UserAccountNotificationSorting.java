package org.sklsft.social.api.model.notifications.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType notifiedEmailOrderType;
private OrderType actionTypeCodeOrderType;
private OrderType notificationDateOrderType;
private OrderType messageOrderType;
private OrderType readOrderType;

/*
 * getters and setters
 */
public OrderType getNotifiedEmailOrderType() {
return this.notifiedEmailOrderType;
}

public void setNotifiedEmailOrderType(OrderType notifiedEmailOrderType) {
this.notifiedEmailOrderType = notifiedEmailOrderType;
}

public OrderType getActionTypeCodeOrderType() {
return this.actionTypeCodeOrderType;
}

public void setActionTypeCodeOrderType(OrderType actionTypeCodeOrderType) {
this.actionTypeCodeOrderType = actionTypeCodeOrderType;
}

public OrderType getNotificationDateOrderType() {
return this.notificationDateOrderType;
}

public void setNotificationDateOrderType(OrderType notificationDateOrderType) {
this.notificationDateOrderType = notificationDateOrderType;
}

public OrderType getMessageOrderType() {
return this.messageOrderType;
}

public void setMessageOrderType(OrderType messageOrderType) {
this.messageOrderType = messageOrderType;
}

public OrderType getReadOrderType() {
return this.readOrderType;
}

public void setReadOrderType(OrderType readOrderType) {
this.readOrderType = readOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
