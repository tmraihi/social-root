package org.sklsft.social.api.model.account.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String email;
private String firstName;
private String lastName;
private Date creationDateMinValue;
private Date creationDateMaxValue;
private Boolean activated;
private Boolean emailValidated;
private String userRole;

/*
 * getters and setters
 */
public String getEmail() {
return this.email;
}
public void setEmail(String email) {
this.email = email;
}
public String getFirstName() {
return this.firstName;
}
public void setFirstName(String firstName) {
this.firstName = firstName;
}
public String getLastName() {
return this.lastName;
}
public void setLastName(String lastName) {
this.lastName = lastName;
}
public Date getCreationDateMinValue() {
return this.creationDateMinValue;
}
public void setCreationDateMinValue(Date creationDateMinValue) {
this.creationDateMinValue = creationDateMinValue;
}
public Date getCreationDateMaxValue() {
return this.creationDateMaxValue;
}
public void setCreationDateMaxValue(Date creationDateMaxValue) {
this.creationDateMaxValue = creationDateMaxValue;
}
public Boolean getActivated() {
return this.activated;
}
public void setActivated(Boolean activated) {
this.activated = activated;
}
public Boolean getEmailValidated() {
return this.emailValidated;
}
public void setEmailValidated(Boolean emailValidated) {
this.emailValidated = emailValidated;
}
public String getUserRole() {
return this.userRole;
}
public void setUserRole(String userRole) {
this.userRole = userRole;
}

/* Specific Code Start */
/* Specific Code End */
}
