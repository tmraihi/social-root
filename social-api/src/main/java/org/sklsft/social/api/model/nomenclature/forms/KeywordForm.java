package org.sklsft.social.api.model.nomenclature.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class KeywordForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String canonicalLabel;

/*
 * getters and setters
 */
public String getCanonicalLabel() {
return this.canonicalLabel;
}
public void setCanonicalLabel(String canonicalLabel) {
this.canonicalLabel = canonicalLabel;
}

/* Specific Code Start */
/* Specific Code End */
}
