package org.sklsft.social.api.model.organization.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class OrganizationSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType codeOrderType;
private OrderType nameOrderType;
private OrderType descriptionOrderType;

/*
 * getters and setters
 */
public OrderType getCodeOrderType() {
return this.codeOrderType;
}

public void setCodeOrderType(OrderType codeOrderType) {
this.codeOrderType = codeOrderType;
}

public OrderType getNameOrderType() {
return this.nameOrderType;
}

public void setNameOrderType(OrderType nameOrderType) {
this.nameOrderType = nameOrderType;
}

public OrderType getDescriptionOrderType() {
return this.descriptionOrderType;
}

public void setDescriptionOrderType(OrderType descriptionOrderType) {
this.descriptionOrderType = descriptionOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
