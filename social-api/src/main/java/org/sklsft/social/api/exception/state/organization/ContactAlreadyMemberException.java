package org.sklsft.social.api.exception.state.organization;

import org.sklsft.commons.api.exception.ApplicationException;


public class ContactAlreadyMemberException extends ApplicationException {

	private static final long serialVersionUID = 7276284138167184144L;
	
	
	public ContactAlreadyMemberException(){
		super();
	}
	
	public ContactAlreadyMemberException(String message) {
		super(message);

	}

	public ContactAlreadyMemberException(String message, Throwable cause) {
		super(message, cause);
	}
}
