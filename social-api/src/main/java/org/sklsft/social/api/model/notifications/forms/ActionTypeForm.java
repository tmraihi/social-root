package org.sklsft.social.api.model.notifications.forms;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 * auto generated form bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class ActionTypeForm implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
@NotNull
private String code;
@NotNull
private String notificationLabel;
@NotNull
private String mailLabel;

/*
 * getters and setters
 */
public String getCode() {
return this.code;
}
public void setCode(String code) {
this.code = code;
}
public String getNotificationLabel() {
return this.notificationLabel;
}
public void setNotificationLabel(String notificationLabel) {
this.notificationLabel = notificationLabel;
}
public String getMailLabel() {
return this.mailLabel;
}
public void setMailLabel(String mailLabel) {
this.mailLabel = mailLabel;
}

/* Specific Code Start */
/* Specific Code End */
}
