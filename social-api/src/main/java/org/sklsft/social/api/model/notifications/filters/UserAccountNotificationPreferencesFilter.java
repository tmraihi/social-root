package org.sklsft.social.api.model.notifications.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationPreferencesFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String actionTypeCode;
private String mailType;

/*
 * getters and setters
 */
public String getActionTypeCode() {
return this.actionTypeCode;
}
public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}
public String getMailType() {
return this.mailType;
}
public void setMailType(String mailType) {
this.mailType = mailType;
}

/* Specific Code Start */
/* Specific Code End */
}
