package org.sklsft.social.api.model.settings.filters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated filter class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeFilter implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private String code;
private String description;

/*
 * getters and setters
 */
public String getCode() {
return this.code;
}
public void setCode(String code) {
this.code = code;
}
public String getDescription() {
return this.description;
}
public void setDescription(String description) {
this.description = description;
}

/* Specific Code Start */
/* Specific Code End */
}
