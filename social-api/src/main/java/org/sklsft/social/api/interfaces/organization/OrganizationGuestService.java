package org.sklsft.social.api.interfaces.organization;

import org.sklsft.social.api.interfaces.organization.base.OrganizationGuestBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface OrganizationGuestService extends OrganizationGuestBaseService {

/* Specific Code Start */
/* Specific Code End */
}
