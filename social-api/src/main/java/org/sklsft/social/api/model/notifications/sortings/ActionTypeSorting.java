package org.sklsft.social.api.model.notifications.sortings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.sklsft.commons.api.model.OrderType;

/**
 * auto generated ordering class file
 * <br/>basic representation of filter used along with jsf datatable
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class ActionTypeSorting implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private OrderType codeOrderType;
private OrderType notificationLabelOrderType;
private OrderType mailLabelOrderType;

/*
 * getters and setters
 */
public OrderType getCodeOrderType() {
return this.codeOrderType;
}

public void setCodeOrderType(OrderType codeOrderType) {
this.codeOrderType = codeOrderType;
}

public OrderType getNotificationLabelOrderType() {
return this.notificationLabelOrderType;
}

public void setNotificationLabelOrderType(OrderType notificationLabelOrderType) {
this.notificationLabelOrderType = notificationLabelOrderType;
}

public OrderType getMailLabelOrderType() {
return this.mailLabelOrderType;
}

public void setMailLabelOrderType(OrderType mailLabelOrderType) {
this.mailLabelOrderType = mailLabelOrderType;
}


/* Specific Code Start */
/* Specific Code End */
}
