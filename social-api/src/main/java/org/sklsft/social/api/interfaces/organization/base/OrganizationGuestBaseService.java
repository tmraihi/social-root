package org.sklsft.social.api.interfaces.organization.base;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.sklsft.commons.api.model.ScrollForm;
import org.sklsft.commons.api.model.ScrollView;
import org.sklsft.commons.api.model.SelectItem;
import org.sklsft.social.api.model.organization.filters.OrganizationGuestFilter;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.api.model.organization.sortings.OrganizationGuestSorting;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;

/**
 * auto generated base service interface file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public interface OrganizationGuestBaseService {

/**
 * load object list
 */
List<OrganizationGuestBasicView> loadList();
public static final String GET_LIST_URL = "/organization-guest/list";

/**
 * load object list from organization
 */
List<OrganizationGuestBasicView> loadListFromOrganization (Long organizationId);
public static final String GET_ORGANIZATION_GUEST_LIST_FROM_ORGANIZATION_URL = "/organization/{organizationId}/organization-guest/list";

/**
 * load object list from userAccount
 */
List<OrganizationGuestBasicView> loadListFromUserAccount (Long userAccountId);
public static final String GET_ORGANIZATION_GUEST_LIST_FROM_USER_ACCOUNT_URL = "/user-account/{userAccountId}/organization-guest/list";

/**
 * scroll object list
 */
ScrollView<OrganizationGuestBasicView> scroll(ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form);
public static final String SCROLL_URL = "/organization-guest/scroll";

/**
 * scroll object list from organization
 */
ScrollView<OrganizationGuestBasicView> scrollFromOrganization (Long organizationId, ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form);
public static final String SCROLL_ORGANIZATION_GUEST_FROM_ORGANIZATION_URL = "/organization/{organizationId}/organization-guest/scroll";

/**
 * scroll object list from userAccount
 */
ScrollView<OrganizationGuestBasicView> scrollFromUserAccount (Long userAccountId, ScrollForm<OrganizationGuestFilter, OrganizationGuestSorting> form);
public static final String SCROLL_ORGANIZATION_GUEST_FROM_USER_ACCOUNT_URL = "/user-account/{userAccountId}/organization-guest/scroll";

/**
 * load object
 */
OrganizationGuestFullView load(Long id);
public static final String GET_URL = "/organization-guest/{id}";

/**
 * find object
 */
public static final String FIND_URL = "/organization-guest/find";
OrganizationGuestFullView find(String organizationCode, String email);

/**
 * create object
 */
OrganizationGuestFullView create();
public static final String GET_NEW_URL = "/organization-guest/new";

/**
 * save object
 */
Long save(OrganizationGuestForm organizationGuestForm);
public static final String SAVE_URL = "/organization-guest";

/**
 * update object
 */
void update(Long id, OrganizationGuestForm organizationGuestForm);
public static final String UPDATE_URL = "/organization-guest/{id}";

/**
 * delete object
 */
void delete(Long id);
public static final String DELETE_URL = "/organization-guest/{id}";

/**
 * delete object list
 */
void deleteList(List<Long> idList);
public static final String DELETE_LIST_URL = "/organization-guest/delete";

}
