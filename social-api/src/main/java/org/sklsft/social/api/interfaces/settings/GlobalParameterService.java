package org.sklsft.social.api.interfaces.settings;

import org.sklsft.social.api.interfaces.settings.base.GlobalParameterBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface GlobalParameterService extends GlobalParameterBaseService {

/* Specific Code Start */
/* Specific Code End */
}
