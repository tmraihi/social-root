package org.sklsft.social.api.interfaces.organization;

import org.sklsft.social.api.interfaces.organization.base.OrganizationBaseService;

/**
 * auto generated service interface file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public interface OrganizationService extends OrganizationBaseService {

/* Specific Code Start */
/* Specific Code End */
}
