package org.sklsft.social.api.model.notifications.views.basic;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * auto generated view bean class file
 * <br/>basic representation of what is going to be considered as model in MVC patterns
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBasicView implements Serializable {

private static final long serialVersionUID = 1L;

/*
 * properties
 */
private Long id;
private boolean selected;
private boolean canDelete;
private String notifiedEmail;
private String actionTypeCode;
private Date notificationDate;
private String message;
private Boolean read;

/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public boolean getSelected() {
return this.selected;
}

public void setSelected(boolean selected) {
this.selected = selected;
}

public boolean getCanDelete() {
return this.canDelete;
}

public void setCanDelete(boolean canDelete) {
this.canDelete = canDelete;
}

public String getNotifiedEmail() {
return this.notifiedEmail;
}

public void setNotifiedEmail(String notifiedEmail) {
this.notifiedEmail = notifiedEmail;
}

public String getActionTypeCode() {
return this.actionTypeCode;
}

public void setActionTypeCode(String actionTypeCode) {
this.actionTypeCode = actionTypeCode;
}

public Date getNotificationDate() {
return this.notificationDate;
}

public void setNotificationDate(Date notificationDate) {
this.notificationDate = notificationDate;
}

public String getMessage() {
return this.message;
}

public void setMessage(String message) {
this.message = message;
}

public Boolean getRead() {
return this.read;
}

public void setRead(Boolean read) {
this.read = read;
}


/* Specific Code Start */
/* Specific Code End */
}
