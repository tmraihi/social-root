package org.sklsft.social.populator.command.nomenclature;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.nomenclature.KeywordService;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class KeywordCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(KeywordService.class);

@Inject
private KeywordService keywordService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<KeywordForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<KeywordForm>(KeywordForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<KeywordForm>(KeywordForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute keywordService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
KeywordForm keywordForm = mapper.mapFrom(new KeywordForm(), args);

this.keywordService.save(keywordForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
