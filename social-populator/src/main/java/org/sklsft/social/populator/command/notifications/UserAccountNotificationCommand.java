package org.sklsft.social.populator.command.notifications;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.notifications.UserAccountNotificationService;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class UserAccountNotificationCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(UserAccountNotificationService.class);

@Inject
private UserAccountNotificationService userAccountNotificationService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<UserAccountNotificationForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<UserAccountNotificationForm>(UserAccountNotificationForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<UserAccountNotificationForm>(UserAccountNotificationForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute userAccountNotificationService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
UserAccountNotificationForm userAccountNotificationForm = mapper.mapFrom(new UserAccountNotificationForm(), args);

this.userAccountNotificationService.save(userAccountNotificationForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
