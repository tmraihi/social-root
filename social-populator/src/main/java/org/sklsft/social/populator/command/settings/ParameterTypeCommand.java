package org.sklsft.social.populator.command.settings;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.settings.ParameterTypeService;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class ParameterTypeCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(ParameterTypeService.class);

@Inject
private ParameterTypeService parameterTypeService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<ParameterTypeForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<ParameterTypeForm>(ParameterTypeForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<ParameterTypeForm>(ParameterTypeForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute parameterTypeService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
ParameterTypeForm parameterTypeForm = mapper.mapFrom(new ParameterTypeForm(), args);

this.parameterTypeService.save(parameterTypeForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
