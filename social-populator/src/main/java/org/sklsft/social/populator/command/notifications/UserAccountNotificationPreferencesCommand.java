package org.sklsft.social.populator.command.notifications;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringToObjectConverter;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationPreferencesForm;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationPreferencesFullView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated view command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class UserAccountNotificationPreferencesCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(UserAccountService.class);

@Inject
private UserAccountService userAccountService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<UserAccountNotificationPreferencesForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<UserAccountNotificationPreferencesForm>(UserAccountNotificationPreferencesForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<UserAccountNotificationPreferencesForm>(UserAccountNotificationPreferencesForm.class);
}
for (Object[] args:arguments.getArguments()) {
String message = "execute userAccountService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm = mapper.mapFrom(new UserAccountNotificationPreferencesForm(), Arrays.copyOfRange(args,1,args.length));

String arg0 = arguments.isArgumentsTyped()?(String)args[0]:(String)(StringToObjectConverter.getObjectFromString((String)args[0], String.class));
UserAccountFullView userAccountFullView = userAccountService.find(arg0);

this.userAccountService.saveUserAccountNotificationPreferences(userAccountFullView.getId(), userAccountNotificationPreferencesForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
