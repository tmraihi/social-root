package org.sklsft.social.populator.command.organization;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.organization.OrganizationMemberService;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class OrganizationMemberCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(OrganizationMemberService.class);

@Inject
private OrganizationMemberService organizationMemberService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<OrganizationMemberForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<OrganizationMemberForm>(OrganizationMemberForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<OrganizationMemberForm>(OrganizationMemberForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute organizationMemberService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
OrganizationMemberForm organizationMemberForm = mapper.mapFrom(new OrganizationMemberForm(), args);

this.organizationMemberService.save(organizationMemberForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
