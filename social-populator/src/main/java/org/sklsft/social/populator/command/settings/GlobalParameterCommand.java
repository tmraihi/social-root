package org.sklsft.social.populator.command.settings;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.settings.GlobalParameterService;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class GlobalParameterCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(GlobalParameterService.class);

@Inject
private GlobalParameterService globalParameterService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<GlobalParameterForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<GlobalParameterForm>(GlobalParameterForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<GlobalParameterForm>(GlobalParameterForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute globalParameterService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
GlobalParameterForm globalParameterForm = mapper.mapFrom(new GlobalParameterForm(), args);

this.globalParameterService.save(globalParameterForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
