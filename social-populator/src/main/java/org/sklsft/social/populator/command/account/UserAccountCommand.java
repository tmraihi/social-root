package org.sklsft.social.populator.command.account;

import java.util.List;
import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.ObjectArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.impl.StringArrayToBeanMapperImpl;
import org.sklsft.commons.mapper.interfaces.ObjectArrayToBeanMapper;
import org.sklsft.generator.repository.backup.command.interfaces.BackupArgumentsCommand;
import org.sklsft.generator.repository.backup.reader.model.BackupArguments;
import org.sklsft.social.api.interfaces.account.UserAccountService;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * auto generated bean populator command class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
@Component
public class UserAccountCommand implements BackupArgumentsCommand {

/*
 * logger
 */
private static final Logger logger = LoggerFactory.getLogger(UserAccountService.class);

@Inject
private UserAccountService userAccountService;

@Override
public void execute(BackupArguments arguments) {
ObjectArrayToBeanMapper<UserAccountForm> mapper;
if (arguments.isArgumentsTyped()) {
mapper = new ObjectArrayToBeanMapperImpl<UserAccountForm>(UserAccountForm.class);
} else {
mapper = new StringArrayToBeanMapperImpl<UserAccountForm>(UserAccountForm.class);
}
for (Object[] args : arguments.getArguments()) {
String message = "execute userAccountService.save - args : ";
for (Object arg:args) {
message += "[" + arg + "]";
}
logger.info(message);

try {
UserAccountForm userAccountForm = mapper.mapFrom(new UserAccountForm(), args);

this.userAccountService.save(userAccountForm);
} catch (Exception e) {
logger.error(message + "failed : " + e.getClass().getSimpleName() + " - " + e.getMessage(), e);
}
}
}
}
