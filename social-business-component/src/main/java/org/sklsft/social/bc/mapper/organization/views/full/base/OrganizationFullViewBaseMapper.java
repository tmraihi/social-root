package org.sklsft.social.bc.mapper.organization.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.organization.forms.OrganizationForm;
import org.sklsft.social.api.model.organization.views.full.OrganizationFullView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationStateManager;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class OrganizationFullViewBaseMapper extends FullViewMapper<OrganizationFullView, Long, OrganizationForm, Organization> {

@Inject
protected OrganizationRightsManager organizationRightsManager;
@Inject
protected OrganizationStateManager organizationStateManager;

public OrganizationFullViewBaseMapper() {
super(OrganizationFullView.class, Organization.class);
}

@Override
public OrganizationFullView mapFrom(OrganizationFullView organizationFullView, Organization organization) {
organizationFullView = super.mapFrom(organizationFullView, organization);
organizationFullView.setCanUpdate(organizationRightsManager.canUpdate(organization) && organizationStateManager.canUpdate(organization));
return organizationFullView;
}

}
