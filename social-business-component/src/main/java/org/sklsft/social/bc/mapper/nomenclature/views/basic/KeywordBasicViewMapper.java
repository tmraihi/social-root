package org.sklsft.social.bc.mapper.nomenclature.views.basic;

import org.sklsft.social.bc.mapper.nomenclature.views.basic.base.KeywordBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialKeywordBasicViewMapper")
public class KeywordBasicViewMapper extends KeywordBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
