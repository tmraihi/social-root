package org.sklsft.social.bc.rightsmanager.settings.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseRightsManager implements RightsManager<ParameterType, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("ParameterType.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(ParameterType parameterType) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(ParameterType parameterType) {
if (!canAccess(parameterType)) {
throw new AccessDeniedException("ParameterType.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("ParameterType.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(ParameterType parameterType) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(ParameterType parameterType) {
if (!canSave(parameterType)) {
throw new OperationDeniedException("ParameterType.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(ParameterType parameterType) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(ParameterType parameterType) {
if (!canUpdate(parameterType)) {
throw new OperationDeniedException("ParameterType.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(ParameterType parameterType) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(ParameterType parameterType) {
if (!canDelete(parameterType)) {
throw new OperationDeniedException("ParameterType.delete.operationDenied");
}
}

}
