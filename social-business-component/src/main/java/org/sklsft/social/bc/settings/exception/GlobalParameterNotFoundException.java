package org.sklsft.social.bc.settings.exception;

public class GlobalParameterNotFoundException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public GlobalParameterNotFoundException(String message) {
		super(message);

	}

	public GlobalParameterNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
