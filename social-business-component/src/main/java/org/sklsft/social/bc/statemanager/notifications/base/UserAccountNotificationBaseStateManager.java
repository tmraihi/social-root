package org.sklsft.social.bc.statemanager.notifications.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.notifications.UserAccountNotification;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseStateManager implements StateManager<UserAccountNotification, Long> {

/**
 * can save
 */
public boolean canSave(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(UserAccountNotification userAccountNotification) {
if (!canSave(userAccountNotification)) {
throw new InvalidStateException("UserAccountNotification.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(UserAccountNotification userAccountNotification) {
if (!canUpdate(userAccountNotification)) {
throw new InvalidStateException("UserAccountNotification.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(UserAccountNotification userAccountNotification) {
if (!canDelete(userAccountNotification)) {
throw new InvalidStateException("UserAccountNotification.delete.invalidState");
}
}

}
