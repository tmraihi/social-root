package org.sklsft.social.bc.settings;

import javax.inject.Inject;

import org.hibernate.ObjectNotFoundException;
import org.sklsft.social.bc.settings.exception.GlobalParameterNotFoundException;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.GlobalParameterDao;
import org.springframework.stereotype.Component;

/**
 * This class is used to fetch a global parameter as a :
 * <li>String
 * <li>Boolean
 * <li>Integer
 * <li>Long
 * <li>Double
 * <br>The parameter type is checked for compatibility
 * @author Nicolas Thibault
 *
 */
@Component
public class SettingRetriever {
	
	@Inject
	private GlobalParameterDao globalParameterDao;
	
	
	public String getSettingAsString (SettingCodes code) {
		
		GlobalParameter parameter;
		
		try {
			parameter = globalParameterDao.find(code.name());
		} catch (ObjectNotFoundException e) {
			throw new GlobalParameterNotFoundException("No parameter found with code " + code.name(), e);
		}
		
		if (!parameter.getParameterType().getCode().equalsIgnoreCase("string")) {
			throw new IllegalArgumentException("The parameter " + code.name() + " is not a String");
		}
		
		return parameter.getValue();
	}
	
	
	public boolean getSettingAsBoolean (SettingCodes code) {
		
		GlobalParameter parameter;
		
		try {
			parameter = globalParameterDao.find(code.name());
		} catch (ObjectNotFoundException e) {
			throw new GlobalParameterNotFoundException("No parameter found with code " + code.name(), e);
		}
		
		if (!parameter.getParameterType().getCode().equalsIgnoreCase("boolean")) {
			throw new IllegalArgumentException("The parameter " + code.name() + " is not a Boolean");
		}
		
		return Boolean.valueOf(parameter.getValue());
	}
	
	
	public int getSettingAsInteger (SettingCodes code) {
		
		GlobalParameter parameter;
		
		try {
			parameter = globalParameterDao.find(code.name());
		} catch (ObjectNotFoundException e) {
			throw new GlobalParameterNotFoundException("No parameter found with code " + code.name(), e);
		}
		
		if (!parameter.getParameterType().getCode().equalsIgnoreCase("integer")) {
			throw new IllegalArgumentException("The parameter " + code.name() + " is not an Integer");
		}
		
		return Integer.valueOf(parameter.getValue()).intValue();
	}
	
	
	public long getSettingAsLong (SettingCodes code) {
		
		GlobalParameter parameter;
		
		try {
			parameter = globalParameterDao.find(code.name());
		} catch (ObjectNotFoundException e) {
			throw new GlobalParameterNotFoundException("No parameter found with code " + code.name(), e);
		}
		
		if (!parameter.getParameterType().getCode().equalsIgnoreCase("long")) {
			throw new IllegalArgumentException("The parameter " + code.name() + " is not a Long");
		}
		
		return Integer.valueOf(parameter.getValue()).longValue();
	}


	public double getSettingAsDouble (SettingCodes code) {
		
		GlobalParameter parameter;
		
		try {
			parameter = globalParameterDao.find(code.name());
		} catch (ObjectNotFoundException e) {
			throw new GlobalParameterNotFoundException("No parameter found with code " + code.name(), e);
		}
		
		if (!parameter.getParameterType().getCode().equalsIgnoreCase("double")) {
			throw new IllegalArgumentException("The parameter " + code.name() + " is not a Double");
		}
		
		return Double.valueOf(parameter.getValue()).doubleValue();	
	}
}
