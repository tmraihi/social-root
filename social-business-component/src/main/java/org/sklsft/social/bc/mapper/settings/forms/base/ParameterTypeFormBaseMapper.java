package org.sklsft.social.bc.mapper.settings.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeFormBaseMapper extends BasicMapperImpl<ParameterTypeForm, ParameterType> {

public ParameterTypeFormBaseMapper() {
super(ParameterTypeForm.class, ParameterType.class);
}

/*
 * properties
 */

/**
 * mapping form from object
 */
@Override
public ParameterTypeForm mapFrom(ParameterTypeForm parameterTypeForm, ParameterType parameterType) {
parameterTypeForm = super.mapFrom(parameterTypeForm, parameterType);
return parameterTypeForm;
}

/**
 * mapping view to object
 */
@Override
public ParameterType mapTo(ParameterTypeForm parameterTypeForm, ParameterType parameterType) {
parameterType = super.mapTo(parameterTypeForm, parameterType);
return parameterType;
}

}
