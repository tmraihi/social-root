package org.sklsft.social.bc.mapper.nomenclature.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.sklsft.social.api.model.nomenclature.views.full.KeywordFullView;
import org.sklsft.social.bc.rightsmanager.nomenclature.KeywordRightsManager;
import org.sklsft.social.bc.statemanager.nomenclature.KeywordStateManager;
import org.sklsft.social.model.nomenclature.Keyword;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class KeywordFullViewBaseMapper extends FullViewMapper<KeywordFullView, Long, KeywordForm, Keyword> {

@Inject
protected KeywordRightsManager keywordRightsManager;
@Inject
protected KeywordStateManager keywordStateManager;

public KeywordFullViewBaseMapper() {
super(KeywordFullView.class, Keyword.class);
}

@Override
public KeywordFullView mapFrom(KeywordFullView keywordFullView, Keyword keyword) {
keywordFullView = super.mapFrom(keywordFullView, keyword);
keywordFullView.setCanUpdate(keywordRightsManager.canUpdate(keyword) && keywordStateManager.canUpdate(keyword));
return keywordFullView;
}

}
