package org.sklsft.social.bc.mapper.notifications.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.views.basic.ActionTypeBasicView;
import org.sklsft.social.bc.rightsmanager.notifications.ActionTypeRightsManager;
import org.sklsft.social.bc.statemanager.notifications.ActionTypeStateManager;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBasicViewBaseMapper extends BasicMapperImpl<ActionTypeBasicView, ActionType> {

public ActionTypeBasicViewBaseMapper() {
super(ActionTypeBasicView.class, ActionType.class);
}

/*
 * properties
 */

@Inject
protected ActionTypeRightsManager actionTypeRightsManager;
@Inject
protected ActionTypeStateManager actionTypeStateManager;

/**
 * mapping view from object
 */
@Override
public ActionTypeBasicView mapFrom(ActionTypeBasicView actionTypeBasicView, ActionType actionType) {
actionTypeBasicView = super.mapFrom(actionTypeBasicView, actionType);
actionTypeBasicView.setSelected(false);
actionTypeBasicView.setCanDelete(actionTypeRightsManager.canDelete(actionType) && actionTypeStateManager.canDelete(actionType));
return actionTypeBasicView;
}

/**
 * mapping view to object
 */
@Override
public ActionType mapTo(ActionTypeBasicView actionTypeBasicView, ActionType actionType) {
actionType = super.mapTo(actionTypeBasicView, actionType);
return actionType;
}

}
