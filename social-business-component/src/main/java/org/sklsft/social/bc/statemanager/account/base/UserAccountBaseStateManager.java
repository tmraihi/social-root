package org.sklsft.social.bc.statemanager.account.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseStateManager implements StateManager<UserAccount, Long> {

/**
 * can save
 */
public boolean canSave(UserAccount userAccount) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(UserAccount userAccount) {
if (!canSave(userAccount)) {
throw new InvalidStateException("UserAccount.save.invalidState");
}
}

/**
 * can save one to many component UserAccountNotificationPreferences
 */
public boolean canSaveUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences,UserAccount userAccount) {
return true;
}

/**
 * check can save one to many component UserAccountNotificationPreferences
 */
public void checkCanSaveUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences,UserAccount userAccount) {
if (!canSaveUserAccountNotificationPreferences(userAccountNotificationPreferences, userAccount)) {
throw new InvalidStateException("UserAccountNotificationPreferences.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(UserAccount userAccount) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(UserAccount userAccount) {
if (!canUpdate(userAccount)) {
throw new InvalidStateException("UserAccount.update.invalidState");
}
}

/**
 * can update one to many component UserAccountNotificationPreferences
 */
public boolean canUpdateUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
return true;
}

/**
 * check can update one to many component UserAccountNotificationPreferences
 */
public void checkCanUpdateUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
if (!canUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences)) {
throw new InvalidStateException("UserAccountNotificationPreferences.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(UserAccount userAccount) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(UserAccount userAccount) {
if (!canDelete(userAccount)) {
throw new InvalidStateException("UserAccount.delete.invalidState");
}
}

/**
 * can delete one to many component UserAccountNotificationPreferences
 */
public boolean canDeleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
return true;
}

/**
 * check can delete one to many component UserAccountNotificationPreferences
 */
public void checkCanDeleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
if (!canDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences)) {
throw new InvalidStateException("UserAccountNotificationPreferences.delete.invalidState");
}
}

}
