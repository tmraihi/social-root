package org.sklsft.social.bc.mapper.notifications.forms;

import org.sklsft.social.bc.mapper.notifications.forms.base.UserAccountNotificationFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationFormMapper")
public class UserAccountNotificationFormMapper extends UserAccountNotificationFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
