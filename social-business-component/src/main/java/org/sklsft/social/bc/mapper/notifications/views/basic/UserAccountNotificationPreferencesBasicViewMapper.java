package org.sklsft.social.bc.mapper.notifications.views.basic;

import org.sklsft.social.bc.mapper.notifications.views.basic.base.UserAccountNotificationPreferencesBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationPreferencesBasicViewMapper")
public class UserAccountNotificationPreferencesBasicViewMapper extends UserAccountNotificationPreferencesBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
