package org.sklsft.social.bc.mapper.notifications.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationPreferencesBasicView;
import org.sklsft.social.bc.rightsmanager.account.UserAccountRightsManager;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationPreferencesBasicViewBaseMapper extends BasicMapperImpl<UserAccountNotificationPreferencesBasicView, UserAccountNotificationPreferences> {

public UserAccountNotificationPreferencesBasicViewBaseMapper() {
super(UserAccountNotificationPreferencesBasicView.class, UserAccountNotificationPreferences.class);
}

/*
 * properties
 */
@Inject
protected ActionTypeDao actionTypeDao;

@Inject
protected UserAccountRightsManager userAccountRightsManager;
@Inject
protected UserAccountStateManager userAccountStateManager;

/**
 * mapping view from object
 */
@Override
public UserAccountNotificationPreferencesBasicView mapFrom(UserAccountNotificationPreferencesBasicView userAccountNotificationPreferencesBasicView, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferencesBasicView = super.mapFrom(userAccountNotificationPreferencesBasicView, userAccountNotificationPreferences);
userAccountNotificationPreferencesBasicView.setSelected(false);
userAccountNotificationPreferencesBasicView.setCanDelete(userAccountRightsManager.canDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences) && userAccountStateManager.canDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences));
userAccountNotificationPreferencesBasicView.setActionTypeCode(userAccountNotificationPreferences.getActionType().getCode());
return userAccountNotificationPreferencesBasicView;
}

/**
 * mapping view to object
 */
@Override
public UserAccountNotificationPreferences mapTo(UserAccountNotificationPreferencesBasicView userAccountNotificationPreferencesBasicView, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferences = super.mapTo(userAccountNotificationPreferencesBasicView, userAccountNotificationPreferences);
userAccountNotificationPreferences.setActionType(actionTypeDao.find(userAccountNotificationPreferencesBasicView.getActionTypeCode()));
return userAccountNotificationPreferences;
}

}
