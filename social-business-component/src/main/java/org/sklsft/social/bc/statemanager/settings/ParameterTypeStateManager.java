package org.sklsft.social.bc.statemanager.settings;

import org.sklsft.social.bc.statemanager.settings.base.ParameterTypeBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialParameterTypeStateManager")
public class ParameterTypeStateManager extends ParameterTypeBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
