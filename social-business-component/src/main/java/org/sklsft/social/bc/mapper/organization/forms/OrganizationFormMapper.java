package org.sklsft.social.bc.mapper.organization.forms;

import org.sklsft.social.bc.mapper.organization.forms.base.OrganizationFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationFormMapper")
public class OrganizationFormMapper extends OrganizationFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
