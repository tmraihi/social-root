package org.sklsft.social.bc.mapper.organization.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberFormBaseMapper extends BasicMapperImpl<OrganizationMemberForm, OrganizationMember> {

public OrganizationMemberFormBaseMapper() {
super(OrganizationMemberForm.class, OrganizationMember.class);
}

/*
 * properties
 */
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;

/**
 * mapping form from object
 */
@Override
public OrganizationMemberForm mapFrom(OrganizationMemberForm organizationMemberForm, OrganizationMember organizationMember) {
organizationMemberForm = super.mapFrom(organizationMemberForm, organizationMember);
organizationMemberForm.setOrganizationCode(organizationMember.getOrganization().getCode());
organizationMemberForm.setUserAccountEmail(organizationMember.getUserAccount().getEmail());
return organizationMemberForm;
}

/**
 * mapping view to object
 */
@Override
public OrganizationMember mapTo(OrganizationMemberForm organizationMemberForm, OrganizationMember organizationMember) {
organizationMember = super.mapTo(organizationMemberForm, organizationMember);
organizationMember.setOrganization(organizationDao.find(organizationMemberForm.getOrganizationCode()));
organizationMember.setUserAccount(userAccountDao.find(organizationMemberForm.getUserAccountEmail()));
return organizationMember;
}

}
