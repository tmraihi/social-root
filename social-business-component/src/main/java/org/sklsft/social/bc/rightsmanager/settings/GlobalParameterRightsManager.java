package org.sklsft.social.bc.rightsmanager.settings;

import org.sklsft.social.bc.rightsmanager.settings.base.GlobalParameterBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialGlobalParameterRightsManager")
public class GlobalParameterRightsManager extends GlobalParameterBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
