package org.sklsft.social.bc.mapper.organization.views.basic;

import org.sklsft.social.bc.mapper.organization.views.basic.base.OrganizationGuestBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationGuestBasicViewMapper")
public class OrganizationGuestBasicViewMapper extends OrganizationGuestBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
