package org.sklsft.social.bc.processor.account;

import java.util.Date;
import org.sklsft.social.bc.processor.account.base.UserAccountBaseProcessor;
import org.sklsft.social.model.account.UserAccount;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class UserAccountProcessor extends UserAccountBaseProcessor {

/* Specific Code Start */
	
public Long save(UserAccount userAccount) {
		
		userAccount.setActivated(true);
		userAccount.setEmailValidated(false);
		userAccount.setCreationDate(new Date());
		
		Long id = userAccountDao.save(userAccount);
		
		return id;
	}

/* Specific Code End */
}
