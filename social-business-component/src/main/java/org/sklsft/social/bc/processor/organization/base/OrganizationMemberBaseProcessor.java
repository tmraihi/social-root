package org.sklsft.social.bc.processor.organization.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationMemberDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseProcessor implements Processor<OrganizationMember, Long> {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationMemberDao organizationMemberDao;

/**
 * process save
 */
public Long save(OrganizationMember organizationMember) {
return organizationMemberDao.save(organizationMember);
}

/**
 * process update
 */
public void update(OrganizationMember organizationMember) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(OrganizationMember organizationMember) {
organizationMemberDao.delete(organizationMember);
}

}
