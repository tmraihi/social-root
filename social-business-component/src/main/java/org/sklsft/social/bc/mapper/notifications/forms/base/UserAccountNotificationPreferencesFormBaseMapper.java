package org.sklsft.social.bc.mapper.notifications.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationPreferencesForm;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationPreferencesFormBaseMapper extends BasicMapperImpl<UserAccountNotificationPreferencesForm, UserAccountNotificationPreferences> {

public UserAccountNotificationPreferencesFormBaseMapper() {
super(UserAccountNotificationPreferencesForm.class, UserAccountNotificationPreferences.class);
}

/*
 * properties
 */
@Inject
protected ActionTypeDao actionTypeDao;

/**
 * mapping form from object
 */
@Override
public UserAccountNotificationPreferencesForm mapFrom(UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferencesForm = super.mapFrom(userAccountNotificationPreferencesForm, userAccountNotificationPreferences);
userAccountNotificationPreferencesForm.setActionTypeCode(userAccountNotificationPreferences.getActionType().getCode());
return userAccountNotificationPreferencesForm;
}

/**
 * mapping view to object
 */
@Override
public UserAccountNotificationPreferences mapTo(UserAccountNotificationPreferencesForm userAccountNotificationPreferencesForm, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferences = super.mapTo(userAccountNotificationPreferencesForm, userAccountNotificationPreferences);
userAccountNotificationPreferences.setActionType(actionTypeDao.find(userAccountNotificationPreferencesForm.getActionTypeCode()));
return userAccountNotificationPreferences;
}

}
