package org.sklsft.social.bc.mapper.nomenclature.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.nomenclature.forms.KeywordForm;
import org.sklsft.social.model.nomenclature.Keyword;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordFormBaseMapper extends BasicMapperImpl<KeywordForm, Keyword> {

public KeywordFormBaseMapper() {
super(KeywordForm.class, Keyword.class);
}

/*
 * properties
 */

/**
 * mapping form from object
 */
@Override
public KeywordForm mapFrom(KeywordForm keywordForm, Keyword keyword) {
keywordForm = super.mapFrom(keywordForm, keyword);
return keywordForm;
}

/**
 * mapping view to object
 */
@Override
public Keyword mapTo(KeywordForm keywordForm, Keyword keyword) {
keyword = super.mapTo(keywordForm, keyword);
return keyword;
}

}
