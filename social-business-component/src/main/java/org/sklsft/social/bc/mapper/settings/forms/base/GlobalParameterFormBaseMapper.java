package org.sklsft.social.bc.mapper.settings.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.ParameterTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterFormBaseMapper extends BasicMapperImpl<GlobalParameterForm, GlobalParameter> {

public GlobalParameterFormBaseMapper() {
super(GlobalParameterForm.class, GlobalParameter.class);
}

/*
 * properties
 */
@Inject
protected ParameterTypeDao parameterTypeDao;

/**
 * mapping form from object
 */
@Override
public GlobalParameterForm mapFrom(GlobalParameterForm globalParameterForm, GlobalParameter globalParameter) {
globalParameterForm = super.mapFrom(globalParameterForm, globalParameter);
globalParameterForm.setParameterTypeCode(globalParameter.getParameterType().getCode());
return globalParameterForm;
}

/**
 * mapping view to object
 */
@Override
public GlobalParameter mapTo(GlobalParameterForm globalParameterForm, GlobalParameter globalParameter) {
globalParameter = super.mapTo(globalParameterForm, globalParameter);
globalParameter.setParameterType(parameterTypeDao.find(globalParameterForm.getParameterTypeCode()));
return globalParameter;
}

}
