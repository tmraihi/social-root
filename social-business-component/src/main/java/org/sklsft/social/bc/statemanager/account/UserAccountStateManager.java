package org.sklsft.social.bc.statemanager.account;

import javax.inject.Inject;
import org.sklsft.social.api.exception.state.account.AccountAlreadyUsedException;
import org.sklsft.social.bc.statemanager.account.base.UserAccountBaseStateManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountStateManager")
public class UserAccountStateManager extends UserAccountBaseStateManager {

/* Specific Code Start */
	
	@Inject
	private UserAccountDao userAccountDao;
	
	
	public void checkCanSave(UserAccount userAccount) {
		
		if (userAccountDao.exists(userAccount.getEmail())) {
			throw new AccountAlreadyUsedException("account.alreadyUsed");
		}		
	}
	
/* Specific Code End */
}
