package org.sklsft.social.bc.statemanager.organization.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseStateManager implements StateManager<OrganizationMember, Long> {

/**
 * can save
 */
public boolean canSave(OrganizationMember organizationMember) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(OrganizationMember organizationMember) {
if (!canSave(organizationMember)) {
throw new InvalidStateException("OrganizationMember.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(OrganizationMember organizationMember) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(OrganizationMember organizationMember) {
if (!canUpdate(organizationMember)) {
throw new InvalidStateException("OrganizationMember.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(OrganizationMember organizationMember) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(OrganizationMember organizationMember) {
if (!canDelete(organizationMember)) {
throw new InvalidStateException("OrganizationMember.delete.invalidState");
}
}

}
