package org.sklsft.social.bc.mapper.organization.views.full;

import org.sklsft.social.bc.mapper.organization.views.full.base.OrganizationMemberFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationMemberFullViewMapper")
public class OrganizationMemberFullViewMapper extends OrganizationMemberFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
