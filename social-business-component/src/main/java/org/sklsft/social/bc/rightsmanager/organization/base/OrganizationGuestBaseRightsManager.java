package org.sklsft.social.bc.rightsmanager.organization.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.organization.OrganizationGuest;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseRightsManager implements RightsManager<OrganizationGuest, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("OrganizationGuest.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(OrganizationGuest organizationGuest) {
if (!canAccess(organizationGuest)) {
throw new AccessDeniedException("OrganizationGuest.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("OrganizationGuest.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(OrganizationGuest organizationGuest) {
if (!canSave(organizationGuest)) {
throw new OperationDeniedException("OrganizationGuest.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(OrganizationGuest organizationGuest) {
if (!canUpdate(organizationGuest)) {
throw new OperationDeniedException("OrganizationGuest.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(OrganizationGuest organizationGuest) {
if (!canDelete(organizationGuest)) {
throw new OperationDeniedException("OrganizationGuest.delete.operationDenied");
}
}

}
