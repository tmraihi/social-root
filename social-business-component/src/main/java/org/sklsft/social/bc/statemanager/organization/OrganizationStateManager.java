package org.sklsft.social.bc.statemanager.organization;

import org.sklsft.social.bc.statemanager.organization.base.OrganizationBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationStateManager")
public class OrganizationStateManager extends OrganizationBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
