package org.sklsft.social.bc.mapper.account.views.full;

import org.sklsft.social.bc.mapper.account.views.full.base.UserAccountFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountFullViewMapper")
public class UserAccountFullViewMapper extends UserAccountFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
