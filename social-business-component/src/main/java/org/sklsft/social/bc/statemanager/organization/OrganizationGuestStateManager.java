package org.sklsft.social.bc.statemanager.organization;

import org.sklsft.social.bc.statemanager.organization.base.OrganizationGuestBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationGuestStateManager")
public class OrganizationGuestStateManager extends OrganizationGuestBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
