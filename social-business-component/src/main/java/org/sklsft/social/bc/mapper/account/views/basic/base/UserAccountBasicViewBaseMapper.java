package org.sklsft.social.bc.mapper.account.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.account.views.basic.UserAccountBasicView;
import org.sklsft.social.bc.rightsmanager.account.UserAccountRightsManager;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.model.account.UserAccount;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBasicViewBaseMapper extends BasicMapperImpl<UserAccountBasicView, UserAccount> {

public UserAccountBasicViewBaseMapper() {
super(UserAccountBasicView.class, UserAccount.class);
}

/*
 * properties
 */

@Inject
protected UserAccountRightsManager userAccountRightsManager;
@Inject
protected UserAccountStateManager userAccountStateManager;

/**
 * mapping view from object
 */
@Override
public UserAccountBasicView mapFrom(UserAccountBasicView userAccountBasicView, UserAccount userAccount) {
userAccountBasicView = super.mapFrom(userAccountBasicView, userAccount);
userAccountBasicView.setSelected(false);
userAccountBasicView.setCanDelete(userAccountRightsManager.canDelete(userAccount) && userAccountStateManager.canDelete(userAccount));
return userAccountBasicView;
}

/**
 * mapping view to object
 */
@Override
public UserAccount mapTo(UserAccountBasicView userAccountBasicView, UserAccount userAccount) {
userAccount = super.mapTo(userAccountBasicView, userAccount);
return userAccount;
}

}
