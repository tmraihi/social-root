package org.sklsft.social.bc.processor.notifications;

import org.sklsft.social.bc.processor.notifications.base.UserAccountNotificationBaseProcessor;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class UserAccountNotificationProcessor extends UserAccountNotificationBaseProcessor {

/* Specific Code Start */
/* Specific Code End */
}
