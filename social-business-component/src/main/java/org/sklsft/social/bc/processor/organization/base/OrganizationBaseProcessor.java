package org.sklsft.social.bc.processor.organization.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBaseProcessor implements Processor<Organization, Long> {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationDao organizationDao;

/**
 * process save
 */
public Long save(Organization organization) {
return organizationDao.save(organization);
}

/**
 * process update
 */
public void update(Organization organization) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(Organization organization) {
organizationDao.delete(organization);
}

}
