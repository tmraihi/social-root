package org.sklsft.social.bc.processor.organization;

import org.sklsft.social.bc.processor.organization.base.OrganizationMemberBaseProcessor;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class OrganizationMemberProcessor extends OrganizationMemberBaseProcessor {

/* Specific Code Start */
/* Specific Code End */
}
