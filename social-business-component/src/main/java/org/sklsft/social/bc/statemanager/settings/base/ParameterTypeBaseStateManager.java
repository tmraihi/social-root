package org.sklsft.social.bc.statemanager.settings.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseStateManager implements StateManager<ParameterType, Long> {

/**
 * can save
 */
public boolean canSave(ParameterType parameterType) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(ParameterType parameterType) {
if (!canSave(parameterType)) {
throw new InvalidStateException("ParameterType.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(ParameterType parameterType) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(ParameterType parameterType) {
if (!canUpdate(parameterType)) {
throw new InvalidStateException("ParameterType.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(ParameterType parameterType) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(ParameterType parameterType) {
if (!canDelete(parameterType)) {
throw new InvalidStateException("ParameterType.delete.invalidState");
}
}

}
