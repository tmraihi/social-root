package org.sklsft.social.bc.mapper.account.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.model.account.UserAccount;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountFormBaseMapper extends BasicMapperImpl<UserAccountForm, UserAccount> {

public UserAccountFormBaseMapper() {
super(UserAccountForm.class, UserAccount.class);
}

/*
 * properties
 */

/**
 * mapping form from object
 */
@Override
public UserAccountForm mapFrom(UserAccountForm userAccountForm, UserAccount userAccount) {
userAccountForm = super.mapFrom(userAccountForm, userAccount);
return userAccountForm;
}

/**
 * mapping view to object
 */
@Override
public UserAccount mapTo(UserAccountForm userAccountForm, UserAccount userAccount) {
userAccount = super.mapTo(userAccountForm, userAccount);
return userAccount;
}

}
