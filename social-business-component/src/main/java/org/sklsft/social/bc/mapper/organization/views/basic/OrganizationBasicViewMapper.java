package org.sklsft.social.bc.mapper.organization.views.basic;

import org.sklsft.social.bc.mapper.organization.views.basic.base.OrganizationBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationBasicViewMapper")
public class OrganizationBasicViewMapper extends OrganizationBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
