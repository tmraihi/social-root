package org.sklsft.social.bc.mapper.notifications.views.full;

import org.sklsft.social.bc.mapper.notifications.views.full.base.UserAccountNotificationPreferencesFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationPreferencesFullViewMapper")
public class UserAccountNotificationPreferencesFullViewMapper extends UserAccountNotificationPreferencesFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
