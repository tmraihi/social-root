package org.sklsft.social.bc.text;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.sklsft.social.util.text.PunctuationsProvider;

public class PunctuationsProviderImpl implements PunctuationsProvider {

	private static Set<String> punctuations;
	private static String[] punctuationsArray = new String[]{".", ",", ";", "?", "!", ":"};
	
	
	static {
		punctuations = new HashSet<>(Arrays.asList(punctuationsArray));
	}
	
	@Override
	public Set<String> getPunctuations() {
		return punctuations;
	}

}
