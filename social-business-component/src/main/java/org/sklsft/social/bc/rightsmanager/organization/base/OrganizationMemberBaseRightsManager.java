package org.sklsft.social.bc.rightsmanager.organization.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBaseRightsManager implements RightsManager<OrganizationMember, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("OrganizationMember.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(OrganizationMember organizationMember) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(OrganizationMember organizationMember) {
if (!canAccess(organizationMember)) {
throw new AccessDeniedException("OrganizationMember.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("OrganizationMember.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(OrganizationMember organizationMember) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(OrganizationMember organizationMember) {
if (!canSave(organizationMember)) {
throw new OperationDeniedException("OrganizationMember.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(OrganizationMember organizationMember) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(OrganizationMember organizationMember) {
if (!canUpdate(organizationMember)) {
throw new OperationDeniedException("OrganizationMember.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(OrganizationMember organizationMember) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(OrganizationMember organizationMember) {
if (!canDelete(organizationMember)) {
throw new OperationDeniedException("OrganizationMember.delete.operationDenied");
}
}

}
