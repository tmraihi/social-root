package org.sklsft.social.bc.statemanager.nomenclature.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.nomenclature.Keyword;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseStateManager implements StateManager<Keyword, Long> {

/**
 * can save
 */
public boolean canSave(Keyword keyword) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(Keyword keyword) {
if (!canSave(keyword)) {
throw new InvalidStateException("Keyword.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(Keyword keyword) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(Keyword keyword) {
if (!canUpdate(keyword)) {
throw new InvalidStateException("Keyword.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(Keyword keyword) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(Keyword keyword) {
if (!canDelete(keyword)) {
throw new InvalidStateException("Keyword.delete.invalidState");
}
}

}
