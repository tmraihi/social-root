package org.sklsft.social.bc.processor.nomenclature.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.nomenclature.Keyword;
import org.sklsft.social.repository.dao.interfaces.nomenclature.KeywordDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseProcessor implements Processor<Keyword, Long> {

/*
 * properties injected by spring
 */
@Inject
protected KeywordDao keywordDao;

/**
 * process save
 */
public Long save(Keyword keyword) {
return keywordDao.save(keyword);
}

/**
 * process update
 */
public void update(Keyword keyword) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(Keyword keyword) {
keywordDao.delete(keyword);
}

}
