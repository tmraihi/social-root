package org.sklsft.social.bc.rightsmanager.notifications;

import org.sklsft.social.bc.rightsmanager.notifications.base.UserAccountNotificationBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationRightsManager")
public class UserAccountNotificationRightsManager extends UserAccountNotificationBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
