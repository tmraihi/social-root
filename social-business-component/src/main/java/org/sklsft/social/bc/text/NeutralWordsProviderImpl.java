package org.sklsft.social.bc.text;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.sklsft.social.util.text.NeutralWordsProvider;

public class NeutralWordsProviderImpl implements NeutralWordsProvider {

	private static Set<String> neutralWords;
	private static String[] neutralWordsArray = new String[]{};
	
	
	static {
		neutralWords = new HashSet<>(Arrays.asList(neutralWordsArray));
	}
	
	@Override
	public Set<String> getNeutralWords() {
		return neutralWords;
	}

}
