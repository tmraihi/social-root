package org.sklsft.social.bc.mapper.settings.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.settings.forms.ParameterTypeForm;
import org.sklsft.social.api.model.settings.views.full.ParameterTypeFullView;
import org.sklsft.social.bc.rightsmanager.settings.ParameterTypeRightsManager;
import org.sklsft.social.bc.statemanager.settings.ParameterTypeStateManager;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class ParameterTypeFullViewBaseMapper extends FullViewMapper<ParameterTypeFullView, Long, ParameterTypeForm, ParameterType> {

@Inject
protected ParameterTypeRightsManager parameterTypeRightsManager;
@Inject
protected ParameterTypeStateManager parameterTypeStateManager;

public ParameterTypeFullViewBaseMapper() {
super(ParameterTypeFullView.class, ParameterType.class);
}

@Override
public ParameterTypeFullView mapFrom(ParameterTypeFullView parameterTypeFullView, ParameterType parameterType) {
parameterTypeFullView = super.mapFrom(parameterTypeFullView, parameterType);
parameterTypeFullView.setCanUpdate(parameterTypeRightsManager.canUpdate(parameterType) && parameterTypeStateManager.canUpdate(parameterType));
return parameterTypeFullView;
}

}
