package org.sklsft.social.bc.mapper.settings.forms;

import org.sklsft.social.bc.mapper.settings.forms.base.GlobalParameterFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialGlobalParameterFormMapper")
public class GlobalParameterFormMapper extends GlobalParameterFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
