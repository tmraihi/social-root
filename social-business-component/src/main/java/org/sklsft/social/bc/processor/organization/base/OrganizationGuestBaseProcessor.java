package org.sklsft.social.bc.processor.organization.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationGuestDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseProcessor implements Processor<OrganizationGuest, Long> {

/*
 * properties injected by spring
 */
@Inject
protected OrganizationGuestDao organizationGuestDao;

/**
 * process save
 */
public Long save(OrganizationGuest organizationGuest) {
return organizationGuestDao.save(organizationGuest);
}

/**
 * process update
 */
public void update(OrganizationGuest organizationGuest) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(OrganizationGuest organizationGuest) {
organizationGuestDao.delete(organizationGuest);
}

}
