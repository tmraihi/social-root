package org.sklsft.social.bc.mapper.organization.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.organization.forms.OrganizationMemberForm;
import org.sklsft.social.api.model.organization.views.full.OrganizationMemberFullView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationMemberRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationMemberStateManager;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class OrganizationMemberFullViewBaseMapper extends FullViewMapper<OrganizationMemberFullView, Long, OrganizationMemberForm, OrganizationMember> {

@Inject
protected OrganizationMemberRightsManager organizationMemberRightsManager;
@Inject
protected OrganizationMemberStateManager organizationMemberStateManager;

public OrganizationMemberFullViewBaseMapper() {
super(OrganizationMemberFullView.class, OrganizationMember.class);
}

@Override
public OrganizationMemberFullView mapFrom(OrganizationMemberFullView organizationMemberFullView, OrganizationMember organizationMember) {
organizationMemberFullView = super.mapFrom(organizationMemberFullView, organizationMember);
organizationMemberFullView.setCanUpdate(organizationMemberRightsManager.canUpdate(organizationMember) && organizationMemberStateManager.canUpdate(organizationMember));
return organizationMemberFullView;
}

}
