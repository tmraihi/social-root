package org.sklsft.social.bc.rightsmanager.settings.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.settings.GlobalParameter;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseRightsManager implements RightsManager<GlobalParameter, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("GlobalParameter.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(GlobalParameter globalParameter) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(GlobalParameter globalParameter) {
if (!canAccess(globalParameter)) {
throw new AccessDeniedException("GlobalParameter.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("GlobalParameter.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(GlobalParameter globalParameter) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(GlobalParameter globalParameter) {
if (!canSave(globalParameter)) {
throw new OperationDeniedException("GlobalParameter.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(GlobalParameter globalParameter) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(GlobalParameter globalParameter) {
if (!canUpdate(globalParameter)) {
throw new OperationDeniedException("GlobalParameter.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(GlobalParameter globalParameter) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(GlobalParameter globalParameter) {
if (!canDelete(globalParameter)) {
throw new OperationDeniedException("GlobalParameter.delete.operationDenied");
}
}

}
