package org.sklsft.social.bc.mapper.notifications.views.full;

import org.sklsft.social.bc.mapper.notifications.views.full.base.ActionTypeFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialActionTypeFullViewMapper")
public class ActionTypeFullViewMapper extends ActionTypeFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
