package org.sklsft.social.bc.mapper.nomenclature.views.full;

import org.sklsft.social.bc.mapper.nomenclature.views.full.base.KeywordFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialKeywordFullViewMapper")
public class KeywordFullViewMapper extends KeywordFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
