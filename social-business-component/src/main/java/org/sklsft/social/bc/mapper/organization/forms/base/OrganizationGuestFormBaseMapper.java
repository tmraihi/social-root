package org.sklsft.social.bc.mapper.organization.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestFormBaseMapper extends BasicMapperImpl<OrganizationGuestForm, OrganizationGuest> {

public OrganizationGuestFormBaseMapper() {
super(OrganizationGuestForm.class, OrganizationGuest.class);
}

/*
 * properties
 */
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;

/**
 * mapping form from object
 */
@Override
public OrganizationGuestForm mapFrom(OrganizationGuestForm organizationGuestForm, OrganizationGuest organizationGuest) {
organizationGuestForm = super.mapFrom(organizationGuestForm, organizationGuest);
organizationGuestForm.setOrganizationCode(organizationGuest.getOrganization().getCode());
if (organizationGuest.getUserAccount() != null) {
organizationGuestForm.setUserAccountEmail(organizationGuest.getUserAccount().getEmail());
}
return organizationGuestForm;
}

/**
 * mapping view to object
 */
@Override
public OrganizationGuest mapTo(OrganizationGuestForm organizationGuestForm, OrganizationGuest organizationGuest) {
organizationGuest = super.mapTo(organizationGuestForm, organizationGuest);
organizationGuest.setOrganization(organizationDao.find(organizationGuestForm.getOrganizationCode()));
organizationGuest.setUserAccount(userAccountDao.find(organizationGuestForm.getUserAccountEmail()));
return organizationGuest;
}

}
