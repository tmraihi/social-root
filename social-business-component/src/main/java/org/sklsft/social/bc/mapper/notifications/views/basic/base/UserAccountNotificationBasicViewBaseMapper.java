package org.sklsft.social.bc.mapper.notifications.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.views.basic.UserAccountNotificationBasicView;
import org.sklsft.social.bc.rightsmanager.notifications.UserAccountNotificationRightsManager;
import org.sklsft.social.bc.statemanager.notifications.UserAccountNotificationStateManager;
import org.sklsft.social.model.notifications.UserAccountNotification;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBasicViewBaseMapper extends BasicMapperImpl<UserAccountNotificationBasicView, UserAccountNotification> {

public UserAccountNotificationBasicViewBaseMapper() {
super(UserAccountNotificationBasicView.class, UserAccountNotification.class);
}

/*
 * properties
 */
@Inject
protected UserAccountDao userAccountDao;
@Inject
protected ActionTypeDao actionTypeDao;

@Inject
protected UserAccountNotificationRightsManager userAccountNotificationRightsManager;
@Inject
protected UserAccountNotificationStateManager userAccountNotificationStateManager;

/**
 * mapping view from object
 */
@Override
public UserAccountNotificationBasicView mapFrom(UserAccountNotificationBasicView userAccountNotificationBasicView, UserAccountNotification userAccountNotification) {
userAccountNotificationBasicView = super.mapFrom(userAccountNotificationBasicView, userAccountNotification);
userAccountNotificationBasicView.setSelected(false);
userAccountNotificationBasicView.setCanDelete(userAccountNotificationRightsManager.canDelete(userAccountNotification) && userAccountNotificationStateManager.canDelete(userAccountNotification));
userAccountNotificationBasicView.setNotifiedEmail(userAccountNotification.getNotified().getEmail());
userAccountNotificationBasicView.setActionTypeCode(userAccountNotification.getActionType().getCode());
return userAccountNotificationBasicView;
}

/**
 * mapping view to object
 */
@Override
public UserAccountNotification mapTo(UserAccountNotificationBasicView userAccountNotificationBasicView, UserAccountNotification userAccountNotification) {
userAccountNotification = super.mapTo(userAccountNotificationBasicView, userAccountNotification);
userAccountNotification.setNotified(userAccountDao.find(userAccountNotificationBasicView.getNotifiedEmail()));
userAccountNotification.setActionType(actionTypeDao.find(userAccountNotificationBasicView.getActionTypeCode()));
return userAccountNotification;
}

}
