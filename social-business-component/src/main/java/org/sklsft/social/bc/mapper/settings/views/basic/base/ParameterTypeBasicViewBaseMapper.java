package org.sklsft.social.bc.mapper.settings.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.settings.views.basic.ParameterTypeBasicView;
import org.sklsft.social.bc.rightsmanager.settings.ParameterTypeRightsManager;
import org.sklsft.social.bc.statemanager.settings.ParameterTypeStateManager;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBasicViewBaseMapper extends BasicMapperImpl<ParameterTypeBasicView, ParameterType> {

public ParameterTypeBasicViewBaseMapper() {
super(ParameterTypeBasicView.class, ParameterType.class);
}

/*
 * properties
 */

@Inject
protected ParameterTypeRightsManager parameterTypeRightsManager;
@Inject
protected ParameterTypeStateManager parameterTypeStateManager;

/**
 * mapping view from object
 */
@Override
public ParameterTypeBasicView mapFrom(ParameterTypeBasicView parameterTypeBasicView, ParameterType parameterType) {
parameterTypeBasicView = super.mapFrom(parameterTypeBasicView, parameterType);
parameterTypeBasicView.setSelected(false);
parameterTypeBasicView.setCanDelete(parameterTypeRightsManager.canDelete(parameterType) && parameterTypeStateManager.canDelete(parameterType));
return parameterTypeBasicView;
}

/**
 * mapping view to object
 */
@Override
public ParameterType mapTo(ParameterTypeBasicView parameterTypeBasicView, ParameterType parameterType) {
parameterType = super.mapTo(parameterTypeBasicView, parameterType);
return parameterType;
}

}
