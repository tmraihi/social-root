package org.sklsft.social.bc.mapper.organization.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.forms.OrganizationForm;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationFormBaseMapper extends BasicMapperImpl<OrganizationForm, Organization> {

public OrganizationFormBaseMapper() {
super(OrganizationForm.class, Organization.class);
}

/*
 * properties
 */

/**
 * mapping form from object
 */
@Override
public OrganizationForm mapFrom(OrganizationForm organizationForm, Organization organization) {
organizationForm = super.mapFrom(organizationForm, organization);
return organizationForm;
}

/**
 * mapping view to object
 */
@Override
public Organization mapTo(OrganizationForm organizationForm, Organization organization) {
organization = super.mapTo(organizationForm, organization);
return organization;
}

}
