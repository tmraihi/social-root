package org.sklsft.social.bc.mapper.notifications.views.basic;

import org.sklsft.social.bc.mapper.notifications.views.basic.base.ActionTypeBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialActionTypeBasicViewMapper")
public class ActionTypeBasicViewMapper extends ActionTypeBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
