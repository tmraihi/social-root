package org.sklsft.social.bc.statemanager.nomenclature;

import org.sklsft.social.bc.statemanager.nomenclature.base.KeywordBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialKeywordStateManager")
public class KeywordStateManager extends KeywordBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
