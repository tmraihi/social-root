package org.sklsft.social.bc.mapper.notifications.forms;

import org.sklsft.social.bc.mapper.notifications.forms.base.MemberNotificationPreferencesFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialMemberNotificationPreferencesFormMapper")
public class MemberNotificationPreferencesFormMapper extends MemberNotificationPreferencesFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
