package org.sklsft.social.bc.rightsmanager.notifications.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseRightsManager implements RightsManager<ActionType, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("ActionType.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(ActionType actionType) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(ActionType actionType) {
if (!canAccess(actionType)) {
throw new AccessDeniedException("ActionType.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("ActionType.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(ActionType actionType) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(ActionType actionType) {
if (!canSave(actionType)) {
throw new OperationDeniedException("ActionType.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(ActionType actionType) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(ActionType actionType) {
if (!canUpdate(actionType)) {
throw new OperationDeniedException("ActionType.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(ActionType actionType) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(ActionType actionType) {
if (!canDelete(actionType)) {
throw new OperationDeniedException("ActionType.delete.operationDenied");
}
}

}
