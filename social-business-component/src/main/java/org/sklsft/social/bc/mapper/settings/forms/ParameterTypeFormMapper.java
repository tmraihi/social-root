package org.sklsft.social.bc.mapper.settings.forms;

import org.sklsft.social.bc.mapper.settings.forms.base.ParameterTypeFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialParameterTypeFormMapper")
public class ParameterTypeFormMapper extends ParameterTypeFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
