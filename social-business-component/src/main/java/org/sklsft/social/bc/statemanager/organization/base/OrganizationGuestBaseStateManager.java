package org.sklsft.social.bc.statemanager.organization.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.organization.OrganizationGuest;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBaseStateManager implements StateManager<OrganizationGuest, Long> {

/**
 * can save
 */
public boolean canSave(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(OrganizationGuest organizationGuest) {
if (!canSave(organizationGuest)) {
throw new InvalidStateException("OrganizationGuest.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(OrganizationGuest organizationGuest) {
if (!canUpdate(organizationGuest)) {
throw new InvalidStateException("OrganizationGuest.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(OrganizationGuest organizationGuest) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(OrganizationGuest organizationGuest) {
if (!canDelete(organizationGuest)) {
throw new InvalidStateException("OrganizationGuest.delete.invalidState");
}
}

}
