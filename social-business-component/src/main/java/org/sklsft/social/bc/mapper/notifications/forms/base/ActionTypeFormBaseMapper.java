package org.sklsft.social.bc.mapper.notifications.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.forms.ActionTypeForm;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeFormBaseMapper extends BasicMapperImpl<ActionTypeForm, ActionType> {

public ActionTypeFormBaseMapper() {
super(ActionTypeForm.class, ActionType.class);
}

/*
 * properties
 */

/**
 * mapping form from object
 */
@Override
public ActionTypeForm mapFrom(ActionTypeForm actionTypeForm, ActionType actionType) {
actionTypeForm = super.mapFrom(actionTypeForm, actionType);
return actionTypeForm;
}

/**
 * mapping view to object
 */
@Override
public ActionType mapTo(ActionTypeForm actionTypeForm, ActionType actionType) {
actionType = super.mapTo(actionTypeForm, actionType);
return actionType;
}

}
