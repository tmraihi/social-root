package org.sklsft.social.bc.crypto;

import javax.inject.Inject;

import org.apache.commons.codec.binary.Base64;
import org.sklsft.commons.crypto.accessors.AesKeyAccessor;
import org.sklsft.social.bc.settings.SettingCodes;
import org.sklsft.social.bc.settings.SettingRetriever;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Component
public class AesKeyAccessorImpl implements AesKeyAccessor {
	
	@Inject
	private SettingRetriever settingRetriever;
	
	
	@Override
	@Transactional(readOnly=true)
	public byte[] getAesKey() {
		
		String tokenKey = settingRetriever.getSettingAsString(SettingCodes.AES_KEY);
		
		return Base64.decodeBase64(tokenKey);
		
	}
}
