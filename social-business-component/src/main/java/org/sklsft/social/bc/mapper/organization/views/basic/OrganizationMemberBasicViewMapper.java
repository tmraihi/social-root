package org.sklsft.social.bc.mapper.organization.views.basic;

import org.sklsft.social.bc.mapper.organization.views.basic.base.OrganizationMemberBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationMemberBasicViewMapper")
public class OrganizationMemberBasicViewMapper extends OrganizationMemberBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
