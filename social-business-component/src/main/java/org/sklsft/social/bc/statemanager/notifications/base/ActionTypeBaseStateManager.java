package org.sklsft.social.bc.statemanager.notifications.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseStateManager implements StateManager<ActionType, Long> {

/**
 * can save
 */
public boolean canSave(ActionType actionType) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(ActionType actionType) {
if (!canSave(actionType)) {
throw new InvalidStateException("ActionType.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(ActionType actionType) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(ActionType actionType) {
if (!canUpdate(actionType)) {
throw new InvalidStateException("ActionType.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(ActionType actionType) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(ActionType actionType) {
if (!canDelete(actionType)) {
throw new InvalidStateException("ActionType.delete.invalidState");
}
}

}
