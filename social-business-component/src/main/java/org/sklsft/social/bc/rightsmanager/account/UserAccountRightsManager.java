package org.sklsft.social.bc.rightsmanager.account;

import org.sklsft.social.bc.rightsmanager.account.base.UserAccountBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountRightsManager")
public class UserAccountRightsManager extends UserAccountBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
