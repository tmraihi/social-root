package org.sklsft.social.bc.mapper.organization.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.organization.forms.OrganizationGuestForm;
import org.sklsft.social.api.model.organization.views.full.OrganizationGuestFullView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationGuestRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationGuestStateManager;
import org.sklsft.social.model.organization.OrganizationGuest;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class OrganizationGuestFullViewBaseMapper extends FullViewMapper<OrganizationGuestFullView, Long, OrganizationGuestForm, OrganizationGuest> {

@Inject
protected OrganizationGuestRightsManager organizationGuestRightsManager;
@Inject
protected OrganizationGuestStateManager organizationGuestStateManager;

public OrganizationGuestFullViewBaseMapper() {
super(OrganizationGuestFullView.class, OrganizationGuest.class);
}

@Override
public OrganizationGuestFullView mapFrom(OrganizationGuestFullView organizationGuestFullView, OrganizationGuest organizationGuest) {
organizationGuestFullView = super.mapFrom(organizationGuestFullView, organizationGuest);
organizationGuestFullView.setCanUpdate(organizationGuestRightsManager.canUpdate(organizationGuest) && organizationGuestStateManager.canUpdate(organizationGuest));
return organizationGuestFullView;
}

}
