package org.sklsft.social.bc.mapper.notifications.forms.base;

import javax.inject.Inject;

import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.forms.MemberNotificationPreferencesForm;
import org.sklsft.social.model.notifications.MemberNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class MemberNotificationPreferencesFormBaseMapper extends BasicMapperImpl<MemberNotificationPreferencesForm, MemberNotificationPreferences> {

public MemberNotificationPreferencesFormBaseMapper() {
super(MemberNotificationPreferencesForm.class, MemberNotificationPreferences.class);
}

/*
 * properties
 */
@Inject
protected ActionTypeDao actionTypeDao;

/**
 * mapping form from object
 */
@Override
public MemberNotificationPreferencesForm mapFrom(MemberNotificationPreferencesForm memberNotificationPreferencesForm, MemberNotificationPreferences memberNotificationPreferences) {
memberNotificationPreferencesForm = super.mapFrom(memberNotificationPreferencesForm, memberNotificationPreferences);
memberNotificationPreferencesForm.setActionTypeCode(memberNotificationPreferences.getActionType().getCode());
return memberNotificationPreferencesForm;
}

/**
 * mapping view to object
 */
@Override
public MemberNotificationPreferences mapTo(MemberNotificationPreferencesForm memberNotificationPreferencesForm, MemberNotificationPreferences memberNotificationPreferences) {
memberNotificationPreferences = super.mapTo(memberNotificationPreferencesForm, memberNotificationPreferences);
memberNotificationPreferences.setActionType(actionTypeDao.find(memberNotificationPreferencesForm.getActionTypeCode()));
return memberNotificationPreferences;
}

}
