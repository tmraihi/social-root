package org.sklsft.social.bc.mapper.nomenclature.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.nomenclature.views.basic.KeywordBasicView;
import org.sklsft.social.bc.rightsmanager.nomenclature.KeywordRightsManager;
import org.sklsft.social.bc.statemanager.nomenclature.KeywordStateManager;
import org.sklsft.social.model.nomenclature.Keyword;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBasicViewBaseMapper extends BasicMapperImpl<KeywordBasicView, Keyword> {

public KeywordBasicViewBaseMapper() {
super(KeywordBasicView.class, Keyword.class);
}

/*
 * properties
 */

@Inject
protected KeywordRightsManager keywordRightsManager;
@Inject
protected KeywordStateManager keywordStateManager;

/**
 * mapping view from object
 */
@Override
public KeywordBasicView mapFrom(KeywordBasicView keywordBasicView, Keyword keyword) {
keywordBasicView = super.mapFrom(keywordBasicView, keyword);
keywordBasicView.setSelected(false);
keywordBasicView.setCanDelete(keywordRightsManager.canDelete(keyword) && keywordStateManager.canDelete(keyword));
return keywordBasicView;
}

/**
 * mapping view to object
 */
@Override
public Keyword mapTo(KeywordBasicView keywordBasicView, Keyword keyword) {
keyword = super.mapTo(keywordBasicView, keyword);
return keyword;
}

}
