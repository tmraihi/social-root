package org.sklsft.social.bc.mapper.notifications.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.notifications.forms.ActionTypeForm;
import org.sklsft.social.api.model.notifications.views.full.ActionTypeFullView;
import org.sklsft.social.bc.rightsmanager.notifications.ActionTypeRightsManager;
import org.sklsft.social.bc.statemanager.notifications.ActionTypeStateManager;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class ActionTypeFullViewBaseMapper extends FullViewMapper<ActionTypeFullView, Long, ActionTypeForm, ActionType> {

@Inject
protected ActionTypeRightsManager actionTypeRightsManager;
@Inject
protected ActionTypeStateManager actionTypeStateManager;

public ActionTypeFullViewBaseMapper() {
super(ActionTypeFullView.class, ActionType.class);
}

@Override
public ActionTypeFullView mapFrom(ActionTypeFullView actionTypeFullView, ActionType actionType) {
actionTypeFullView = super.mapFrom(actionTypeFullView, actionType);
actionTypeFullView.setCanUpdate(actionTypeRightsManager.canUpdate(actionType) && actionTypeStateManager.canUpdate(actionType));
return actionTypeFullView;
}

}
