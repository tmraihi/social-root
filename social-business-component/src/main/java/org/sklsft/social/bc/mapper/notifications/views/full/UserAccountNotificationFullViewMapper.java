package org.sklsft.social.bc.mapper.notifications.views.full;

import org.sklsft.social.bc.mapper.notifications.views.full.base.UserAccountNotificationFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationFullViewMapper")
public class UserAccountNotificationFullViewMapper extends UserAccountNotificationFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
