package org.sklsft.social.bc.statemanager.notifications;

import org.sklsft.social.bc.statemanager.notifications.base.UserAccountNotificationBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationStateManager")
public class UserAccountNotificationStateManager extends UserAccountNotificationBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
