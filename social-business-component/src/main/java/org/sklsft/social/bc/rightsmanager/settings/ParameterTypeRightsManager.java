package org.sklsft.social.bc.rightsmanager.settings;

import org.sklsft.social.bc.rightsmanager.settings.base.ParameterTypeBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialParameterTypeRightsManager")
public class ParameterTypeRightsManager extends ParameterTypeBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
