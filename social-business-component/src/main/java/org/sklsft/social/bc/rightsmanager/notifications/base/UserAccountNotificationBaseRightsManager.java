package org.sklsft.social.bc.rightsmanager.notifications.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.notifications.UserAccountNotification;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseRightsManager implements RightsManager<UserAccountNotification, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("UserAccountNotification.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(UserAccountNotification userAccountNotification) {
if (!canAccess(userAccountNotification)) {
throw new AccessDeniedException("UserAccountNotification.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("UserAccountNotification.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(UserAccountNotification userAccountNotification) {
if (!canSave(userAccountNotification)) {
throw new OperationDeniedException("UserAccountNotification.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(UserAccountNotification userAccountNotification) {
if (!canUpdate(userAccountNotification)) {
throw new OperationDeniedException("UserAccountNotification.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(UserAccountNotification userAccountNotification) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(UserAccountNotification userAccountNotification) {
if (!canDelete(userAccountNotification)) {
throw new OperationDeniedException("UserAccountNotification.delete.operationDenied");
}
}

}
