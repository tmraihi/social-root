package org.sklsft.social.bc.mapper.notifications.views.basic;

import org.sklsft.social.bc.mapper.notifications.views.basic.base.UserAccountNotificationBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountNotificationBasicViewMapper")
public class UserAccountNotificationBasicViewMapper extends UserAccountNotificationBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
