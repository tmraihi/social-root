package org.sklsft.social.bc.mapper.notifications.forms;

import org.sklsft.social.bc.mapper.notifications.forms.base.ActionTypeFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialActionTypeFormMapper")
public class ActionTypeFormMapper extends ActionTypeFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
