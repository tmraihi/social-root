package org.sklsft.social.bc.statemanager.settings.base;

import org.sklsft.commons.api.exception.state.InvalidStateException;
import org.sklsft.commons.model.patterns.StateManager;
import org.sklsft.social.model.settings.GlobalParameter;

/**
 * auto generated base state manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseStateManager implements StateManager<GlobalParameter, Long> {

/**
 * can save
 */
public boolean canSave(GlobalParameter globalParameter) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(GlobalParameter globalParameter) {
if (!canSave(globalParameter)) {
throw new InvalidStateException("GlobalParameter.save.invalidState");
}
}

/**
 * can update
 */
public boolean canUpdate(GlobalParameter globalParameter) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(GlobalParameter globalParameter) {
if (!canUpdate(globalParameter)) {
throw new InvalidStateException("GlobalParameter.update.invalidState");
}
}

/**
 * can delete
 */
public boolean canDelete(GlobalParameter globalParameter) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(GlobalParameter globalParameter) {
if (!canDelete(globalParameter)) {
throw new InvalidStateException("GlobalParameter.delete.invalidState");
}
}

}
