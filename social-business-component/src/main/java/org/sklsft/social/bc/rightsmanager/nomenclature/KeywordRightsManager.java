package org.sklsft.social.bc.rightsmanager.nomenclature;

import org.sklsft.social.bc.rightsmanager.nomenclature.base.KeywordBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialKeywordRightsManager")
public class KeywordRightsManager extends KeywordBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
