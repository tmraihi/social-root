package org.sklsft.social.bc.mapper.nomenclature.forms;

import org.sklsft.social.bc.mapper.nomenclature.forms.base.KeywordFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialKeywordFormMapper")
public class KeywordFormMapper extends KeywordFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
