package org.sklsft.social.bc.settings;

/**
 * An enum of the global settings
 * @author Nicolas Thibault
 *
 */
public enum SettingCodes {
	
	AES_KEY;
}
