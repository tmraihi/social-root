package org.sklsft.social.bc.processor.nomenclature;

import org.sklsft.social.bc.processor.nomenclature.base.KeywordBaseProcessor;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class KeywordProcessor extends KeywordBaseProcessor {

/* Specific Code Start */
/* Specific Code End */
}
