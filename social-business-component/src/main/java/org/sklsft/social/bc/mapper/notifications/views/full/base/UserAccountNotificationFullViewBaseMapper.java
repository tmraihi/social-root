package org.sklsft.social.bc.mapper.notifications.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationFullView;
import org.sklsft.social.bc.rightsmanager.notifications.UserAccountNotificationRightsManager;
import org.sklsft.social.bc.statemanager.notifications.UserAccountNotificationStateManager;
import org.sklsft.social.model.notifications.UserAccountNotification;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class UserAccountNotificationFullViewBaseMapper extends FullViewMapper<UserAccountNotificationFullView, Long, UserAccountNotificationForm, UserAccountNotification> {

@Inject
protected UserAccountNotificationRightsManager userAccountNotificationRightsManager;
@Inject
protected UserAccountNotificationStateManager userAccountNotificationStateManager;

public UserAccountNotificationFullViewBaseMapper() {
super(UserAccountNotificationFullView.class, UserAccountNotification.class);
}

@Override
public UserAccountNotificationFullView mapFrom(UserAccountNotificationFullView userAccountNotificationFullView, UserAccountNotification userAccountNotification) {
userAccountNotificationFullView = super.mapFrom(userAccountNotificationFullView, userAccountNotification);
userAccountNotificationFullView.setCanUpdate(userAccountNotificationRightsManager.canUpdate(userAccountNotification) && userAccountNotificationStateManager.canUpdate(userAccountNotification));
return userAccountNotificationFullView;
}

}
