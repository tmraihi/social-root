package org.sklsft.social.bc.rightsmanager.nomenclature.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.nomenclature.Keyword;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class KeywordBaseRightsManager implements RightsManager<Keyword, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("Keyword.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(Keyword keyword) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(Keyword keyword) {
if (!canAccess(keyword)) {
throw new AccessDeniedException("Keyword.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("Keyword.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(Keyword keyword) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(Keyword keyword) {
if (!canSave(keyword)) {
throw new OperationDeniedException("Keyword.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(Keyword keyword) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(Keyword keyword) {
if (!canUpdate(keyword)) {
throw new OperationDeniedException("Keyword.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(Keyword keyword) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(Keyword keyword) {
if (!canDelete(keyword)) {
throw new OperationDeniedException("Keyword.delete.operationDenied");
}
}

}
