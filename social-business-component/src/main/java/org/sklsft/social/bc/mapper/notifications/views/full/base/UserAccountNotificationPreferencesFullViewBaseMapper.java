package org.sklsft.social.bc.mapper.notifications.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationPreferencesForm;
import org.sklsft.social.api.model.notifications.views.full.UserAccountNotificationPreferencesFullView;
import org.sklsft.social.bc.rightsmanager.account.UserAccountRightsManager;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class UserAccountNotificationPreferencesFullViewBaseMapper extends FullViewMapper<UserAccountNotificationPreferencesFullView, Long, UserAccountNotificationPreferencesForm, UserAccountNotificationPreferences> {

@Inject
protected UserAccountRightsManager userAccountRightsManager;
@Inject
protected UserAccountStateManager userAccountStateManager;

public UserAccountNotificationPreferencesFullViewBaseMapper() {
super(UserAccountNotificationPreferencesFullView.class, UserAccountNotificationPreferences.class);
}

@Override
public UserAccountNotificationPreferencesFullView mapFrom(UserAccountNotificationPreferencesFullView userAccountNotificationPreferencesFullView, UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountNotificationPreferencesFullView = super.mapFrom(userAccountNotificationPreferencesFullView, userAccountNotificationPreferences);
userAccountNotificationPreferencesFullView.setCanUpdate(userAccountRightsManager.canUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences) && userAccountStateManager.canUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences));
return userAccountNotificationPreferencesFullView;
}

}
