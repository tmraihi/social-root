package org.sklsft.social.bc.rightsmanager.account.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseRightsManager implements RightsManager<UserAccount, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("UserAccount.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(UserAccount userAccount) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(UserAccount userAccount) {
if (!canAccess(userAccount)) {
throw new AccessDeniedException("UserAccount.accessDenied");
}
}

/**
 * can access one to many component UserAccountNotificationPreferences
 */
public boolean canAccessUserAccountNotificationPreferences(UserAccount userAccount) {
return canAccess(userAccount);
}

/**
 * check can access one to many component UserAccountNotificationPreferences
 */
public void checkCanAccessUserAccountNotificationPreferences(UserAccount userAccount) {
if (!canAccessUserAccountNotificationPreferences(userAccount)) {
throw new AccessDeniedException("UserAccountNotificationPreferences.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("UserAccount.create.operationDenied");
}
}

/**
 * can create one to many component UserAccountNotificationPreferences
 */
public boolean canCreateUserAccountNotificationPreferences(UserAccount userAccount) {
return canUpdate(userAccount);
}

/**
 * check can create one to many component UserAccountNotificationPreferences
 */
public void checkCanCreateUserAccountNotificationPreferences(UserAccount userAccount) {
if (!canCreateUserAccountNotificationPreferences(userAccount)) {
throw new OperationDeniedException("UserAccountNotificationPreferences.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(UserAccount userAccount) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(UserAccount userAccount) {
if (!canSave(userAccount)) {
throw new OperationDeniedException("UserAccount.save.operationDenied");
}
}

/**
 * can save one to many component UserAccountNotificationPreferences
 */
public boolean canSaveUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences,UserAccount userAccount) {
return canUpdate(userAccount);
}

/**
 * check can save one to many component UserAccountNotificationPreferences
 */
public void checkCanSaveUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences,UserAccount userAccount) {
if (!canSaveUserAccountNotificationPreferences(userAccountNotificationPreferences, userAccount)) {
throw new OperationDeniedException("UserAccountNotificationPreferences.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(UserAccount userAccount) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(UserAccount userAccount) {
if (!canUpdate(userAccount)) {
throw new OperationDeniedException("UserAccount.update.operationDenied");
}
}

/**
 * can update one to many component UserAccountNotificationPreferences
 */
public boolean canUpdateUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
return canUpdate(userAccountNotificationPreferences.getUserAccount());
}

/**
 * check can update one to many component UserAccountNotificationPreferences
 */
public void checkCanUpdateUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
if (!canUpdateUserAccountNotificationPreferences(userAccountNotificationPreferences)) {
throw new OperationDeniedException("UserAccountNotificationPreferences.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(UserAccount userAccount) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(UserAccount userAccount) {
if (!canDelete(userAccount)) {
throw new OperationDeniedException("UserAccount.delete.operationDenied");
}
}

/**
 * can delete one to many component UserAccountNotificationPreferences
 */
public boolean canDeleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
return canUpdate(userAccountNotificationPreferences.getUserAccount());
}

/**
 * check can delete one to many component UserAccountNotificationPreferences
 */
public void checkCanDeleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
if (!canDeleteUserAccountNotificationPreferences(userAccountNotificationPreferences)) {
throw new OperationDeniedException("UserAccountNotificationPreferences.delete.operationDenied");
}
}

}
