package org.sklsft.social.bc.mapper.settings.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.settings.views.basic.GlobalParameterBasicView;
import org.sklsft.social.bc.rightsmanager.settings.GlobalParameterRightsManager;
import org.sklsft.social.bc.statemanager.settings.GlobalParameterStateManager;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.ParameterTypeDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBasicViewBaseMapper extends BasicMapperImpl<GlobalParameterBasicView, GlobalParameter> {

public GlobalParameterBasicViewBaseMapper() {
super(GlobalParameterBasicView.class, GlobalParameter.class);
}

/*
 * properties
 */
@Inject
protected ParameterTypeDao parameterTypeDao;

@Inject
protected GlobalParameterRightsManager globalParameterRightsManager;
@Inject
protected GlobalParameterStateManager globalParameterStateManager;

/**
 * mapping view from object
 */
@Override
public GlobalParameterBasicView mapFrom(GlobalParameterBasicView globalParameterBasicView, GlobalParameter globalParameter) {
globalParameterBasicView = super.mapFrom(globalParameterBasicView, globalParameter);
globalParameterBasicView.setSelected(false);
globalParameterBasicView.setCanDelete(globalParameterRightsManager.canDelete(globalParameter) && globalParameterStateManager.canDelete(globalParameter));
globalParameterBasicView.setParameterTypeCode(globalParameter.getParameterType().getCode());
return globalParameterBasicView;
}

/**
 * mapping view to object
 */
@Override
public GlobalParameter mapTo(GlobalParameterBasicView globalParameterBasicView, GlobalParameter globalParameter) {
globalParameter = super.mapTo(globalParameterBasicView, globalParameter);
globalParameter.setParameterType(parameterTypeDao.find(globalParameterBasicView.getParameterTypeCode()));
return globalParameter;
}

}
