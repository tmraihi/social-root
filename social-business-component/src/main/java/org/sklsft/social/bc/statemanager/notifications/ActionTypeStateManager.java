package org.sklsft.social.bc.statemanager.notifications;

import org.sklsft.social.bc.statemanager.notifications.base.ActionTypeBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialActionTypeStateManager")
public class ActionTypeStateManager extends ActionTypeBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
