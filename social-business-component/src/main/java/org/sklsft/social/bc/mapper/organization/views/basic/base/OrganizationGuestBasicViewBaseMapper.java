package org.sklsft.social.bc.mapper.organization.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.views.basic.OrganizationGuestBasicView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationGuestRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationGuestStateManager;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationGuestBasicViewBaseMapper extends BasicMapperImpl<OrganizationGuestBasicView, OrganizationGuest> {

public OrganizationGuestBasicViewBaseMapper() {
super(OrganizationGuestBasicView.class, OrganizationGuest.class);
}

/*
 * properties
 */
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;

@Inject
protected OrganizationGuestRightsManager organizationGuestRightsManager;
@Inject
protected OrganizationGuestStateManager organizationGuestStateManager;

/**
 * mapping view from object
 */
@Override
public OrganizationGuestBasicView mapFrom(OrganizationGuestBasicView organizationGuestBasicView, OrganizationGuest organizationGuest) {
organizationGuestBasicView = super.mapFrom(organizationGuestBasicView, organizationGuest);
organizationGuestBasicView.setSelected(false);
organizationGuestBasicView.setCanDelete(organizationGuestRightsManager.canDelete(organizationGuest) && organizationGuestStateManager.canDelete(organizationGuest));
organizationGuestBasicView.setOrganizationCode(organizationGuest.getOrganization().getCode());
if (organizationGuest.getUserAccount() != null) {
organizationGuestBasicView.setUserAccountEmail(organizationGuest.getUserAccount().getEmail());
}
return organizationGuestBasicView;
}

/**
 * mapping view to object
 */
@Override
public OrganizationGuest mapTo(OrganizationGuestBasicView organizationGuestBasicView, OrganizationGuest organizationGuest) {
organizationGuest = super.mapTo(organizationGuestBasicView, organizationGuest);
organizationGuest.setOrganization(organizationDao.find(organizationGuestBasicView.getOrganizationCode()));
organizationGuest.setUserAccount(userAccountDao.find(organizationGuestBasicView.getUserAccountEmail()));
return organizationGuest;
}

}
