package org.sklsft.social.bc.mapper.settings.views.full;

import org.sklsft.social.bc.mapper.settings.views.full.base.ParameterTypeFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialParameterTypeFullViewMapper")
public class ParameterTypeFullViewMapper extends ParameterTypeFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
