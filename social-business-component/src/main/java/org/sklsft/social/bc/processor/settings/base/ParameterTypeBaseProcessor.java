package org.sklsft.social.bc.processor.settings.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.settings.ParameterType;
import org.sklsft.social.repository.dao.interfaces.settings.ParameterTypeDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ParameterTypeBaseProcessor implements Processor<ParameterType, Long> {

/*
 * properties injected by spring
 */
@Inject
protected ParameterTypeDao parameterTypeDao;

/**
 * process save
 */
public Long save(ParameterType parameterType) {
return parameterTypeDao.save(parameterType);
}

/**
 * process update
 */
public void update(ParameterType parameterType) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(ParameterType parameterType) {
parameterTypeDao.delete(parameterType);
}

}
