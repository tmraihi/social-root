package org.sklsft.social.bc.mapper.settings.views.basic;

import org.sklsft.social.bc.mapper.settings.views.basic.base.GlobalParameterBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialGlobalParameterBasicViewMapper")
public class GlobalParameterBasicViewMapper extends GlobalParameterBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
