package org.sklsft.social.bc.processor.organization;

import javax.inject.Inject;
import org.sklsft.social.api.exception.state.organization.OrganizationAlreadyUsedException;
import org.sklsft.social.bc.processor.organization.base.OrganizationBaseProcessor;
import org.sklsft.social.model.organization.Organization;
import org.sklsft.social.util.text.FullTextNormalizer;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class OrganizationProcessor extends OrganizationBaseProcessor {

/* Specific Code Start */
	
	@Inject
	private FullTextNormalizer fullTextNormalizer;
	
	
	@Override
	public Long save(Organization organization) {
		
		String code = getCode(organization.getName());
		organization.setCode(code);
		
		Long id = organizationDao.save(organization);
		
		return id;
	}	
	
	
	private String getCode(String name) {
		
		String canonicalLabel = fullTextNormalizer.getCanonicalLabel(name);
		
		String result = canonicalLabel.replace(" ", "-");		
		
		if (organizationDao.exists(result)) {
			int i = 1;
			while (organizationDao.exists(result + "-" + i)) {
				i++;
			}
			result = result + "-" + i;
		}
		return result;
	}
	
	
	public void updateIdentifier(Organization organization, String code) {

		if (!organization.getCode().equals(code) && organizationDao.exists(code)) {
			throw new OrganizationAlreadyUsedException("organization.code.alreadyExists");
		}

		organization.setCode(code);
		
	}
	
	
/* Specific Code End */
}
