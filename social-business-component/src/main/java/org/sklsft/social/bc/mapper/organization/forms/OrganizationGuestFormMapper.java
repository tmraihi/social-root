package org.sklsft.social.bc.mapper.organization.forms;

import org.sklsft.social.bc.mapper.organization.forms.base.OrganizationGuestFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationGuestFormMapper")
public class OrganizationGuestFormMapper extends OrganizationGuestFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
