package org.sklsft.social.bc.rightsmanager.organization;

import org.sklsft.social.bc.rightsmanager.organization.base.OrganizationMemberBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationMemberRightsManager")
public class OrganizationMemberRightsManager extends OrganizationMemberBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
