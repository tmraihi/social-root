package org.sklsft.social.bc.rightsmanager.notifications;

import org.sklsft.social.bc.rightsmanager.notifications.base.ActionTypeBaseRightsManager;
import org.springframework.stereotype.Component;

/**
 * auto generated rights manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialActionTypeRightsManager")
public class ActionTypeRightsManager extends ActionTypeBaseRightsManager {

/* Specific Code Start */
/* Specific Code End */
}
