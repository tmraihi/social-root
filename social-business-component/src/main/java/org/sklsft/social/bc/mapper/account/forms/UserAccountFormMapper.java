package org.sklsft.social.bc.mapper.account.forms;

import org.sklsft.social.bc.mapper.account.forms.base.UserAccountFormBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountFormMapper")
public class UserAccountFormMapper extends UserAccountFormBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
