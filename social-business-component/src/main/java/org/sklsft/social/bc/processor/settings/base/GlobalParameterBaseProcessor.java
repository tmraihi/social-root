package org.sklsft.social.bc.processor.settings.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.settings.GlobalParameter;
import org.sklsft.social.repository.dao.interfaces.settings.GlobalParameterDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class GlobalParameterBaseProcessor implements Processor<GlobalParameter, Long> {

/*
 * properties injected by spring
 */
@Inject
protected GlobalParameterDao globalParameterDao;

/**
 * process save
 */
public Long save(GlobalParameter globalParameter) {
return globalParameterDao.save(globalParameter);
}

/**
 * process update
 */
public void update(GlobalParameter globalParameter) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(GlobalParameter globalParameter) {
globalParameterDao.delete(globalParameter);
}

}
