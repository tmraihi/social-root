package org.sklsft.social.bc.rightsmanager.organization.base;

import org.sklsft.commons.api.exception.rights.AccessDeniedException;
import org.sklsft.commons.api.exception.rights.OperationDeniedException;
import org.sklsft.commons.model.patterns.RightsManager;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated base rights manager class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBaseRightsManager implements RightsManager<Organization, Long> {

/**
 * can access all
 */
public boolean canAccess() {
return true;
}

/**
 * check can access all
 */
public void checkCanAccess() {
if (!canAccess()) {
throw new AccessDeniedException("Organization.accessDenied");
}
}

/**
 * can access
 */
public boolean canAccess(Organization organization) {
return true;
}

/**
 * check can access
 */
public void checkCanAccess(Organization organization) {
if (!canAccess(organization)) {
throw new AccessDeniedException("Organization.accessDenied");
}
}

/**
 * can create
 */
public boolean canCreate() {
return true;
}

/**
 * check can create
 */
public void checkCanCreate() {
if (!canCreate()) {
throw new OperationDeniedException("Organization.create.operationDenied");
}
}

/**
 * can save
 */
public boolean canSave(Organization organization) {
return true;
}

/**
 * check can save
 */
public void checkCanSave(Organization organization) {
if (!canSave(organization)) {
throw new OperationDeniedException("Organization.save.operationDenied");
}
}

/**
 * can update
 */
public boolean canUpdate(Organization organization) {
return true;
}

/**
 * check can update
 */
public void checkCanUpdate(Organization organization) {
if (!canUpdate(organization)) {
throw new OperationDeniedException("Organization.update.operationDenied");
}
}

/**
 * can delete
 */
public boolean canDelete(Organization organization) {
return true;
}

/**
 * check can delete
 */
public void checkCanDelete(Organization organization) {
if (!canDelete(organization)) {
throw new OperationDeniedException("Organization.delete.operationDenied");
}
}

}
