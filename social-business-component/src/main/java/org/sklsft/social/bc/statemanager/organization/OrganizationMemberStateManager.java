package org.sklsft.social.bc.statemanager.organization;

import org.sklsft.social.bc.statemanager.organization.base.OrganizationMemberBaseStateManager;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialOrganizationMemberStateManager")
public class OrganizationMemberStateManager extends OrganizationMemberBaseStateManager {

/* Specific Code Start */
/* Specific Code End */
}
