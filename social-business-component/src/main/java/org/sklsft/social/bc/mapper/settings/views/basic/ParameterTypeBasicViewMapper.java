package org.sklsft.social.bc.mapper.settings.views.basic;

import org.sklsft.social.bc.mapper.settings.views.basic.base.ParameterTypeBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialParameterTypeBasicViewMapper")
public class ParameterTypeBasicViewMapper extends ParameterTypeBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
