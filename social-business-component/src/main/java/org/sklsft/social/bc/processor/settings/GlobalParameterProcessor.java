package org.sklsft.social.bc.processor.settings;

import org.sklsft.social.bc.processor.settings.base.GlobalParameterBaseProcessor;
import org.springframework.stereotype.Component;

/**
 * auto generated state manager class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component
public class GlobalParameterProcessor extends GlobalParameterBaseProcessor {

/* Specific Code Start */
/* Specific Code End */
}
