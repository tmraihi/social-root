package org.sklsft.social.bc.mapper.settings.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.settings.forms.GlobalParameterForm;
import org.sklsft.social.api.model.settings.views.full.GlobalParameterFullView;
import org.sklsft.social.bc.rightsmanager.settings.GlobalParameterRightsManager;
import org.sklsft.social.bc.statemanager.settings.GlobalParameterStateManager;
import org.sklsft.social.model.settings.GlobalParameter;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class GlobalParameterFullViewBaseMapper extends FullViewMapper<GlobalParameterFullView, Long, GlobalParameterForm, GlobalParameter> {

@Inject
protected GlobalParameterRightsManager globalParameterRightsManager;
@Inject
protected GlobalParameterStateManager globalParameterStateManager;

public GlobalParameterFullViewBaseMapper() {
super(GlobalParameterFullView.class, GlobalParameter.class);
}

@Override
public GlobalParameterFullView mapFrom(GlobalParameterFullView globalParameterFullView, GlobalParameter globalParameter) {
globalParameterFullView = super.mapFrom(globalParameterFullView, globalParameter);
globalParameterFullView.setCanUpdate(globalParameterRightsManager.canUpdate(globalParameter) && globalParameterStateManager.canUpdate(globalParameter));
return globalParameterFullView;
}

}
