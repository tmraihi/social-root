package org.sklsft.social.bc.mapper.settings.views.full;

import org.sklsft.social.bc.mapper.settings.views.full.base.GlobalParameterFullViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialGlobalParameterFullViewMapper")
public class GlobalParameterFullViewMapper extends GlobalParameterFullViewBaseMapper{

/* Specific Code Start */
/* Specific Code End */
}
