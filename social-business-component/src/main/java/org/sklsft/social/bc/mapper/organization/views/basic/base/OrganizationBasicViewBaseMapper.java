package org.sklsft.social.bc.mapper.organization.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.views.basic.OrganizationBasicView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationStateManager;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationBasicViewBaseMapper extends BasicMapperImpl<OrganizationBasicView, Organization> {

public OrganizationBasicViewBaseMapper() {
super(OrganizationBasicView.class, Organization.class);
}

/*
 * properties
 */

@Inject
protected OrganizationRightsManager organizationRightsManager;
@Inject
protected OrganizationStateManager organizationStateManager;

/**
 * mapping view from object
 */
@Override
public OrganizationBasicView mapFrom(OrganizationBasicView organizationBasicView, Organization organization) {
organizationBasicView = super.mapFrom(organizationBasicView, organization);
organizationBasicView.setSelected(false);
organizationBasicView.setCanDelete(organizationRightsManager.canDelete(organization) && organizationStateManager.canDelete(organization));
return organizationBasicView;
}

/**
 * mapping view to object
 */
@Override
public Organization mapTo(OrganizationBasicView organizationBasicView, Organization organization) {
organization = super.mapTo(organizationBasicView, organization);
return organization;
}

}
