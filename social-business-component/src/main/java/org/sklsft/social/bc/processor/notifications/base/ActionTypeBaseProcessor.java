package org.sklsft.social.bc.processor.notifications.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.notifications.ActionType;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class ActionTypeBaseProcessor implements Processor<ActionType, Long> {

/*
 * properties injected by spring
 */
@Inject
protected ActionTypeDao actionTypeDao;

/**
 * process save
 */
public Long save(ActionType actionType) {
return actionTypeDao.save(actionType);
}

/**
 * process update
 */
public void update(ActionType actionType) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(ActionType actionType) {
actionTypeDao.delete(actionType);
}

}
