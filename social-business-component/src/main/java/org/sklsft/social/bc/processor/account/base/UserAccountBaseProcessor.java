package org.sklsft.social.bc.processor.account.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountBaseProcessor implements Processor<UserAccount, Long> {

/*
 * properties injected by spring
 */
@Inject
protected UserAccountDao userAccountDao;

/**
 * process save
 */
public Long save(UserAccount userAccount) {
return userAccountDao.save(userAccount);
}

/**
 * process save one to many component UserAccountNotificationPreferences
 */
public void saveUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences,UserAccount userAccount) {
userAccountDao.saveUserAccountNotificationPreferences(userAccount, userAccountNotificationPreferences);
}

/**
 * process update
 */
public void update(UserAccount userAccount) {
// Empty by default. Can be overridden
}

/**
 * process update one to many component UserAccountNotificationPreferences
 */
public void updateUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(UserAccount userAccount) {
userAccountDao.delete(userAccount);
}

/**
 * process delete one to many component UserAccountNotificationPreferences
 */
public void deleteUserAccountNotificationPreferences(UserAccountNotificationPreferences userAccountNotificationPreferences) {
userAccountDao.deleteUserAccountNotificationPreferences(userAccountNotificationPreferences);
}

}
