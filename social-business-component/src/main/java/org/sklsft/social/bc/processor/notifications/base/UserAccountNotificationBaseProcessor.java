package org.sklsft.social.bc.processor.notifications.base;

import javax.inject.Inject;
import org.sklsft.commons.model.patterns.Processor;
import org.sklsft.social.model.notifications.UserAccountNotification;
import org.sklsft.social.repository.dao.interfaces.notifications.UserAccountNotificationDao;

/**
 * auto generated base processor class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationBaseProcessor implements Processor<UserAccountNotification, Long> {

/*
 * properties injected by spring
 */
@Inject
protected UserAccountNotificationDao userAccountNotificationDao;

/**
 * process save
 */
public Long save(UserAccountNotification userAccountNotification) {
return userAccountNotificationDao.save(userAccountNotification);
}

/**
 * process update
 */
public void update(UserAccountNotification userAccountNotification) {
// Empty by default. Can be overridden
}

/**
 * process delete
 */
public void delete(UserAccountNotification userAccountNotification) {
userAccountNotificationDao.delete(userAccountNotification);
}

}
