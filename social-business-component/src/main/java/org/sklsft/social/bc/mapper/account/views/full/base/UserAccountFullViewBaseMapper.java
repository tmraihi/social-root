package org.sklsft.social.bc.mapper.account.views.full.base;

import javax.inject.Inject;
import org.sklsft.commons.mapper.impl.FullViewMapper;
import org.sklsft.social.api.model.account.forms.UserAccountForm;
import org.sklsft.social.api.model.account.views.full.UserAccountFullView;
import org.sklsft.social.bc.rightsmanager.account.UserAccountRightsManager;
import org.sklsft.social.bc.statemanager.account.UserAccountStateManager;
import org.sklsft.social.model.account.UserAccount;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

public class UserAccountFullViewBaseMapper extends FullViewMapper<UserAccountFullView, Long, UserAccountForm, UserAccount> {

@Inject
protected UserAccountRightsManager userAccountRightsManager;
@Inject
protected UserAccountStateManager userAccountStateManager;

public UserAccountFullViewBaseMapper() {
super(UserAccountFullView.class, UserAccount.class);
}

@Override
public UserAccountFullView mapFrom(UserAccountFullView userAccountFullView, UserAccount userAccount) {
userAccountFullView = super.mapFrom(userAccountFullView, userAccount);
userAccountFullView.setCanUpdate(userAccountRightsManager.canUpdate(userAccount) && userAccountStateManager.canUpdate(userAccount));
return userAccountFullView;
}

}
