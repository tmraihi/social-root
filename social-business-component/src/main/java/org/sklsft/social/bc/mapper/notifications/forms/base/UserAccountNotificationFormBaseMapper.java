package org.sklsft.social.bc.mapper.notifications.forms.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.notifications.forms.UserAccountNotificationForm;
import org.sklsft.social.model.notifications.UserAccountNotification;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.notifications.ActionTypeDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class UserAccountNotificationFormBaseMapper extends BasicMapperImpl<UserAccountNotificationForm, UserAccountNotification> {

public UserAccountNotificationFormBaseMapper() {
super(UserAccountNotificationForm.class, UserAccountNotification.class);
}

/*
 * properties
 */
@Inject
protected UserAccountDao userAccountDao;
@Inject
protected ActionTypeDao actionTypeDao;
@Inject
protected OrganizationDao organizationDao;

/**
 * mapping form from object
 */
@Override
public UserAccountNotificationForm mapFrom(UserAccountNotificationForm userAccountNotificationForm, UserAccountNotification userAccountNotification) {
userAccountNotificationForm = super.mapFrom(userAccountNotificationForm, userAccountNotification);
userAccountNotificationForm.setNotifiedEmail(userAccountNotification.getNotified().getEmail());
userAccountNotificationForm.setActionTypeCode(userAccountNotification.getActionType().getCode());
userAccountNotificationForm.setFromEmail(userAccountNotification.getFrom().getEmail());
if (userAccountNotification.getFromOrganization() != null) {
userAccountNotificationForm.setFromOrganizationCode(userAccountNotification.getFromOrganization().getCode());
}
return userAccountNotificationForm;
}

/**
 * mapping view to object
 */
@Override
public UserAccountNotification mapTo(UserAccountNotificationForm userAccountNotificationForm, UserAccountNotification userAccountNotification) {
userAccountNotification = super.mapTo(userAccountNotificationForm, userAccountNotification);
userAccountNotification.setNotified(userAccountDao.find(userAccountNotificationForm.getNotifiedEmail()));
userAccountNotification.setActionType(actionTypeDao.find(userAccountNotificationForm.getActionTypeCode()));
userAccountNotification.setFrom(userAccountDao.find(userAccountNotificationForm.getFromEmail()));
userAccountNotification.setFromOrganization(organizationDao.find(userAccountNotificationForm.getFromOrganizationCode()));
return userAccountNotification;
}

}
