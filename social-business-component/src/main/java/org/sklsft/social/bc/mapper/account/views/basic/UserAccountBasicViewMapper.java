package org.sklsft.social.bc.mapper.account.views.basic;

import org.sklsft.social.bc.mapper.account.views.basic.base.UserAccountBasicViewBaseMapper;
import org.springframework.stereotype.Component;

/**
 * auto generated mapper class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Component("socialUserAccountBasicViewMapper")
public class UserAccountBasicViewMapper extends UserAccountBasicViewBaseMapper {

/* Specific Code Start */
/* Specific Code End */
}
