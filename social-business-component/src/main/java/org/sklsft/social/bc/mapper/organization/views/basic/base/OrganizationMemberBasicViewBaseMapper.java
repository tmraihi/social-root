package org.sklsft.social.bc.mapper.organization.views.basic.base;

import javax.inject.Inject;
import org.sklsft.commons.api.exception.repository.ObjectNotFoundException;
import org.sklsft.commons.mapper.impl.BasicMapperImpl;
import org.sklsft.social.api.model.organization.views.basic.OrganizationMemberBasicView;
import org.sklsft.social.bc.rightsmanager.organization.OrganizationMemberRightsManager;
import org.sklsft.social.bc.statemanager.organization.OrganizationMemberStateManager;
import org.sklsft.social.model.organization.OrganizationMember;
import org.sklsft.social.repository.dao.interfaces.account.UserAccountDao;
import org.sklsft.social.repository.dao.interfaces.organization.OrganizationDao;

/**
 * auto generated base mapper class file
 * <br/>no modification should be done to this file
 * <br/>processed by skeleton-generator
 */
public class OrganizationMemberBasicViewBaseMapper extends BasicMapperImpl<OrganizationMemberBasicView, OrganizationMember> {

public OrganizationMemberBasicViewBaseMapper() {
super(OrganizationMemberBasicView.class, OrganizationMember.class);
}

/*
 * properties
 */
@Inject
protected OrganizationDao organizationDao;
@Inject
protected UserAccountDao userAccountDao;

@Inject
protected OrganizationMemberRightsManager organizationMemberRightsManager;
@Inject
protected OrganizationMemberStateManager organizationMemberStateManager;

/**
 * mapping view from object
 */
@Override
public OrganizationMemberBasicView mapFrom(OrganizationMemberBasicView organizationMemberBasicView, OrganizationMember organizationMember) {
organizationMemberBasicView = super.mapFrom(organizationMemberBasicView, organizationMember);
organizationMemberBasicView.setSelected(false);
organizationMemberBasicView.setCanDelete(organizationMemberRightsManager.canDelete(organizationMember) && organizationMemberStateManager.canDelete(organizationMember));
organizationMemberBasicView.setOrganizationCode(organizationMember.getOrganization().getCode());
organizationMemberBasicView.setUserAccountEmail(organizationMember.getUserAccount().getEmail());
return organizationMemberBasicView;
}

/**
 * mapping view to object
 */
@Override
public OrganizationMember mapTo(OrganizationMemberBasicView organizationMemberBasicView, OrganizationMember organizationMember) {
organizationMember = super.mapTo(organizationMemberBasicView, organizationMember);
organizationMember.setOrganization(organizationDao.find(organizationMemberBasicView.getOrganizationCode()));
organizationMember.setUserAccount(userAccountDao.find(organizationMemberBasicView.getUserAccountEmail()));
return organizationMember;
}

}
