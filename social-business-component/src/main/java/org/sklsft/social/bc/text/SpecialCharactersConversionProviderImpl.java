package org.sklsft.social.bc.text;

import java.util.HashMap;
import java.util.Map;

import org.sklsft.social.util.text.SpecialCharactersConversionProvider;

public class SpecialCharactersConversionProviderImpl implements SpecialCharactersConversionProvider {

	private static Map<String, String> specialCharactersMap;
	
	static {
		specialCharactersMap = new HashMap<>();
		specialCharactersMap.put("+", " ");
		specialCharactersMap.put("_", " ");
		specialCharactersMap.put("/", " ");
		specialCharactersMap.put("\\", " ");
		specialCharactersMap.put("\"", " ");
		specialCharactersMap.put("'", " ");
		specialCharactersMap.put("(", " ");
		specialCharactersMap.put(")", " ");
		specialCharactersMap.put("[", " ");
		specialCharactersMap.put("]", " ");
		specialCharactersMap.put("{", " ");
		specialCharactersMap.put("}", " ");
	}


	@Override
	public Map<String, String> getSpecialCharactersConversion() {
		return specialCharactersMap;
	}
	
	

}
