package org.sklsft.social.interfaces;

public interface FileEntity {
	
	Long getId();
	
	String getRelativeUrl();

	String getPoolName();
	
}
