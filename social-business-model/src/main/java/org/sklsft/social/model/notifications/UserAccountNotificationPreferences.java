package org.sklsft.social.model.notifications;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.ActionType;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="USER_ACCOUNT_NOTIFICATION_PREFERENCES"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"USER_ACCOUNT_ID", "ACTION_TYPE_ID"})})
public class UserAccountNotificationPreferences implements org.sklsft.commons.model.interfaces.Entity<Long> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public UserAccountNotificationPreferences(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "USER_ACCOUNT_NOTIFICATION_PREFERENCES_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@ManyToOne(fetch = FetchType.LAZY)
@Fetch(FetchMode.JOIN)
@JoinColumn(name = "USER_ACCOUNT_ID", nullable = false)
private UserAccount userAccount;

@ManyToOne(fetch = FetchType.LAZY)
@Fetch(FetchMode.JOIN)
@JoinColumn(name = "ACTION_TYPE_ID", nullable = false)
private ActionType actionType;

@Column(name = "MAIL_TYPE", nullable = false)
private String mailType;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public UserAccount getUserAccount() {
return this.userAccount;
}

public void setUserAccount(UserAccount userAccount) {
this.userAccount = userAccount;
}

public ActionType getActionType() {
return this.actionType;
}

public void setActionType(ActionType actionType) {
this.actionType = actionType;
}

public String getMailType() {
return this.mailType;
}

public void setMailType(String mailType) {
this.mailType = mailType;
}


/* Specific Code Start */
/* Specific Code End */
}