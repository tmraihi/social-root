package org.sklsft.social.model.notifications;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.notifications.ActionType;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="USER_ACCOUNT_NOTIFICATION")
public class UserAccountNotification implements org.sklsft.commons.model.interfaces.Entity<Long> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public UserAccountNotification(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "USER_ACCOUNT_NOTIFICATION_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "NOTIFIED_ID", nullable = false)
private UserAccount notified;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "ACTION_TYPE_ID", nullable = false)
private ActionType actionType;

@Temporal(TemporalType.TIMESTAMP)
@Column(name = "NOTIFICATION_DATE", nullable = false)
private Date notificationDate;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "FROM_ID", nullable = false)
private UserAccount from;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "FROM_ORGANIZATION_ID")
private Organization fromOrganization;

@Lob
@Type(type="org.hibernate.type.TextType")
@Column(name = "MESSAGE")
private String message;

@Column(name = "READ", nullable = false)
private Boolean read;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public UserAccount getNotified() {
return this.notified;
}

public void setNotified(UserAccount notified) {
this.notified = notified;
}

public ActionType getActionType() {
return this.actionType;
}

public void setActionType(ActionType actionType) {
this.actionType = actionType;
}

public Date getNotificationDate() {
return this.notificationDate;
}

public void setNotificationDate(Date notificationDate) {
this.notificationDate = notificationDate;
}

public UserAccount getFrom() {
return this.from;
}

public void setFrom(UserAccount from) {
this.from = from;
}

public Organization getFromOrganization() {
return this.fromOrganization;
}

public void setFromOrganization(Organization fromOrganization) {
this.fromOrganization = fromOrganization;
}

public String getMessage() {
return this.message;
}

public void setMessage(String message) {
this.message = message;
}

public Boolean getRead() {
return this.read;
}

public void setRead(Boolean read) {
this.read = read;
}


/* Specific Code Start */
/* Specific Code End */
}