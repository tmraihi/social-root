package org.sklsft.social.model.notifications;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="MEMBER_NOTIFICATION_PREFERENCES"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"MEMBER_ID", "ACTION_TYPE_ID"})})
public class MemberNotificationPreferences implements org.sklsft.commons.model.interfaces.Entity<Long> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public MemberNotificationPreferences(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "MEMBER_NOTIFICATION_PREFERENCES_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@ManyToOne(fetch = FetchType.LAZY)
@Fetch(FetchMode.JOIN)
@JoinColumn(name = "MEMBER_ID", nullable = false)
private OrganizationMember member;

@ManyToOne(fetch = FetchType.LAZY)
@Fetch(FetchMode.JOIN)
@JoinColumn(name = "ACTION_TYPE_ID", nullable = false)
private ActionType actionType;

@Column(name = "MAIL_TYPE", nullable = false)
private String mailType;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public OrganizationMember getMember() {
return this.member;
}

public void setMember(OrganizationMember member) {
this.member = member;
}

public ActionType getActionType() {
return this.actionType;
}

public void setActionType(ActionType actionType) {
this.actionType = actionType;
}

public String getMailType() {
return this.mailType;
}

public void setMailType(String mailType) {
this.mailType = mailType;
}


/* Specific Code Start */
/* Specific Code End */
}