package org.sklsft.social.model.account;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.interfaces.FileEntity;
import org.sklsft.social.model.notifications.UserAccountNotificationPreferences;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="USER_ACCOUNT"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"EMAIL"})})
public class UserAccount implements org.sklsft.commons.model.interfaces.Entity<Long>, FileEntity {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public UserAccount(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "USER_ACCOUNT_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@Column(name = "EMAIL", nullable = false)
private String email;

@Column(name = "PASSWORD_HASH")
private String passwordHash;

@Column(name = "FIRST_NAME", nullable = false)
private String firstName;

@Column(name = "LAST_NAME", nullable = false)
private String lastName;

@Temporal(TemporalType.TIMESTAMP)
@Column(name = "CREATION_DATE", nullable = false)
private Date creationDate;

@Column(name = "ACTIVATED", nullable = false)
private Boolean activated;

@Column(name = "EMAIL_VALIDATED", nullable = false)
private Boolean emailValidated;

@Column(name = "USER_ROLE", nullable = false)
private String userRole;

@OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
private Set <OrganizationMember> organizationMemberCollection;

@OneToMany(fetch = FetchType.LAZY, mappedBy = "userAccount")
private Set <OrganizationGuest> organizationGuestCollection;

@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval=true, mappedBy = "userAccount")
private Set <UserAccountNotificationPreferences> userAccountNotificationPreferencesCollection;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public String getEmail() {
return this.email;
}

public void setEmail(String email) {
this.email = email;
}

public String getPasswordHash() {
return this.passwordHash;
}

public void setPasswordHash(String passwordHash) {
this.passwordHash = passwordHash;
}

public String getFirstName() {
return this.firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return this.lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public Date getCreationDate() {
return this.creationDate;
}

public void setCreationDate(Date creationDate) {
this.creationDate = creationDate;
}

public Boolean getActivated() {
return this.activated;
}

public void setActivated(Boolean activated) {
this.activated = activated;
}

public Boolean getEmailValidated() {
return this.emailValidated;
}

public void setEmailValidated(Boolean emailValidated) {
this.emailValidated = emailValidated;
}

public String getUserRole() {
return this.userRole;
}

public void setUserRole(String userRole) {
this.userRole = userRole;
}

public Set <OrganizationMember> getOrganizationMemberCollection () {
return this.organizationMemberCollection;
}

public void setOrganizationMemberCollection(Set <OrganizationMember> organizationMemberCollection) {
this.organizationMemberCollection = organizationMemberCollection;
}

public Set <OrganizationGuest> getOrganizationGuestCollection () {
return this.organizationGuestCollection;
}

public void setOrganizationGuestCollection(Set <OrganizationGuest> organizationGuestCollection) {
this.organizationGuestCollection = organizationGuestCollection;
}

public Set <UserAccountNotificationPreferences> getUserAccountNotificationPreferencesCollection () {
return this.userAccountNotificationPreferencesCollection;
}

public void setUserAccountNotificationPreferencesCollection(Set <UserAccountNotificationPreferences> userAccountNotificationPreferencesCollection) {
this.userAccountNotificationPreferencesCollection = userAccountNotificationPreferencesCollection;
}


/* Specific Code Start */

@Override
public String getRelativeUrl() {
	return "/account/public/" + id + "/logo";
}

@Override
public String getPoolName() {
	return "user_account_logo";
}


/* Specific Code End */
}