package org.sklsft.social.model.organization;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.interfaces.FileEntity;
import org.sklsft.social.model.organization.OrganizationGuest;
import org.sklsft.social.model.organization.OrganizationMember;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="ORGANIZATION"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"CODE"})})
public class Organization implements org.sklsft.commons.model.interfaces.Entity<Long>, FileEntity {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public Organization(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "ORGANIZATION_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@Column(name = "CODE", nullable = false)
private String code;

@Column(name = "NAME", nullable = false)
private String name;

@Lob
@Type(type="org.hibernate.type.TextType")
@Column(name = "DESCRIPTION", nullable = false)
private String description;

@OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
private Set <OrganizationMember> organizationMemberCollection;

@OneToMany(fetch = FetchType.LAZY, mappedBy = "organization")
private Set <OrganizationGuest> organizationGuestCollection;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public String getCode() {
return this.code;
}

public void setCode(String code) {
this.code = code;
}

public String getName() {
return this.name;
}

public void setName(String name) {
this.name = name;
}

public String getDescription() {
return this.description;
}

public void setDescription(String description) {
this.description = description;
}

public Set <OrganizationMember> getOrganizationMemberCollection () {
return this.organizationMemberCollection;
}

public void setOrganizationMemberCollection(Set <OrganizationMember> organizationMemberCollection) {
this.organizationMemberCollection = organizationMemberCollection;
}

public Set <OrganizationGuest> getOrganizationGuestCollection () {
return this.organizationGuestCollection;
}

public void setOrganizationGuestCollection(Set <OrganizationGuest> organizationGuestCollection) {
this.organizationGuestCollection = organizationGuestCollection;
}


/* Specific Code Start */

@Override
public String getRelativeUrl() {
	return "/organization/public/" + id + "/logo";
}

@Override
public String getPoolName() {
	return "organization_logo";
}

/* Specific Code End */
}