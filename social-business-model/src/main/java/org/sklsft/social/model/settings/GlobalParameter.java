package org.sklsft.social.model.settings;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.model.settings.ParameterType;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="GLOBAL_PARAMETER"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"CODE"})})
public class GlobalParameter implements org.sklsft.commons.model.interfaces.Entity<Long> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public GlobalParameter(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "GLOBAL_PARAMETER_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@Column(name = "CODE", nullable = false)
private String code;

@Column(name = "DESCRIPTION", nullable = false)
private String description;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "PARAMETER_TYPE_ID", nullable = false)
private ParameterType parameterType;

@Column(name = "VALUE", nullable = false)
private String value;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public String getCode() {
return this.code;
}

public void setCode(String code) {
this.code = code;
}

public String getDescription() {
return this.description;
}

public void setDescription(String description) {
this.description = description;
}

public ParameterType getParameterType() {
return this.parameterType;
}

public void setParameterType(ParameterType parameterType) {
this.parameterType = parameterType;
}

public String getValue() {
return this.value;
}

public void setValue(String value) {
this.value = value;
}


/* Specific Code Start */
/* Specific Code End */
}