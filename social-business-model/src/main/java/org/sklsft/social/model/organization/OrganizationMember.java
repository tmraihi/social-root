package org.sklsft.social.model.organization;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.sklsft.social.model.account.UserAccount;
import org.sklsft.social.model.organization.Organization;

/**
 * auto generated entity class file
 * <br/>write modifications between specific code marks
 * <br/>processed by skeleton-generator
 */

@Entity
@Table(name="ORGANIZATION_MEMBER"
, uniqueConstraints = {@UniqueConstraint(columnNames = {"ORGANIZATION_ID", "USER_ACCOUNT_ID"})})
public class OrganizationMember implements org.sklsft.commons.model.interfaces.Entity<Long> {

private static final long serialVersionUID = 1L;

/*
 * no argument constructor
 */
public OrganizationMember(){
}

/*
 * properties
 */
@Id
@Column(name = "id", nullable = false)
@SequenceGenerator(name = "generator", sequenceName = "ORGANIZATION_MEMBER_id_seq", allocationSize=1)
@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator")
private Long id;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "ORGANIZATION_ID", nullable = false)
private Organization organization;

@ManyToOne(fetch = FetchType.LAZY)
@JoinColumn(name = "USER_ACCOUNT_ID", nullable = false)
private UserAccount userAccount;

@Column(name = "ROLE", nullable = false)
private String role;

@Column(name = "POSITION")
private String position;


/*
 * getters and setters
 */
public Long getId() {
return this.id;
}

public void setId(Long id) {
this.id = id;
}

public Organization getOrganization() {
return this.organization;
}

public void setOrganization(Organization organization) {
this.organization = organization;
}

public UserAccount getUserAccount() {
return this.userAccount;
}

public void setUserAccount(UserAccount userAccount) {
this.userAccount = userAccount;
}

public String getRole() {
return this.role;
}

public void setRole(String role) {
this.role = role;
}

public String getPosition() {
return this.position;
}

public void setPosition(String position) {
this.position = position;
}


/* Specific Code Start */
/* Specific Code End */
}